<?php

namespace App\Corcel;

use Corcel\Model\Post;

class RecurringEvent extends Post
{
    protected $postType = 'imprezy-cykliczne';
}