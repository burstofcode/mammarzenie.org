<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Themosis\Support\Facades\Action;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot()
    {

    }

    /**
     * Register any application services.
     */
    public function register()
    {

    }
}
