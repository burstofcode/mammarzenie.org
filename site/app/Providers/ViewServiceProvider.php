<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

/**
 * Class ViewServiceProvider
 * @package App\Providers
 */
class ViewServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Header menu
        View::composer('parts.header-menu',
            'App\Http\View\Composers\NaviComposer@header');

        // Footer menu
        View::composer('parts.footer-menu',
            'App\Http\View\Composers\NaviComposer@footer');

        // About us menu
        View::composer('parts.about-us-menu',
            'App\Http\View\Composers\NaviComposer@aboutUs');

        // Date filter
        View::composer([
            'dreamers.front.index',
            'dreamers.front.branch',
            'news.archive'
        ],
            'App\Http\View\Composers\FilterComposer@date');

        // Category filter
        View::composer([
            'dreamers.front.index',
            'dreamers.front.branch'
        ],
            'App\Http\View\Composers\FilterComposer@dreamCategory');

        // Branch filter
        View::composer([
            'dreamers.front.index',
            'dreamers.front.branch',
            'what-we-do.archive',
            'news.archive'
        ],
            'App\Http\View\Composers\FilterComposer@branch');

        // Event type filter
        View::composer([
            'what-we-do.archive'
        ],
            'App\Http\View\Composers\FilterComposer@eventType');

        // Event time frame filter
        View::composer([
            'what-we-do.archive'
        ],
            'App\Http\View\Composers\FilterComposer@eventTimeFrame');

        // View type filter
        View::composer([
            'dreamers.front.index',
            'dreamers.front.branch'
        ],
            'App\Http\View\Composers\FilterComposer@viewType');
    }
}
