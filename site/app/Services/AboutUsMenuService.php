<?php

namespace App\Services;

use Illuminate\Http\Request;

/**
 * Class AboutUsMenuService
 * @package App\Services
 */
class AboutUsMenuService
{
    /**
     * @param Request $request
     * @return bool
     */
    public static function isActive( Request $request)
    {
        if (
            $request->is('o-fundacji')
            OR $request->is('o-fundacji/zarzad')
            OR $request->is('o-fundacji/rada-programowa')
            OR $request->is('o-fundacji/finanse')
            OR $request->is('o-fundacji/jakich-marzen-nie-spelniamy')
        ) {
            return true;
        }
    }
}