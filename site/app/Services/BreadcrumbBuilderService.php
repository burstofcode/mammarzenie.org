<?php

namespace App\Services;

use App\Models\Branch;
use App\Models\WishState;
use Illuminate\Http\Request;

class BreadcrumbBuilderService
{
    public static $get;


    public static function home()
    {
        static::$get = '<span><a href="' . home_url('/') . '">' . __('Strona głowna') . '</a></span> > ';

        return new static();
    }

    public static function current($name)
    {
        static::$get .= '<span>' . $name . '</span>';

        return new static();
    }

    public static function inactive($name, $delimiter = true)
    {
        $delimiter = true ? ' > ' : '';

        static::$get .= '<span>' . $name . '</span>' . $delimiter;

        return new static();
    }

    public static function link($name, $url, $delimiter = true)
    {
        $delimiter = true ? ' > ' : '';

        static::$get .= '<span><a href="' . home_url($url) . '">' . $name . '</a></span>' . $delimiter;

        return new static();
    }

    public static function get()
    {
        return static::$get;
    }
}