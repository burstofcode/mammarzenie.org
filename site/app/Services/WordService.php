<?php

namespace App\Services;

class WordService
{
    /**
     * Modify word age in Polish to make sure that it shows proper form
     */
    public static function modifyWordYear($age)
    {
        if($age <= 31) {
            if($age == '1') return 'rok';
            elseif(($age >= 2 && $age <= 4) OR ($age >= 22 && $age <= 24)) return 'lata';
            elseif(($age >= 5 && $age <= 21) OR ($age >= 25 && $age <= 31)) return 'lat';
        }
    }
}