<?php

namespace App\Services;

/**
 * Class FmmFontService
 * @package App\Services\FmmFontService
 */
class FmmFontService
{
    public function wishTypeIcon($query)
    {
        if ($query == 'byc') {
            return '<img src="' . PathService::image('be.svg') . '" />';
        } elseif ($query == 'dostac') {
            return '<img src="' . PathService::image('get.svg') . '" />';
        } elseif ($query == 'zobaczyc') {
            return '<img src="' . PathService::image('see.svg') . '" />';
        } elseif ($query == 'spotkac') {
            return '<img src="' . PathService::image('meet.svg') . '" />';
        } elseif ($query == 'inne') {
            return '<img src="' . PathService::image('other.svg') . '" />';
        }
    }
}