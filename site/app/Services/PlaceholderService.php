<?php

namespace App\Services;

use App\Services\PathService;

class PlaceholderService
{

    /**
     * @return mixed
     * @throws \Illuminate\Container\EntryNotFoundException
     */
    public static function placeholder270x200()
    {
        return PathService::image('placeholder_270x200.jpg');
    }

    public static function placeholderAvatar()
    {
        return PathService::image('default_avatar.png');
    }
}