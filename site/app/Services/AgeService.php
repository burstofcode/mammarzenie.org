<?php

namespace App\Services;

use Carbon\Carbon;

class AgeService
{
    /**
     * Calculate age of dreamer
     *
     * It's utilizing Carbon library
     */
    public static function calculateAge( $dob = null)
    {
        return Carbon::createFromDate($dob)->diff(Carbon::now())
            ->format('%y');
    }
}