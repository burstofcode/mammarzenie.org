<?php

namespace App\Services;

use Illuminate\Http\Request;

class TitleService
{
    /**
     * Return custom page title
     * @param Request $request
     * @return string|void
     */
    public static function title(Request $request)
    {
        if($request->is('marzyciele/oczekujace/*')) {
            return 'Marzenia oczekujące';

        } elseif($request->is('marzyciele/zaadoptowane/*')) {
            return 'Marzenia zaadoptowane';

        } elseif($request->is('marzyciele/spelnione/*')) {
            return 'Marzenia spełnione';

        } elseif($request->is('marzyciele/formularz-zgloszeniowy')) {
            return 'Zgłoś marzyciela';

        } elseif($request->is('marzyciele/byli-z-nami/*')) {
            return 'Byli z nami';

        } elseif($request->is('aktualnosci/*')) {
            return __('Aktualności');

        } elseif($request->is('co-robimy/*')) {
            return __('Co robimy');

        } elseif($request->is('blog')) {
            return __('Blog');
        }
    }
}