<?php

namespace App\Services;

use App\Models\Branch;
use Illuminate\Http\Request;
use App\Services\BreadcrumbBuilderService as Breadcrumbs;

class BreadcrumbService
{
    /**
     * Return custom page title
     * @param Request $request
     * @return string|void
     */
    public function breadcrumbs(Request $request)
    {
        if($request->is('marzyciele/oczekujace/*')) {
            return Breadcrumbs::home()->current('Marzenia oczekujące')->get();

        } elseif($request->is('marzyciele/zaadoptowane/*')) {
            return Breadcrumbs::home()->current('Marzenia zaadoptowane')->get();

        } elseif($request->is('marzyciele/spelnione/*')) {
            return Breadcrumbs::home()->current('Marzenia spełnione')->get();

        } elseif($request->is('marzyciele/byli-z-nami/*')) {
            return Breadcrumbs::home()->current('Byli z nami')->get();

        } elseif($request->is('marzyciele/formularz-zgloszeniowy')) {
            return Breadcrumbs::home()->current('Zgłoś marzyciela')->get();

        } elseif($request->is('marzyciele/*')) {
            return Breadcrumbs::home()->link('Marzyciele', '/marzyciele/oczekujace/cala-polska/wszystkie/desc/20')->current('Profil marzyciela')->get();

        } elseif($request->is('aktualnosci/*')) {
            return Breadcrumbs::home()->current('Aktualności')->get();

        } elseif($request->is('co-robimy/*')) {
            return Breadcrumbs::home()->current('Co robimy')->get();

        } elseif($request->is('blog/*')) {
            return Breadcrumbs::home()->current('Blog')->get();

        }

        // All branches
        $branches = Branch::list();

        foreach($branches as $branch) {
            if($request->is($branch->slug)) {
                return Breadcrumbs::home()->inactive('Oddział')->current($branch->city)->get();
            }
        }

    }
}