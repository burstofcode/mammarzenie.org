<?php

namespace App\Services;

class PathService
{
    public static function image($image)
    {
        return  app('wp.theme')->getUrl('dist/images/' . $image);
    }

    public static function svg($svg)
    {
        return  app('wp.theme')->getUrl('dist/images/svg/' . $svg);
    }
}