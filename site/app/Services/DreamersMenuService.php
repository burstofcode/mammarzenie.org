<?php

namespace App\Services;

use App\Models\Wish;

/**
 * Class DreamersMenuService
 * @package App\Services
 */
class DreamersMenuService
{
    /**
     * @param $dreamer
     * @return mixed
     */
    protected function wishState($dreamer)
    {
        return Wish::where('dreamer_id', $dreamer->id)->get()[0]->wish_state_id;
    }

    /**
     * @param $dreamer
     * @return string
     */
    public function pendingMenuItem($dreamer)
    {
        $wish_state = $this->wishState($dreamer);
        if($wish_state == 1) {
            return 'active';
        }
    }

    /**
     * @param $dreamer
     * @return string
     */
    public function adoptedMenuItem($dreamer)
    {
        $wish_state = $this->wishState($dreamer);
        if($wish_state == 2) {
            return 'active';
        }
    }

    /**
     * @param $dreamer
     * @return string
     */
    public function fulfilledMenuItem($dreamer)
    {
        $wish_state = $this->wishState($dreamer);
        if($wish_state == 4) {
            return 'active';
        }
    }

    /**
     * @param $dreamer
     * @return string
     */
    public function goneMenuItem($dreamer)
    {
        $wish_state = $this->wishState($dreamer);
        if($wish_state == 5) {
            return 'active';
        }
    }
}