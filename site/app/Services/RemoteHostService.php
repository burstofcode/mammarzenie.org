<?php

namespace App\Services;

/**
 * Class RemoteHostService
 * @package App\Services
 */
class RemoteHostService
{
    /**
     * @return string
     */
    public static function dreamerImagePath()
    {
        return 'http://www3.mammarzenie.org/images/dreamers/';
    }

    /**
     * @return string
     */
    public static function eventImagePath()
    {
        return 'http://www3.mammarzenie.org/images/events/';
    }
}