<?php

namespace App\Services;

class ImageService
{
    /**
     * Returns partial path to dreamer photo
     *
     * This partial path is related with application
     * folder with images
     */
    public static function imagePathPart( $dreamer_id)
    {
        $code = '/';
        for ($ii = 0; $ii < strlen($dreamer_id) - 2; $ii++) {
            $code .= substr($dreamer_id, $ii, 1) . '/';
        }
        return $code;
    }
}