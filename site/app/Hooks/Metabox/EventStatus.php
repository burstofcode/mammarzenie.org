<?php

namespace App\Hooks\Metabox;

use Carbon\Carbon;
use Themosis\Hook\Hookable;
use Themosis\Support\Facades\Action;
use Themosis\Support\Facades\Metabox;

/**
 * Class EventStatus
 * @package App\Hooks\Metabox
 */
class EventStatus extends Hookable
{
    public function register()
    {

        Metabox::make('event_status', ['imprezy-cykliczne', 'nasze-akcje'])
            ->setContext('side')
            ->setCallback(function () {
                return view('metabox.event-status');
            })
            ->set();



        $this->currentStatus();
    }

    public function currentStatus()
    {

        Action::add('init', function() {
            $post_id = app()->request->post;

            $now = Carbon::now()->format('Ymd');
            $start_date = get_field('start_date', $post_id);
            $end_date = get_field('end_date', $post_id);


            if ($start_date < $now AND $end_date < $now) {
                $status = 'past';
            } elseif ($start_date > $now AND $end_date > $now) {
                $status = 'future';
            } elseif ($start_date <= $now AND $now <= $end_date) {
                $status = 'live';
            } else {
                $status = '';
            }
            wp_set_object_terms( $post_id, $status, 'event_status');
        });
    }
}
