<?php

namespace App\Hooks\Taxonomy;

use Themosis\Hook\Hookable;
use Themosis\Support\Facades\Taxonomy;

/**
 * Class EventStatus
 * @package App\Hooks\Taxonomy
 */
class EventStatus extends Hookable
{
    /**
     * Extend WordPress.
     */
    public function register()
    {
        $this->taxonomy();
    }

    public function taxonomy()
    {
        Taxonomy::make('event_status', ['nasze-akcje', 'imprezy-cykliczne'], __('Event Statuses'), __('Event Status'))
            ->setArguments([
                'public' => true,
                'show_in_nav_menus'  => true,
                'hierarchical' => false,
                'show_tagcloud' => false,
                'show_in_quick_edit' => false,
                'show_ui' => true,
                'show_admin_column' => true,
            ])
            ->set();
    }
}
