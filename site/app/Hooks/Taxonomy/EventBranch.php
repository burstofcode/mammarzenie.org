<?php

namespace App\Hooks\Taxonomy;

use Themosis\Hook\Hookable;
use Themosis\Support\Facades\Taxonomy;
use Themosis\Support\Facades\TaxonomyField;

class EventBranch extends Hookable
{
    /**
     * Extend WordPress.
     */
    public function register()
    {
        $this->taxonomy();
    }

    public function taxonomy()
    {
        Taxonomy::make('event_branch', ['nasze-akcje', 'imprezy-cykliczne'], __('Branches'), __('Branch'))
            ->setArguments([
                'public' => true,
                'show_in_nav_menus'  => false,
                'hierarchical' => true,
                'show_tagcloud' => false,
                'show_in_quick_edit' => true,
//                'show_admin_column' => true
            ])
            ->set();
    }
}
