<?php

namespace App\Hooks;

use Themosis\Hook\Hookable;
use Themosis\Support\Facades\Filter;

/**
 * Class Gutenberg
 * @package App\Hooks
 */
class Gutenberg extends Hookable
{
    /**
     * Extend WordPress.
     */
    public function register()
    {
        Filter::add( 'block_categories', [ $this, 'categories' ], 10, 2 );
        // Filter::add('allowed_block_types', [$this, 'restrict_blocks']);

        Filter::add( 'gutenberg_can_edit_post_type', [$this, 'disableGutenbergOnSpecificIds'], 10, 2 );
        Filter::add( 'use_block_editor_for_post_type', [$this, 'disableGutenbergOnSpecificIds'], 10, 2 );


        $this->addColorPaletteThemeSupport();
        $this->addFontSizeThemeSupport();
    }

    /**
     * Create new categories for Gutenberg blocks
     *
     * @param $categories
     * @param $post
     * @return array
     */
    public function categories( $categories, $post )
    {
        return array_merge(
            $categories,
            [
                [
                    'slug' => 'basic',
                    'title' => __( 'Basic Blocks' ),
                    'description' => __( 'Basic blocks' )
                ],
                [
                    'slug' => 'advanced',
                    'title' => __( 'Advanced Blocks' ),
                    'description' => __( 'Advanced blocks' )
                ],
                [
                    'slug' => 'sections',
                    'title' => __( 'Page Sections' ),
                    'description' => __( 'Custom page sections' )
                ],
                [
                    'slug' => 'social',
                    'title' => __( 'Social Media' ),
                    'description' => __( 'Social media blocks' )
                ]
            ]
        );
    }

    /**
     * Restring usage of specific blocks in certain post types
     *
     * @param $allowed_blocks
     * @param $post
     * @return array
     */
    function restrict_blocks( $allowed_blocks, $post )
    {
        $allowed_blocks = array (
            'core/image',
            'core/paragraph',
            'core/heading',
            'core/list'
        );
        if ( $post->post_type === 'page' ) {
            $allowed_blocks[] = 'core/shortcode';
        }
        return $allowed_blocks;
    }

    /**
     * Adds support for editor color palette.
     */
    public function addColorPaletteThemeSupport()
    {
        add_theme_support( 'editor-color-palette', [
            [
                'name' => 'Primary',
                'slug' => 'primary',
                'color' => '#27AE61',
            ],
            [
                'name' => 'Secondary',
                'slug' => 'secondary',
                'color' => '#495057',
            ],
            [
                'name' => 'Success',
                'slug' => 'success',
                'color' => '#0f834d',
            ],
            [
                'name' => 'Info',
                'slug' => 'info',
                'color' => '#3D9CD2',
            ],
            [
                'name' => 'Warning',
                'slug' => 'warning',
                'color' => '#ffc107',
            ],
            [
                'name' => 'Danger',
                'slug' => 'danger',
                'color' => '#e2401c',
            ],
            [
                'name' => 'Dark',
                'slug' => 'dark',
                'color' => '#1c1c1c',
            ],
            [
                'name' => 'Gray Dark',
                'slug' => 'gray-dark',
                'color' => '#343a40',
            ],
            [
                'name' => 'Light',
                'slug' => 'light',
                'color' => '#ffffff',
            ],
            [
                'name' => 'Gray Lighter',
                'slug' => 'gray-lighter',
                'color' => '#f6f7f9',
            ],
            [
                'name' => 'Gray Light',
                'slug' => 'gray-light',
                'color' => '#ececec',
            ],
        ] );
    }

    /**
     * Adds support for editor font sizes.
     */
    public function addFontSizeThemeSupport()
    {
        add_theme_support( 'editor-font-sizes', [
            [
                'name' => __( 'Heading 1' ),
                'shortName' => 'H1',
                'size' => 58,
                'slug' => 'h1'
            ],
            [
                'name' => __( 'Heading 2' ),
                'shortName' => 'H2',
                'size' => 34,
                'slug' => 'h2'
            ],
            [
                'name' => __( 'Heading 3' ),
                'shortName' => 'H3',
                'size' => 28,
                'slug' => 'h3'
            ],
            [
                'name' => __( 'Heading 4' ),
                'shortName' => 'H4',
                'size' => 24,
                'slug' => 'h4'
            ],
            [
                'name' => __( 'Heading 5' ),
                'shortName' => 'H5',
                'size' => 20,
                'slug' => 'h5'
            ],
            [
                'name' => __( 'Heading 6' ),
                'shortName' => 'H6',
                'size' => 18,
                'slug' => 'h6'
            ],
            [
                'name' => __( 'Paragraph' ),
                'shortName' => 'p',
                'size' => 15,
                'slug' => 'p'
            ],
            [
                'name' => __( 'Large' ),
                'shortName' => 'lg',
                'size' => 22,
                'slug' => 'large'
            ],
            [
                'name' => __( 'Small' ),
                'shortName' => 'sm',
                'size' => 14,
                'slug' => 'small'
            ],
            [
                'name' => __( 'Extra Small' ),
                'shortName' => 'xs',
                'size' => 12,
                'slug' => 'extra-small'
            ],
        ] );
    }

    /**
     * Templates and Page IDs without editor
     * @param bool $id
     * @return bool
     */
    public function findSpecificIds($id = false)
    {
        $excluded_templates = array(
            'templates/modules.php',
            'templates/contact.php'
        );

        $excluded_ids = array(
            119,
            24475,
        );

        if( empty( $id ) )
            return false;

        $id = intval( $id );
        $template = get_page_template_slug( $id );

        return in_array( $id, $excluded_ids ) || in_array( $template, $excluded_templates );
    }

    /**
     * Disable Gutenberg by template
     */
    public function disableGutenbergOnSpecificIds($can_edit, $post_type)
    {
        if( ! ( is_admin() && !empty( $_GET['post'] ) ) )
            return $can_edit;

        if( $this->findSpecificIds( $_GET['post'] ) )
            $can_edit = false;

        return $can_edit;
    }
}