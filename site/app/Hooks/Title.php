<?php

namespace App\Hooks;

use Themosis\Hook\Hookable;
use Themosis\Support\Facades\Filter;

class Title extends Hookable
{
    /**
     * Register the titles.
     */
    public function register()
    {
        Filter::add('pre_get_document_title', function() {
            $title = 'Fundacja Mam Marzenie';
                return $title;
            }, 10, 1);

        Filter::add('wp_title', function() {
            $title = 'Fundacja Mam Marzenie';
            return $title;
        }, 10, 3);

//        Filter::add( 'bloginfo', function( $string, $show )
//        {
//            if ('description' == $show) {
//                $string = 'Some New Description';
//            }
//            return $string;
//        }, 10, 2 );

    }
}
