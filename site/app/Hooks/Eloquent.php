<?php

namespace App\Hooks;

use Themosis\Hook\Hookable;
use WPEloquent\Core\Laravel;
use WPEloquent\Model\Post;

class Eloquent extends Hookable
{
    protected $connection = 'mysql';

    /**
     * Extend WordPress.
     */
    public function register()
    {
        require_once( app()->basePath() . '/vendor/rafflex/wordpress-eloquent/vendor/autoload.php');

        Laravel::connect([
            'global' => true,
            'multiple_connections' => true,
            'config' => [

                'database' => [
                    'default' => [
                        'user'     => env('DATABASE_USER', ''),
                        'password' => env('DATABASE_PASSWORD', ''),
                        'name'     => env('DATABASE_NAME', ''),
                        'host'     => env('DATABASE_HOST', '127.0.0.1'),
                        'port'     => env('DATABASE_PORT', '3306'),
                        'prefix'   => env('DATABASE_PREFIX', 'wp_'),
                    ],
                    'mysql_app' => [
                        'user'     => env('DATABASE_APP_USER', ''),
                        'password' => env('DATABASE_APP_PASSWORD', ''),
                        'name'     => env('DATABASE_APP_NAME', ''),
                        'host'     => env('DATABASE_APP_HOST', '127.0.0.1'),
                        'port'     => env('DATABASE_APP_PORT', '3306'),
                        'prefix' => '',
                    ],
                ],
            ],

            // enable events
            'events' => false,

            // enable query log
            'log'    => true
        ]);
    }
}
