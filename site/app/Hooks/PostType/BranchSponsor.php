<?php

namespace App\Hooks\PostType;

use App\Models\Branch;
use Themosis\Hook\Hookable;
use Themosis\Support\Facades\PostType;

/**
 * Class BranchSponsor
 * @package App\Hooks\PostType
 */
class BranchSponsor extends Hookable
{
    public function register()
    {
        $branches = Branch::list();

        foreach($branches as $branch) {
            PostType::make($branch->slug . '-sponsor', __('Sponsorzy (' . $branch->city . ')'), __('Sponsor (' . $branch->city . ')'))
                ->setLabels([
                    'all_items' => __('Wszyscy sponsorzy (' . $branch->city . ')'),
                    'view_item' => __('Pokaż sponsora (' . $branch->city . ')'),
                    'add_new' => __('Dodaj sponsora (' . $branch->city . ')'),
                    'add_new_item' => __('Dodaj nowego sponsora (' . $branch->city . ')'),
                ])
                ->setArguments([
                    'public' => true,
                    'has_archive' => true,
                    'supports' => ['title', 'thumbnail', 'page-attributes'],
                    'enable_gutenberg' => false,
                    'rewrite' => [
                        'slug' => $branch->slug . '-sponsor',
                        'with_front' => false,
                    ],
                ])
                ->set();
        }
    }
}
