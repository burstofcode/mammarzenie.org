<?php

namespace App\Hooks\PostType;

use App\Models\Branch;
use Themosis\Hook\Hookable;
use Themosis\Support\Facades\PostType;

class BranchNews extends Hookable
{
    public function register()
    {
        $branches = Branch::list();

        foreach($branches as $branch) {
            PostType::make($branch->slug, __('Aktualności (' . $branch->city . ')'), __('Aktualność (' . $branch->city . ')'))
                ->setLabels([
                    'all_items' => __('Wszystkie aktualności (' . $branch->city . ')'),
                    'view_item' => __('Pokaż aktualność (' . $branch->city . ')'),
                    'add_new' => __('Dodaj aktualność (' . $branch->city . ')'),
                    'add_new_item' => __('Dodaj nową aktualność (' . $branch->city . ')'),
                ])
                ->setArguments([
                    'public' => true,
                    'has_archive' => true,
                    'supports' => ['title', 'editor', 'thumbnail', 'page-attributes'],
                    'enable_gutenberg' => true,
                    'rewrite' => [
                        'slug' => 'aktualnosci/' . $branch->slug,
                        'with_front' => false,
                    ],
                ])
                ->set();
        }
    }
}
