<?php

namespace App\Hooks\PostType;

use Themosis\Hook\Hookable;
use Themosis\Support\Facades\PostType;

/**
 * Class RecurringEvent
 * @package App\Hooks\PostTypes
 */
class RecurringEvent extends Hookable
{
    /**
     * Extend WordPress.
     */
    public function register()
    {
        $recurring_events = PostType::make('imprezy-cykliczne', 'Imprezy cykliczne', 'Impreza cykliczna')
            ->setLabels([
                'all_items' => 'Wszystkie imprezy cykliczne',
                'view_item' =>  'Pokaż imprezę cykliczną',
                'add_new' => 'Dodaj imprezę cykliczną',
                'add_new_item' => 'Dodaj nową imprezę cykliczną',
            ])
            ->setArguments([
                'public' => true,
                'has_archive' => true,
                'hierarchical' => true,
                'supports' => ['title', 'editor', 'thumbnail', 'page-attributes', 'excerpt'],
                'enable_gutenberg' => true,
                'rewrite' => [
                    'slug' => 'co-robimy/imprezy-cykliczne',
                    'with_front' => false,
                ],
            ])

            ->set();
    }
}
