<?php

namespace App\Hooks\PostType;

use Themosis\Hook\Hookable;
use Themosis\Support\Facades\PostType;

class MainSponsor extends Hookable
{
    /**
     * Extend WordPress.
     */
    public function register()
    {
        PostType::make('sponsor', __('Sponsors'), __('Sponsor'))
            ->setLabels([
                'all_items' => __('Wszyscy sponsorzy'),
                'view_item' => __('Pokaż sponsora'),
                'add_new' => __('Dodaj sponsora'),
                'add_new_item' => __('Dodaj nowego sponsora'),
            ])
            ->setArguments([
                'public' => true,
                'has_archive' => true,
                'supports' => ['title', 'thumbnail'],
                'enable_gutenberg' => false,
            ])
            ->set();
    }
}
