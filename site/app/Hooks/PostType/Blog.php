<?php

namespace App\Hooks\PostType;

use Themosis\Hook\Hookable;
use Themosis\Support\Facades\Action;
use Themosis\Support\Facades\PostType;

/**
 * Class Posts
 * @package App\Hooks\PostType
 */
class Blog extends Hookable
{
    /**
     * Extend WordPress.
     */
    public function register()
    {
        PostType::make('blog', 'Blog', 'Blog')
            ->setLabels([
                'all_items' => 'Wszystkie wpisy',
                'view_item' =>  'Pokaż wpis',
                'add_new' => 'Dodaj wpis',
                'add_new_item' => 'Dodaj nowy wpis',
            ])
            ->setArguments([
                'public' => true,
                'has_archive' => false,
                'hierarchical' => true,
                'supports' => ['title', 'editor', 'thumbnail', 'page-attributes', 'excerpt'],
                'enable_gutenberg' => true,
                'rewrite' => [
                    'slug' => 'blog',
                    'with_front' => false,
                ],
            ])
            ->set();
    }
}
