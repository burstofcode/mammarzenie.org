<?php

namespace App\Hooks\PostType;

use Themosis\Hook\Hookable;
use Themosis\Support\Facades\PostType;

class BranchPage extends Hookable
{
    public function register()
    {
        PostType::make('strony-oddzialu', 'Strony oddziałów', 'Strona oddziału')
            ->setLabels([
                'all_items' => 'Wszystkie strony',
                'view_item' =>  'Pokaż stronę',
                'add_new' => 'Dodaj stronę',
                'add_new_item' => 'Dodaj nową stronę',
            ])
            ->setArguments([
                'public' => true,
                'has_archive' => false,
                'hierarchical' => true,
                'supports' => ['title'],
                'enable_gutenberg' => false,
                'show_in_rest' => false,
                'rewrite' => [
                    'slug' => 'oddzial',
                    'with_front' => false,
                ],
            ])
            ->set();
    }
}
