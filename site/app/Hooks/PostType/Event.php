<?php

namespace App\Hooks\PostType;

use Themosis\Hook\Hookable;
use Themosis\Support\Facades\PostType;

/**
 * Class Event
 * @package App\Hooks\PostTypes
 */
class Event extends Hookable
{
    public function register()
    {
        PostType::make('nasze-akcje', 'Nasze akcje', 'Nasza akcja')
            ->setLabels([
                'all_items' => 'Wszystkie akcje',
                'view_item' =>  'Pokaż akcję',
                'add_new' => 'Dodaj akcję',
                'add_new_item' => 'Dodaj nową akcję',
            ])
            ->setArguments([
                'public' => true,
                'has_archive' => true,
                'hierarchical' => true,
                'supports' => ['title', 'editor', 'thumbnail', 'page-attributes', 'excerpt'],
                'enable_gutenberg' => true,
                'show_in_rest' => true,
                'rewrite' => [
                    'slug' => 'co-robimy/nasze-akcje'
                ],
            ])
            ->set();
    }
}
