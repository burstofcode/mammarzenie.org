<?php

namespace App\Hooks;

use Themosis\Hook\Hookable;

class AdminColumn extends Hookable
{
    /**
     * Extend WordPress.
     */
    public function register()
    {
        // Column Name
        $this->addStartDate();
        $this->editStartDate();
        $this->populateStartDate();
        $this->saveStartDate();
    }

    public function addStartDate()
    {
        Filter::add('manage_edit-imprezy-cykliczne_columns', function() {

        });
        add_filter('manage_edit-page_columns', 'tessa_add_acf_columns');
        function tessa_add_acf_columns($columns) {
            $columns = array(
                'cb' => '<input type="checkbox" />',
                'title' => 'Title',
                'topaz' => 'Topaz',
                'date' => 'Date',
            );
            return $columns;
        }
    }

    public function editStartDate(){
        //
    }

    public function populateStartDate()
    {
        //
    }

    public function saveStartDate(){
        //
    }
}
