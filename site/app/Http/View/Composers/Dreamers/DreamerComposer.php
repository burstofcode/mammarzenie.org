<?php

namespace App\Http\View\Composers\Dreamers;

use App\Models\Branch;
use App\Models\Dreamer;
use App\Http\Controllers\Dreamer\DreamerController;

/**
 * Class DreamerComposer
 * @package App\Http\View\Composers\Dreamers
 */
class DreamerComposer
{
    /**
     * Branches
     */
    public function branches($view)
    {
        $view->with('branches', Branch::all());
    }
}