<?php

namespace App\Http\View\Composers\Dreamers;

use App\Models\Dreamer;
use App\Http\Controllers\Dreamer\DreamerController;

class PendingComposer
{
    /**
     * Bind header navigation to view.
     */
//    public function title($view)
//    {
//        $view->with('title', __('Marzenia oczekujące'));
//    }

    /**
     * Bind header navigation to view.
     */
    public function dreamers($view)
    {
        $view->with('dreamers', Dreamer::pendingWish()->paginate(20));
    }
}