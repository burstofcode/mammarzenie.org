<?php

namespace App\Http\View\Composers;

class FilterComposer
{
    /**
     * Date filter
     */
    public function date($view)
    {
        $view->with('date_filter', $this->isActive());
    }

    /**
     * Dream category filter
     */
    public function dreamCategory($view)
    {
        $view->with('dream_category_filter', $this->isActive());
    }

    /**
     * Event type filter
     */
    public function eventType($view)
    {
        $view->with('event_type_filter', $this->isActive());
    }

    /**
     * Branch filter
     */
    public function branch($view)
    {
        $view->with('branch_filter', $this->isActive());
    }

    /**
     * Event time frame filter
     */
    public function eventTimeFrame($view)
    {
        $view->with('event_time_frame_filter', $this->isActive());
    }

    /**
     * View type filter
     */
    public function viewType($view)
    {
        $view->with('view_type_filter', $this->isActive());
    }

    /**
     * Return component if variable is passed to view
     */
    public function isActive()
    {
        return true;
    }
}