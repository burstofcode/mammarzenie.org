<?php

namespace App\Http\View\Composers;

use Log1x\Navi\Navi;

class NaviComposer
{
    /**
     * Bind header navigation to view.
     */
    public function header($view)
    {
        $view->with('navigation', $this->headerNavi());
    }

    /**
     * Bind footer navigation to view.
     */
    public function footer($view)
    {
        $view->with('navigation', $this->footerNavi());
    }

    /**
     * Bind footer navigation to view.
     */
    public function aboutUs($view)
    {
        $view->with('navigation', $this->aboutUsNavi());
    }

    /**
     * Create a new menu in header.
     */
    public function headerNavi()
    {
        return (new Navi())->build('menu-header')->toArray();
    }

    /**
     * Create a new menu in footer.
     */
    public function footerNavi()
    {
        return(new Navi())->build('menu-footer')->toArray();
    }

    /**
     * Create a new menu on about us pages.
     */
    public function aboutUsNavi()
    {
        return(new Navi())->build('menu-about-us')->toArray();
    }
}