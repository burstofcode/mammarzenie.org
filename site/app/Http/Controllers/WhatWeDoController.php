<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use App\Services\PlaceholderService;
use App\Services\TitleService;
use Corcel\Model\Post;
use Illuminate\Http\Request;

/**
 * Class WhatWeDoController
 * @package App\Http\Controllers
 */
class WhatWeDoController extends Controller
{
    /**
     * @param $event
     * @param $branch
     * @param $event_status
     * @param $pagination
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Container\EntryNotFoundException
     */
    public function index( $event, $branch, $event_status, $pagination, Request $request)
    {
        $title = TitleService::title($request);
        $the_event = $event;
        $branches = Branch::all();
        $the_branch = $branch;
        $the_event_status = $event_status;
        $the_pagi = $pagination;
        $placeholder = $this->placeholder();

        $posts = $this->mainQuery($event, $branch, $event_status)->paginate($pagination);

        return view('what-we-do.archive', compact('posts', 'title', 'event',
            'the_event', 'branches', 'the_branch', 'the_event_status', 'the_pagi', 'placeholder'));
    }

    public function mainQuery($event, $branch, $event_status)
    {
        return $this->queryTimeframe($event, $branch, $event_status);
    }

    /**
     * @param $event
     * @return mixed
     */
    public function queryType($event)
    {
        if ($event == 'wszystkie') {
            return Post::published()->newest()->whereIn('post_type', ['imprezy-cykliczne', 'nasze-akcje']);
        } elseif ($event == 'nasze-akcje') {
            return Post::published()->newest()->whereIn('post_type', ['nasze-akcje']);
        } elseif ($event == 'imprezy-cykliczne') {
            return Post::published()->newest()->whereIn('post_type', ['imprezy-cykliczne']);
        }
    }

    /**
     * @param $event
     * @param $branch
     * @return mixed
     */
    public function queryBranch( $event, $branch)
    {
        if($branch == 'cala-polska') {
            return $this->queryType($event);
        } else {
            return $this->queryType($event)->taxonomy('event_branch', $branch);
        }
    }

    /**
     * @param $event
     * @param $branch
     * @param $event_status
     * @return mixed
     */
    public function queryTimeframe( $event, $branch, $event_status)
    {
        if ( $event_status == 'trwajace' ) {
            return $this->queryBranch( $event, $branch )->taxonomy( 'event_status', 'live' );
        } elseif ( $event_status == 'zakonczone' ) {
            return $this->queryBranch( $event, $branch )->taxonomy( 'event_status', 'past' );
        } else {
            return $this->queryBranch( $event, $branch )->taxonomy( 'event_status', 'past' );
        }
    }

    /**
     * @return mixed
     * @throws \Illuminate\Container\EntryNotFoundException
     */
    public function placeholder()
    {
        return PlaceholderService::placeholder270x200();
    }
}
