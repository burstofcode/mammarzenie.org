<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Branch;
use App\Models\Dreamer;
use App\Models\WishCategory;
use App\Models\WishState;
use App\Services\TitleService;
use Corcel\Model\Post;
use Illuminate\Http\Request;

class DreamerController extends Controller
{
    public function index($state, $branch, $category, $order, $pagination, Request $request)
    {
        $the_state = $state;
        $the_branch = $branch;
        $the_cat = $category;
        $the_pagi = $pagination;
        $the_order = $order;
        $branches = Branch::all();
        $dreamers = $this->mainQuery($state, $branch, $category)->orderBy('id', $order)->paginate($pagination);
        $wish_cats = WishCategory::all();
        $title = TitleService::title($request);
        $dreamer_image_path = $this->dreamerImagePath();

        return view('dreamers.front.index', compact('dreamers', 'wish_cats', 'branches', 'the_state', 'the_branch', 'the_cat', 'the_order', 'the_pagi', 'title', 'dreamer_image_path'));
    }

    /**
     * @param $dreamer_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\Factory|\Illuminate\View\View
     */
    public function show($dreamer_id)
    {
        $the_id = explode("-", $dreamer_id);
        $dreamer = Dreamer::where('id', $the_id[0])->first();
        $dreamers_pending = Dreamer::randomPendingWishes();
        $dreamers_adopted = Dreamer::randomAdoptedWishes();
        $dreamers_fulfilled = Dreamer::randomFulfilledWishes();
        $dreamers_gone = Dreamer::randomGoneWishes();
        $title = $dreamer->wish->contents;
        $dreamer_image_path = $this->dreamerImagePath();
        $event_image_path = $this->eventImagePath();
        $next_id = $this->pendingNext($the_id);

        return view('dreamers.front.show', compact('dreamer', 'dreamers_pending', 'dreamers_adopted', 'dreamers_fulfilled', 'dreamers_gone', 'title', 'dreamer_image_path', 'event_image_path', 'next_id'));
    }

    /**
     * Returns list of dreamers
     */
    public function mainQuery($state, $branch, $category)
    {
        // Get the id of a branch based on $slug variable from Url
        $b_id = Branch::where( 'slug', $branch )->value( 'id' );

        if ($branch == 'cala-polska') {
            return Dreamer::whereHas('wish', function ($query) use ($state) {
                $state_id = WishState::where( 'ident', $state )->value( 'id' );
                $query->where('wish_state_id', $state_id);
            })
                ->whereHas('wish', function ($query) use($category) {
                if ($category == 'wszystkie') {
                    $query->where('wish_category_id', '!=', 6);
                } else {
                    $cat_id = WishCategory::where('ident', $category)->value( 'id' );
                    $query->where('wish_category_id', $cat_id);
                }
            });
        } else {
            // Get list of dreamers based on the $slug variable from Url
            return Dreamer::whereHas('wish', function ($query) use($state) {
                $state_id = WishState::where( 'ident', $state )->value( 'id' );
                $query->where('wish_state_id', $state_id);
            })
                ->where( 'branch_id', $b_id )->whereHas('wish', function ($query) use($category) {
                if ($category == 'wszystkie') {
                    $query->where('wish_category_id', '!=', 6);
                } else {
                    $cat_id = WishCategory::where( 'ident', $category )->value( 'id' );
                    $query->where('wish_category_id', $cat_id);
                }
            });
        }
    }

    public function indexGone($pagination, Request $request)
    {
        $the_pagi = $pagination;
        $dreamers = $this->goneQuery()->orderBy('id', 'desc')->paginate($pagination);
        $title = TitleService::title($request);
        // Page with ID: 24475, name: "Byli z nami", slug: "byli-z-nami__edit-only"
        $content = Post::find(24475)->post_content;
        $dreamer_image_path = $this->dreamerImagePath();

        return view('dreamers.front.index-gone', compact('dreamers', 'the_pagi', 'title', 'content', 'dreamer_image_path'));
    }

    public function goneQuery()
    {
        return Dreamer::whereHas('wish', function ($query) {
            $query->where('wish_state_id', 5);
        });
    }

    public function pendingNext($the_id)
    {
        $current = Dreamer::where('id', $the_id[0])->first();
        $next_id = Dreamer::where('id', '<', $current->id)->orderBy('id', 'desc')->first();
        return $next_id;
    }
}
