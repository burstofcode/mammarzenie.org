<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use App\Models\Volunteer;
use Corcel\Acf\AdvancedCustomFields;
use Corcel\Model\Post;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

/**
 * Class BranchController
 * @package App\Http\Controllers
 */
class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $branches = Branch::list();

        return view('branches.index', compact('branches'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\Factory|\Illuminate\View\View
     */
    public function show()
    {
        $branch = trim(request()->getRequestUri(), '/');
        $id = Post::where('post_name', $branch)->first()->ID;
        $branch_id = Branch::where('slug', $branch)->get(['id'])[0]->id;
        $title = get_post($id)->post_title;

        $content = get_field('content', $id);
        $volunteers = get_field('volunteers', $id);
        $vol_btn = get_field('more_volunteers_button', $id);
        $news_btn = get_field('more_news_button', $id);
        $vol_title = get_field('volunteers_title', $id);
        $posts = Post::published()->newest()->whereIn('post_type', [$branch])->take(4)->get();
        $all_volunteers = Volunteer::where('branch_id', $branch_id)->get();
        $placeholder = $this->postPlaceholder();

        return view('pages.branch', compact('id', 'title', 'content', 'volunteers', 'vol_btn', 'news_btn', 'vol_title', 'posts', 'all_volunteers', 'placeholder'));

    }
}
