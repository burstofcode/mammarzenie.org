<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use App\Services\PlaceholderService;
use App\Services\TitleService;
use Corcel\Model\Post;
use Illuminate\Http\Request;

/**
 * Class NewsController
 * @package App\Http\Controllers
 */
class NewsController extends Controller
{
    public function index($branch, $order, $pagination, Request $request)
    {
        $title = TitleService::title($request);
        $the_branch = $branch;
        $the_order = $order;
        $branches = Branch::all();
        $the_pagi = $pagination;
        $placeholder = $this->placeholder();

        $posts = $this->mainQuery($branch, $order)->paginate($pagination);

        return view('news.archive', compact('title',  'posts', 'the_order', 'branches',
            'the_branch', 'the_pagi', 'placeholder'));
    }

    public function mainQuery($branch, $order)
    {
        return $this->queryBranch($branch, $order);
    }

    public function queryOrder($order)
    {
        if ($order == 'desc') {
            return Post::published()->newest();
        } elseif('asc') {
            return Post::published()->oldest();
        }
    }

    public function queryBranch($branch, $order)
    {
        $branches = Branch::all();
        $branch_slugs = array();
        if ($branch == 'cala-polska') {

            foreach($branches as $item) {
                array_push($branch_slugs, $item->slug);
            }

            return $this->queryOrder($order)->whereIn('post_type', $branch_slugs);

        } else {
            foreach($branches as $item) {
                if ($branch == $item->slug) {
                    return $this->queryOrder($order)->whereIn('post_type', [$item->slug]);
                }
            }
        }
    }

    public function placeholder()
    {
        return PlaceholderService::placeholder270x200();
    }
}
