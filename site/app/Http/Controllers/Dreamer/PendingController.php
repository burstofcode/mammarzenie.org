<?php

namespace App\Http\Controllers\Dreamer;

use App\Models\Branch;
use App\Models\Dreamer;
use App\Models\WishCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PendingController extends Controller
{
//    /**
//     * Display a listing of the resource.
//    */
//    public function index()
//    {
//        return view('pages.dreamer.front.index');
//    }
//
//    /**
//     * Get the list of dreamers based on branch
//     *
//     * Don't use model name in function parenthesis (Branch $slug), since the slug
//     * is populated through Dreamers model and not by Branch model.
//     * @param $slug
//     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\Factory|\Illuminate\View\View
//     */
//    public function showBranch($slug)
//    {
//        // Get the id of a branch based on $slug variable from Url
//        $b_id = Branch::where('slug', $slug)->value('id');
//        // Get list of dreamers based on the $slug variable from Url
//        $dreamers = Dreamer::pendingWish()->where('branch_id', $b_id)->paginate(20);
//        // Return the list
//        return view('pages.dreamer.front.branch', compact('dreamers'));
//    }

//    public function filters()
//    {
//        $branch = 'krakow';
//        $category = 'byc';
//        $order = 'asc';
//        $request = Request::capture();
//
//        $dreamer_type = strlen('poczekalnia');
//        $the_branch = DreamerController::branchFilter($request, $dreamer_type, $branch);
//        $the_cat = DreamerController::categoryFilter($request, $dreamer_type, $branch, $category);
//        $the_order = DreamerController::orderFilter($request, $order);
//
//        $url_builder = url('/marzyciele') . '/' . $the_branch . '/' . $the_cat . '/' . $the_order . '/';
////        dd($url_builder);
//        return view('pages.dreamer.front.branch', compact('url_builder', 'branch'));
//
//    }

//    public static function branchFilter(Request $request, $dreamer_type, $branch)
//    {
//        $output = '<select class="dropdown__select" id="filter-branch">';
//        $output .= '<option data-placeholder="true"></option>';
//        $output .= '<option selected value="cala-polska">Cała Polska</option>';
//        foreach( Branch::list() as $branch ) {
//        $output .= '<option value = "' . $branch->slug . '"';
//        if ( DreamerController::branchFilter($request, $dreamer_type, $branch) == $branch->slug ) {
//            $output .= 'selected';
//        }
//            $output .= '>' . $branch->city . '</option >';
//
//        }
//        $output .= '</select>';
//
//        return $output;
//    }

//    public static function wishCategoryFilter(Request $request, $dreamer_type, $branch, $category)
//    {
//        $output = '<select class="dropdown__select" id="filter-dreamers-category">';
//        $output .= '<option data-placeholder="true"></option>';
//
//        foreach(WishCategory::all() as $wish) {
//            if(DreamerController::categoryFilter($request, $dreamer_type, $branch, $category) == $wish->ident) {
//                $output .= '<option value="' . $wish->ident . '">' . $wish->name . '</option>';
//            }
//        }
//
//        $output .= '</select>';
//
//        return $output;
//    }
}
