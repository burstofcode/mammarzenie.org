<?php

namespace App\Http\Controllers\Dreamer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class SignUpFormController
 * @package App\Http\Controllers\Dreamer
 */
class SignUpFormController extends Controller
{
    public function index(Request $request)
    {
        $title = $this->title($request);
        $branches = $this->branches();

        return view('dreamers.front.signup-form.index', compact('title', 'branches'));
    }
}
