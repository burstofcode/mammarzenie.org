<?php

namespace App\Http\Controllers;

use Corcel\Model\Post;
use Illuminate\Http\Request;

/**
 * Class BlogController
 * @package App\Http\Controllers
 */
class BlogController extends Controller
{
    public function index($pagination, Request $request)
    {
        $title = $this->title($request);
        $posts = $this->mainQuery()->paginate($pagination);
        $the_pagi = $pagination;
        $placeholder = $this->postPlaceholder();

        return view('blog.archive', compact('title', 'posts', 'the_pagi', 'placeholder'));
    }

    public function show()
    {
        $post = get_post();

        return view('blog.single', compact('post'));
    }

    protected function mainQuery()
    {
        return Post::published()->newest()->whereIn('post_type', ['blog']);
    }

    protected function singleQuery($slug)
    {
        return Post::where('post_name', $slug)->get();
    }
}
