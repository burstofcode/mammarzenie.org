<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use App\Services\PlaceholderService;
use App\Services\RemoteHostService;
use App\Services\TitleService;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Themosis\Core\Forms\FormHelper;
use Themosis\Core\Validation\ValidatesRequests;

/**
 * Class Controller
 * @package App\Http\Controllers
 */
class Controller extends BaseController
{
    use FormHelper;
    use ValidatesRequests;

    /**
     * @return mixed
     */
    protected function branches()
    {
        return Branch::list();
    }

    /**
     * @param Request $request
     * @return string|void
     */
    protected function title( Request $request)
    {
        return TitleService::title($request);
    }

    /**
     * @return mixed
     * @throws \Illuminate\Container\EntryNotFoundException
     */
    protected function postPlaceholder()
    {
        return PlaceholderService::placeholder270x200();
    }

    /**
     * @return string
     */
    protected function dreamerImagePath()
    {
        return RemoteHostService::dreamerImagePath();
    }

    /**
     * @return string
     */
    protected function eventImagePath()
    {
        return RemoteHostService::eventImagePath();
    }
}
