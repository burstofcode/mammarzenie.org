<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use WPEloquent\Model\Post;

class Taxonomy extends Model
{
    public function scopeTaxonomy ($query) {
        return Post::terms()->where('taxonomy', $taxonomy);
    }
}
