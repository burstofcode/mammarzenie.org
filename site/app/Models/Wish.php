<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Wish
 * @package App\Models
 */
class Wish extends Model
{
    protected $connection = 'mysql_app';

    protected $table = 'wish';

    protected $primaryKey = 'dreamer_id';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function wish_category()
    {
        return $this->belongsTo('App\Models\WishCategory', 'wish_category_id', 'id');
    }

    public function wish_state(){
        return $this->belongsTo('App\Models\WishState', 'wish_state_id', 'id');
    }
}

