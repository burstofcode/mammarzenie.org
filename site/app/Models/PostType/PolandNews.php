<?php

namespace App\Models\PostType;

use Illuminate\Database\Eloquent\Model;

class PolandNews extends Model
{
    protected $connection = 'mysql';

    protected $table = 'posts';

    protected $primaryKey = 'ID';

    public static function list()
    {
//        return self::where('post_status', 'publish')
//            ->orWhere('post_type', 'bialystok')
//            ->orWhere('post_type', 'bydgoszcz')
//            ->orWhere('post_type', 'gorzow-wlkp')
//            ->orWhere('post_type', 'katowice')
//            ->orWhere('post_type', 'kielce')
//            ->orWhere('post_type', 'krakow')
//            ->orWhere('post_type', 'lublin')
//            ->orWhere('post_type', 'lodz')
//            ->orWhere('post_type', 'olsztyn')
//            ->orWhere('post_type', 'opole')
//            ->orWhere('post_type', 'poznan')
//            ->orWhere('post_type', 'rzeszow')
//            ->orWhere('post_type', 'szczecin')
//            ->orWhere('post_type', 'trojmiasto')
//            ->orWhere('post_type', 'warszawa')
//            ->orWhere('post_type', 'wroclaw')
//            ->orderBy('post_date', 'DESC')
//            ->take(4)
//            ->get();

        return self::where([
            'post_status' => 'publish',
            'post_type'   => 'bialystok',
            'post_type'   => 'bydgoszcz',
            'post_type'   => 'gorzow-wlkp',
            'post_type'   => 'katowice',
            'post_type'   => 'kielce',
            'post_type'   => 'krakow',
            'post_type'   => 'lublin',
            'post_type'   => 'lodz',
            'post_type'   => 'olsztyn',
            'post_type'   => 'opole',
            'post_type'   => 'poznan',
            'post_type'   => 'rzeszow',
            'post_type'   => 'szczecin',
            'post_type'   => 'trojmiasto',
            'post_type'   => 'warszawa',
            'post_type'   => 'wroclaw',
        ])
            ->orderBy('post_date', 'DESC')
            ->take(4)
            ->get();
    }
}
