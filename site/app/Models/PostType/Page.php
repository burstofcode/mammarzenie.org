<?php

namespace App\Models\PostType;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $connection = 'mysql';

    protected $table = 'posts';

    protected $primaryKey = 'id';

    public static function allPages()
    {
        return self::where('post_type', 'page')->get();
    }

    public function scopeType ($query, $type = 'post') {
        return $query->where('post_type', $type);
    }
}
