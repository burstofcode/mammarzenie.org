<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WishState extends Model
{
//    protected $connection = 'mysql_app';

    protected $table = 'wish_state';

    protected $primaryKey = 'id';

    public function wish()
    {
        return $this->hasMany('App\Models\Wish', 'wish_state_id', 'id');
    }
}
