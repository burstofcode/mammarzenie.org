<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dreamer extends Model
{
    protected $connection = 'mysql_app';

    protected $table = 'dreamer';

    protected $primaryKey = 'id';

    /** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Dreamers - All
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    public function image()
    {
        return $this->hasOne('App\Models\DreamerImage', 'dreamer_id', 'id');
    }

    public function wish()
    {
        return $this->hasOne('App\Models\Wish');
    }

    public function branch()
    {
        return $this->belongsTo('Branch', 'branch_id', 'id');
    }

    public function getBranch($query)
    {
        return $this->belongsTo('Branch', 'branch_id', 'id');
    }

    public function dateOfBirth(){
        return $this->belongsTo('App\Models\Dreamer', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function event()
    {
        return $this->hasMany('App\Models\Event', 'dreamer_id');
    }

    /** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Dreamers - Random
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    /**
     * Returns eight random pending wishes
     */
    public static function randomPendingWishes()
    {
        return Dreamer::whereHas('wish', function ($query) {
            $query->where('wish_state_id', 1);
        })->get()->random(8);
    }

    /**
     * Returns eight random adopted wishes
     */
    public static function randomAdoptedWishes()
    {
        return Dreamer::whereHas('wish', function ($query) {
            $query->where('wish_state_id', 2);
        })->get()->random(8);
    }

    /**
     * Returns eight random fulfilled wishes
     */
    public static function randomFulfilledWishes()
    {
        return Dreamer::whereHas('wish', function ($query) {
            $query->where('wish_state_id', 4);
        })->get()->random(8);
    }

    /**
     * Returns eight random gone wishes
     */
    public static function randomGoneWishes()
    {
        return Dreamer::whereHas('wish', function ($query) {
            $query->where('wish_state_id', 5);
        })->get()->random(8);
    }

    /** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Wish States
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    /**
     * @param $query
     * @return mixed
     */
    public function scopePendingWish($query)
    {
        return $query->whereHas('wish', function ($query) {
            $query->where('wish_state_id', 1);
        });
    }

    /** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Wish Category
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    // Wish category: be (być)
    public function scopeBeWishCategory($query) {
        return $query->whereHas('wish', function ($query) {
            $query->where('wish_category_id', 1);
        });
    }
    // Wish category: get (dostać)
    public function scopeWishCategory($query, $category) {
        $cat_id = WishCategory::where( 'ident', $category )->value( 'id' );
        return $query->whereHas('wish', function ($query) use($cat_id) {
            $query->where('wish_category_id', $cat_id);
        });
    }
    // Wish category: meet (spotkać)
    public function scopeMeetWishCategory($query) {
        return $query->whereHas('wish', function ($query) {
            $query->where('wish_category_id', 3);
        });
    }
    // Wish category: see (zobaczyć)
    public function scopeSeeWishCategory($query) {
        return $query->whereHas('wish', function ($query) {
            $query->where('wish_category_id', 4);
        });
    }
    // Wish category: other (inne)
    public function scopeOtherWishCategory($query) {
        return $query->whereHas('wish', function ($query) {
            $query->where('wish_category_id', 5);
        });
    }
    // Wish category: all (wszystkie)
    public function scopeAllWishCategory($query) {
        return $query->whereHas('wish', function ($query) {
            $query->where('wish_category_id', 6);
        });
    }

    /** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Dreamers - Gone
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
}
