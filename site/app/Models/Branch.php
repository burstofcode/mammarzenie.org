<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
//    protected $connection = 'mysql_app';

    protected $table = 'oddzialy';

    protected $primaryKey = 'id';

    public static function list()
    {
        return Branch::where('parent_id', 41)->get();
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function volunteer(){
        return $this->hasMany('App\Models\Volunteer', 'branch_id', 'id');
    }
}
