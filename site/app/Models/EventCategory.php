<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventCategory extends Model
{
    protected $connection = 'mysql_app';

    protected $table = 'event_category';

    protected $primaryKey = 'id';

    public function event()
    {
        return $this->hasMany('App\Models\Event', 'event_category_id', 'id');
    }
}
