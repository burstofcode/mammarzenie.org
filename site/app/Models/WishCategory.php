<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class WishCategory
 * @package App\Models
 */
class WishCategory extends Model
{
    protected $connection = 'mysql_app';

    protected $table = 'wish_category';

    protected $primaryKey = 'id';

    public function wish()
    {
        return $this->hasMany('App\Models\Wish', 'wish_category_id', 'id');
    }

}
