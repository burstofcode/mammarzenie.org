<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventImage extends Model
{
    protected $connection = 'mysql_app';

    protected $table = 'event_image';

    protected $primaryKey = 'id';

    public function event()
    {
        return $this->belongsTo('App\Models\Event', 'id', 'event_id');
    }
}
