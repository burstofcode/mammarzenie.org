<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $connection = 'mysql_app';

    protected $table = 'event';

    protected $primaryKey = 'id';

    public function dreamer()
    {
        return $this->belongsTo('App\Models\Dreamer', 'id', 'dreamer_id');
    }

    public function event_category()
    {
        return $this->belongsTo('App\Models\EventCategory', 'event_category_id', 'id');
    }

    public function event_image()
    {
        return $this->hasMany('App\Models\EventImage', 'event_id', 'id');
    }
}
