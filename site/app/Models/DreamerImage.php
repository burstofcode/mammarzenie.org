<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DreamerImage extends Model
{
    protected $connection = 'mysql_app';

    protected $table = 'dreamer_image';

    protected $primaryKey = 'dreamer_id';

}
