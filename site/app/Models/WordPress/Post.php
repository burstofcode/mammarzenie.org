<?php

namespace App\Models\WordPress;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $connection = 'mysql';
    protected $table = 'posts';
    protected $primaryKey = 'ID';
    protected $post_type = null;
    public $timestamps = false;

    const CREATED_AT = 'post_date';
    const UPDATED_AT = 'post_modified';

    public function terms () {
        return $this->hasManyThrough(
            TermTaxonomy::class,
            TermRelationship::class,
            'object_id',
            'term_taxonomy_id',
            'ID',
            'term_taxonomy_id'
        )->with('term');
    }

    public function posts()
    {
        $this->where('post_type', 'post');
    }

    public function recurringEvents()
    {
        $this->where('post_type', 'imprezy-cykliczne');
    }

    public function events()
    {
        $this->where('post_type', 'nasze-akcje');
    }

    public function categories () {
        return $this->terms()->where('taxonomy', 'category');
    }

    public function eventBranch () {
        return $this->terms()->where('taxonomy', 'event_branch');
    }


    // Scopes

    public function scopeStatus ( $query, $status = 'publish') {
        return $query->where('post_status', $status);
    }

    public function scopePublished ($query) {
        return $query->status('publish');
    }

    public function scopeType ($query, $type = 'post') {
        return $query->where('post_type', $type);
    }

    public function scopeOrType ($query, $type = 'post') {
        return $query->orWhere('post_type', $type);
    }

    /**
     * @param $query
     * @param $taxonomy
     * @return mixed
     */
    public function scopeTaxonomy ( $query, $taxonomy) {
        $the_id = $query->value('ID');
//        dd($the_id);
        return $query->whereHas('term_relationships', function ($query) use ($taxonomy, $the_id){
            $the_term_tax_id = $query->value('object_id');
            dd($the_term_tax_id);
            $query->where('object_id', $the_id)->whereHas('term_taxonomies', function ($query) use ($taxonomy, $the_term_tax_id) {
               $query->where('term_taxonomy_id', $the_term_tax_id)->with('term');
            });
        });
    }

    public function scopeWithTerms ($query) {
        return $query->hasManyThrough(
            TermTaxonomy::class,
            TermRelationship::class,
            'object_id',
            'term_taxonomy_id',
            'ID',
            'term_taxonomy_id'

        )->with('term');
    }


    // Relationships

    public function term_relationships(){
        $this->belongsTo('\App\Models\WordPress\TermRelationship', 'object_id');
    }

}
