<?php

namespace App\Models\WordPress;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TermTaxonomy
 * @package App\Models\WordPress
 */
class TermTaxonomy extends Model
{
    protected $connection = 'mysql';
    protected $table = 'term_taxonomy';

    public function term() {
        return $this->belongsTo(Term::class, 'term_id', 'term_id');
    }
}
