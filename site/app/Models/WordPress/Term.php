<?php

namespace App\Models\WordPress;

use Illuminate\Database\Eloquent\Model;

class Term extends Model
{
    protected $connection = 'mysql';
    protected $table = 'terms';
    protected $primaryKey = 'term_id';

    public function meta() {
        return $this->hasMany(\WPEloquent\Model\Term\Meta::class, 'term_id')
            ->select(['term_id', 'meta_key', 'meta_value']);
    }
}
