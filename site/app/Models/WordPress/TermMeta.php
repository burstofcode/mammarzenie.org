<?php

namespace App\Models\WordPress;

use Illuminate\Database\Eloquent\Model;

class TermMeta extends Model
{
    protected $connection = 'mysql';
    protected $table = 'term_meta';
    protected $primaryKey = 'meta_id';
    protected $fillable = ['meta_key', 'meta_value'];
}
