<?php

namespace App\Models\WordPress;

use Illuminate\Database\Eloquent\Model;

class TermRelationship extends Model
{
    protected $connection = 'mysql';
    protected $table = 'term_relationships';
    protected $primaryKey = 'object_id';

    public function posts(){
        $this->hasOne(Post::class, 'ID');
    }

    public function term_taxonomies(){
        $this->belongsTo(TermTaxonomy::class, 'term_taxonomy_id');
    }
}
