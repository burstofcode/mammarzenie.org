��          �      �       0     1  M   K     �     �     �     �  	   �     �  
   �     �  /   �  1     e  I     �  X   �  	   #     -     4     F  	   I     S  
   f     q  /   u  1   �        
                                       	           &mdash; No Change &mdash; Allow switching of a post type while editing a post (in post publish section) Cancel Edit John James Jacoby OK Post Type Post Type Switcher Post Type: Type https://profiles.wordpress.org/johnjamesjacoby/ https://wordpress.org/plugins/post-type-switcher/ PO-Revision-Date: 2018-03-08 12:20:15+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: GlotPress/2.4.0-alpha
Language: pl
Project-Id-Version: Plugins - Post Type Switcher - Stable (latest release)
 &mdash; Brak zmian &mdash; Zezwalaj na przełączanie typu treści podczas edytowania posta (w sekcji publikowania) Zatrzymaj Edycja John James Jacoby OK Typ postu Zmień typ treści Typ wpisu: Typ https://profiles.wordpress.org/johnjamesjacoby/ https://wordpress.org/plugins/post-type-switcher/ 