<?php

/**
 * Edit this file in order to configure your theme templates.
 *
 * You can define just a template slug by only defining a key.
 * For a better user experience, you can define a display title as a
 * value and as a second argument, you can specify a list of post types
 * where your template is available.
 */
return [
    'container-template' => [__('Container', THEME_TD), ['page']],
    'narrow-template' => [__('Narrow container', THEME_TD), ['page']],
    'wide-template' => [__('Wide container', THEME_TD), ['page']],
    'full-template' => [__('Full width container', THEME_TD), ['page']],
];
