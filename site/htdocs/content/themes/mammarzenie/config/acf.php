<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Standard Fields
    |--------------------------------------------------------------------------
    |
    | The fields listed here will be automatically loaded on the
    | request to your application.
    |
    */

    'fields' => [
        Theme\Fields\BranchPage::class,
        Theme\Fields\MainSponsor::class,
        Theme\Fields\EventDate::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Gutenberg Blocks
    |--------------------------------------------------------------------------
    |
    | The Gutenberg blocks listed here will be automatically loaded on the
    | request to your application.
    |
    */

    'blocks' => [
        Theme\Blocks\Achievements\Achievements::class,
        Theme\Blocks\Ambasador\Ambasador::class,
        Theme\Blocks\Branches\Branches::class,
        Theme\Blocks\ContactDetails\ContactDetails::class,
        Theme\Blocks\ContactForm\ContactForm::class,
        Theme\Blocks\Counter\Counter::class,
        Theme\Blocks\Hero\Hero::class,
        Theme\Blocks\Donation\Donation::class,
        Theme\Blocks\IconBlock\IconBlock::class,
        Theme\Blocks\News\News::class,
        Theme\Blocks\Newsletter\Newsletter::class,
        Theme\Blocks\PendingWish\PendingWish::class,
        Theme\Blocks\Slogan\Slogan::class,
        Theme\Blocks\Sponsors\Sponsors::class,
        Theme\Blocks\Support\Support::class,
        Theme\Blocks\Team\Team::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Default Field Type Settings
    |--------------------------------------------------------------------------
    |
    | Here you can set default field group and field type configuration that
    | is then merged with your field groups when they are composed.
    |
    | This allows you to avoid the repetitive process of setting common field
    | configuration such as `ui` on every `trueFalse` field or
    | `instruction_placement` on every `fieldGroup`.
    |
    */

    'defaults' => [
        'trueFalse' => ['ui' => 1],
        'select' => ['ui' => 1],
    ],
];
