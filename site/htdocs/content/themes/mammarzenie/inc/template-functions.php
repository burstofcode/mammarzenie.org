<?php

use Themosis\Support\Facades\Action;
use Themosis\Support\Facades\Filter;

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 *
 * @return array
 */
Filter::add('body_class', function ($classes) {
    // Adds a class of hfeed to non-singular pages.
    if (! is_singular()) {
        $classes[] = 'hfeed';
    }

    // Adds a class of no-sidebar when there is no sidebar present.
    if (! is_active_sidebar('sidebar-1')) {
        $classes[] = 'no-sidebar';
    }

    return $classes;
});

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
Action::add('wp_head', function () {
    if (is_singular() && pings_open()) {
        echo '<link rel="pingback" href="'.esc_url(get_bloginfo('pingback_url')).'">';
    }
});

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 */
Action::add('after_setup_theme', function () {
    $GLOBALS['content_width'] = 640;
}, 0);

add_theme_support('align-wide');
add_theme_support('post-thumbnails');

// Removes the canonical redirection
Action::add('parse_query', function() {
    remove_filter( 'template_redirect', 'redirect_canonical' );
});




//add_filter( 'manage_bialystok_posts_columns', 'smashing_realestate_columns' );
//function smashing_realestate_columns( $columns ) {
//
//
//    $columns = array(
//        'cb' => $columns['cb'],
//        'image' => __( 'Image' ),
//        'title' => __( 'Title' ),
//        'post_status' => __( 'Status' ),
//        'date' => __( 'Date' ),
//    );
//
//
//    return $columns;
//}
//
//add_action( 'manage_bialystok_posts_custom_column', 'smashing_realestate_column', 10, 2);
//function smashing_realestate_column( $column, $post_id ) {
//    // Image column
//    if ( 'image' === $column ) {
//        echo get_the_post_thumbnail( $post_id, array(80, 80) );
//    }
//}

add_filter( 'manage_edit-imprezy-cykliczne_sortable_columns', function ( $columns ) {
    $columns['event_branch'] = \App\Services\EventStatusService::register();

    //To make a column 'un-sortable' remove it from the array
    //unset($columns['date']);

    return $columns;
} );

add_filter( 'manage_edit-imprezy-cykliczne_sortable_columns', 'set_custom_mycpt_sortable_columns' );

function set_custom_mycpt_sortable_columns( $columns ) {
    $columns['event_branch'] = \App\Services\EventStatusService::register();

    return $columns;
}
add_action( 'pre_get_posts', 'mycpt_custom_orderby' );

function mycpt_custom_orderby( $query ) {
    if ( ! is_admin() )
        return;

    $orderby = $query->get( 'orderby');

    if ( 'event_branch' == $orderby ) {
        $query->set( 'meta_key', 'event_branch' );
        $query->set( 'orderby', 'meta_value' );
    }
}


function excerpt($post_id, $limit) {
    $excerpt = explode(' ', get_the_excerpt($post_id), $limit);
    if (count($excerpt)>=$limit) {
        array_pop($excerpt);
        $excerpt = implode(" ",$excerpt).'[...]';
    } else {
        $excerpt = implode(" ",$excerpt);
    }
    $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
    return $excerpt;
}

//add_action( 'init', function() {
//    register_post_status( 'old', array(
//        'label'                     => _x( 'Past', 'post' ), // I used only minimum of parameters
//        'label_count'               => _n_noop( 'Featured <span class="count">(%s)</span>', 'Featured <span class="count">(%s)</span>'),
//        'public'                    => true
//    ));
//} );


//Action::add('init', function() {
//    $post_id = !empty($_POST['post']) ? $_POST['post'] : false;
//
//    $now = date('Ymd');
//    $start_date = get_field('start_date', $post_id);
//    $end_date = get_field('end_date', $post_id);
//
//    if ($start_date <= $now AND $now <= $end_date) {
//        $status = 'live';
//    } elseif ($start_date < $now) {
//        $status = 'past';
//    } elseif ($end_date > $now) {
//        $status = 'future';
//    } else {
//        $status = 'past';
//    }
//    wp_set_object_terms( $post_id, 'past', 'event_status' );
//});