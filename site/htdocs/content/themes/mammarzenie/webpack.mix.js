const mix = require('laravel-mix');
require('laravel-mix-wp-blocks');
require('laravel-mix-purgecss');
require('laravel-mix-copy-watched');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Sage application. By default, we are compiling the Sass file
 | for your application, as well as bundling up your JS files.
 |
 */

mix.setPublicPath('dist')
    .browserSync({
        port: 3000,
        proxy: {
            target: 'https://local.mammarzenie.org',
        },
        delay: 0,
        https: {
            key: '/etc/ssl/localhost/localhost.key',
            cert: '/etc/ssl/localhost/localhost.crt',
        },
    });

mix.sass('assets/styles/theme.scss', 'styles')
    .sass('assets/styles/admin.scss', 'styles')
    .sass('assets/styles/editor.scss', 'styles')
    .sass('assets/styles/woocommerce.scss', 'styles');

mix.js('assets/scripts/theme.js', 'scripts')
    .js('assets/scripts/editor.js', 'scripts');

mix.copyWatched('assets/images', 'dist/images')
    .copyWatched('assets/fonts', 'dist/fonts');

mix.autoload({
    jquery: ['$', 'window.jQuery'],
});

mix.options({
    processCssUrls: false,
});

mix.sourceMaps(false, 'source-map')
    .version();
