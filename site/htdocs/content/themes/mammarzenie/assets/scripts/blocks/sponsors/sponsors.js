import Swiper from "swiper";

/**
 * Block - Sponsors
 */
export default () => {

    new Swiper('#js-sponsors', {
        slidesPerView: 'auto',
        centeredSlides: true,
        loop: true,
        speed: 1500,
        autoplay: {
            delay: 1000,
            disableOnInteraction: false,
        },
        spaceBetween:40,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
    });

};
