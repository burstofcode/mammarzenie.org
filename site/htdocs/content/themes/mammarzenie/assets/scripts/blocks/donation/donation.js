let btnPrivate = $('#js-support-button-private');
let btnCompany = $('#js-support-button-company');

let contentPrivate = $('#js-support-content-private');
let contentCompany = $('#js-support-content-company');

let btnActive = 'button--filled';
let btnInactive = 'button--outlined';

let contentShow = 'support__type--show';
let contentHide = 'support__type--hide';

export default () => {
    btnPrivate.click(function () {
        $(this).removeClass(btnInactive).addClass(btnActive);
        $(btnCompany).removeClass(btnActive).addClass(btnInactive);
        $(contentPrivate).removeClass(contentHide).addClass(contentShow);
        $(contentCompany).removeClass(contentShow).addClass(contentHide);
    });

    btnCompany.click(function () {
        $(this).removeClass(btnInactive).addClass(btnActive);
        $(btnPrivate).removeClass(btnActive).addClass(btnInactive);
        $(contentCompany).removeClass(contentHide).addClass(contentShow);
        $(contentPrivate).removeClass(contentShow).addClass(contentHide);
    });

    $(".donation__box--private").on("click", function() {
        $(".donation__box--private").removeClass("active");
        $(this).addClass("active");
    });

    $(".donation__box--company").on("click", function() {
        $(".donation__box--company").removeClass("active");
        $(this).addClass("active");
    });
};
