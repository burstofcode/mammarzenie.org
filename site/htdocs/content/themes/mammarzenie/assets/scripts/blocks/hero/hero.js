export default () => {
  // Adds green background to hero slogan
  $(".js-hero__captions").find(".hero__title").each(function () {

    var $el = $(this),
      text = $el.text(),
      len = text.length,
      newCont = '';

    for (var i = 0; i < len; i++) {
      if (text != '<br>') {
        newCont += '<span>' + text.charAt(i) + '</span>';
      }
    }

    $el.html(newCont);
  });
}
