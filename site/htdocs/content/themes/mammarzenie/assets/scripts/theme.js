/**
 * External Dependencies
 */
import 'jquery';
// import './vendor/swipebox';

/**
 * Parts
 */
import sponsors from "./blocks/sponsors/sponsors";
import filter_date from "./parts/filter_date";
import filter_wish_category from "./parts/filter_wish_category";
import filter_event_type from "./parts/filter_event_type";
import filter_branch from "./parts/filter_branch";
import filter_event_timeframe from "./parts/filter_event_timeframe";
import filter_view_type from "./parts/filter_view_type";
import filter_items_per_page from "./parts/filter_items_per_page";
import {hamburgerAnimation} from "./parts/hamburgerAnimation";
import {navbarVisibility} from "./parts/navbarVisibility";
import {support} from "./blocks/support/support";
import signup_form from "./parts/dreamers/signup_form";
import contact_details from "./blocks/contact_details/contact_details";
import donation from "./blocks/donation/donation";
import single_page from "./parts/dreamers/single_page";
import swipebox from "./vendor/swipebox";
import posts_slider from "./parts/posts_slider";


$(document).ready(() => {
  swipebox();
  hamburgerAnimation();
  navbarVisibility();
  posts_slider();
  support();
  sponsors();
  filter_date();
  filter_wish_category();
  filter_event_type();
  filter_branch();
  filter_event_timeframe();
  filter_view_type();
  filter_items_per_page();
  signup_form();
  contact_details();
  donation();
  single_page();

  ( function( $ ) {
    $( '.swipebox' ).swipebox();
  } )($);

});
