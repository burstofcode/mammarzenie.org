const id = '#view-type--buttons';

export default () => {
    if($(id).length) {
        let grid_view_btn = $('#view-type--grid'),
            list_view_btn = $('#view-type--list'),
            single = $('.dreamers-loop__single'),
            grid_view_class = 'dreamers-loop--grid-view',
            list_view_class = 'dreamers-loop--list-view',
            list = $('#dreamers-loop'),
            dream_type = $('.dreamers-loop__type');

        /**
         * Check which view type is stored in browsers localStorage and use it.
         */
        if (getView() == 'list') {
            listView();
        } else if (getView() == 'grid') {
            gridView();
        } else {
            dream_type.hide();
        }

        /**
         * Use appropriate function to load list view and store choice in browsers localStorage.
         */
        list_view_btn.on('click', function () {
            listView();
            setView('list');
        });

        /**
         * Use appropriate function to load grid view and store choice in browsers localStorage.
         */
        grid_view_btn.on('click', function () {
            gridView();
            setView('grid');
        });

        /**
         * Store chosen view in browsers localStorage.
         */
        function setView(view) {
            localStorage.setItem('view', view);
        }

        /**
         * Get stored view from browsers localStorage.
         */
        function getView(view) {
            return localStorage.getItem('view');
        }

        /**
         * Grid view function - toggle "active" class on buttons and view class type on loop items.
         */
        function gridView() {
            grid_view_btn.addClass('active');
            list_view_btn.removeClass('active');
            list.removeClass(list_view_class).addClass(grid_view_class);
            single.addClass('grid__column--sm-6').addClass('grid__column--md-4').addClass('grid__column--lg-3');
            dream_type.hide();
        }

        /**
         * List view function - toggle "active" class on buttons and view class type on loop items.
         */
        function listView() {
            list_view_btn.addClass('active');
            grid_view_btn.removeClass('active');
            list.removeClass(grid_view_class).addClass(list_view_class);
            single.removeClass('grid__column--sm-6').removeClass('grid__column--md-4').removeClass('grid__column--lg-3');
            dream_type.show();
        }
    }
};
