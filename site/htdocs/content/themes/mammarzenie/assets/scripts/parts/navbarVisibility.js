
export function navbarVisibility() {

    if (window.screen.width >= 992) {

        var prevScrollpos = window.pageYOffset;
        window.onscroll = function() {
            var currentScrollPos = window.pageYOffset;
            if (prevScrollpos > currentScrollPos) {
                document.getElementById("js-header").classList.remove( 'js-is-hidden');
            } else {
                document.getElementById("js-header").classList.add( 'js-is-hidden');
            }
            prevScrollpos = currentScrollPos;
        }

    }

    $( window ).on( "orientationchange", function( event ) {
        $('body').removeClass('modal--open').removeClass('nav--open');
    });
}
