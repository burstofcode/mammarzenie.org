import SlimSelect from "slim-select";

const id = '#filter-dreamers-category';

export default () => {
    if($(id).length) {
        new SlimSelect({
            placeholder: 'kategoria marzeń',
            select: id,
            showSearch: false,
        });

        $(id).on('change', function() {
            location.href = this.value;
        });
    }
};
