import Swiper from "swiper";

/**
 * Page - Dreamer Singular
 */
export default () => {

    let sliderName = '#js-awaiting',
        sliderWrapper = '.awaiting__wrap',
        sliderSingle = '.awaiting__single',

        swiperContainer = 'swiper-container',
        swiperWrapper = 'swiper-wrapper',
        swiperSlide = 'swiper-slide',

        containerClass = 'container',
        wrapperClass = 'grid',
        slideClass = 'grid__column--3';

    if (window.screen.width < 1070) {
        $( sliderName ).addClass( swiperContainer ).removeClass( containerClass );
        $( sliderWrapper ).addClass( swiperWrapper ).removeClass( wrapperClass );
        $( sliderSingle ).addClass( swiperSlide ).removeClass( slideClass );
    } else {
        $( sliderName ).removeClass( swiperContainer ).addClass( containerClass );
        $( sliderWrapper ).removeClass( swiperWrapper ).addClass( wrapperClass );
        $( sliderSingle ).removeClass( swiperSlide ).addClass( slideClass );
    }

    new Swiper( sliderName, {
        centeredSlides: false,
        loop: false,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        breakpoints: {
            1070: {
                slidesPerView: 3.85,
                spaceBetween: 20,
            },
            950: {
                slidesPerView: 3.6,
                spaceBetween: 20,
            },
            825: {
                slidesPerView: 3.1,
                spaceBetween: 20,
            },
            770: {
                slidesPerView: 2.9,
                spaceBetween: 20,
            },
            670: {
                slidesPerView: 2.7,
                spaceBetween: 10,
            },
            530: {
                slidesPerView: 2.5,
                spaceBetween: 10,
            },
            440: {
                slidesPerView: 2.1,
                spaceBetween: 10,
            },
            380: {
                slidesPerView: 1.8,
                spaceBetween: 10,
            },
            345: {
                slidesPerView: 1.55,
                spaceBetween: 10,
            },
            310: {
                slidesPerView: 1.3,
                spaceBetween: 10,
            },
        },
    });

};
