import SlimSelect from "slim-select";

const id = '#filter-date';

export default () => {
    if($(id).length) {
        new SlimSelect({
            placeholder: 'data dodania',
            select: id,
            showSearch: false,
        });

        $(id).on('change', function() {
            location.href = this.value;
        });
    }
};
