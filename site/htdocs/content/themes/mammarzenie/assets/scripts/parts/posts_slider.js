import Swiper from "swiper";

/**
 * Block - News
 */
export default () => {

    let sliderName = '#js-news',
        sliderWrapper = '.news__wrap',
        sliderSingle = '.news__single',

        swiperContainer = 'swiper-container',
        swiperWrapper = 'swiper-wrapper',
        swiperSlide = 'swiper-slide',

        containerClass = 'container',
        wrapperClass = 'grid',
        slideClass = 'grid__column--3';

    if (window.screen.width < 1070) {
        jQuery( sliderName ).addClass( swiperContainer ).removeClass( containerClass );
        jQuery( sliderWrapper ).addClass( swiperWrapper ).removeClass( wrapperClass );
        jQuery( sliderSingle ).addClass( swiperSlide ).removeClass( slideClass );
    } else {
        jQuery( sliderName ).removeClass( swiperContainer ).addClass( containerClass );
        jQuery( sliderWrapper ).removeClass( swiperWrapper ).addClass( wrapperClass );
        jQuery( sliderSingle ).removeClass( swiperSlide ).addClass( slideClass );
    }

    new Swiper( sliderName, {
        centeredSlides: false,
        loop: false,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        breakpoints: {
            991: {
                slidesPerView: 3.3,
                spaceBetween: 20,
            },
            770: {
                slidesPerView: 2.5,
                spaceBetween: 20,
            },
            670: {
                slidesPerView: 2.3,
                spaceBetween: 10,
            },
            530: {
                slidesPerView: 1.8,
                spaceBetween: 10,
            },
            430: {
                slidesPerView: 1.5,
                spaceBetween: 10,
            },
            320: {
                slidesPerView: 1.2,
                spaceBetween: 10,
            }
        }
    });

};
