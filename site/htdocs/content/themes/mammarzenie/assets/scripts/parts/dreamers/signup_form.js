import SlimSelect from "slim-select";
// require('jquery-validation/dist/jquery.validate');
// require('jquery-steps/build/jquery.steps');
/**
 * Page - Zgłoś marzyciela
 */
export default () => {

    const id = '#branch-filter';

    if($(id).length) {
        new SlimSelect({
            placeholder: 'województwo',
            select: id,
            showSearch: false,
        });
    }

    // const form = $("#contact");
    // form.validate({
    //   errorPlacement: function errorPlacement(error, element) { element.before(error); },
    //   rules: {
    //     confirm: {
    //       equalTo: "#password",
    //     },
    //   },
    // });
    // form.children("div").steps({
    //   headerTag: "h3",
    //   bodyTag: "section",
    //   transitionEffect: "slideLeft",
    //   stepsOrientation: "vertical",
    //   // autoFocus: true,
    //   onStepChanging: function (event, currentIndex, newIndex)
    //   {
    //     form.validate().settings.ignore = ":disabled,:hidden";
    //     return form.valid();
    //   },
    //   onFinishing: function (event, currentIndex)
    //   {
    //     form.validate().settings.ignore = ":disabled";
    //     return form.valid();
    //   },
    //   onFinished: function (event, currentIndex)
    //   {
    //     alert("Submitted!");
    //   },
    // });

};
