export function hamburgerAnimation()
{
    $('#js-header-nav__hamburger').click(function () {
        $(this).toggleClass('open');
        $('#js-header-nav').toggleClass('menu-open');
        $('body').toggleClass('nav--open');
    });
}
