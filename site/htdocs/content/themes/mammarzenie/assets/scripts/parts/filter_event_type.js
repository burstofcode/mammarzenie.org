import SlimSelect from "slim-select";

const id = '#filter-event-type';

export default () => {
    if($(id).length) {
        new SlimSelect({
            select: id,
            showSearch: false,
        });
    }

    $(id).on('change', function() {
        location.href = this.value;
    });
};
