import SlimSelect from "slim-select";

const id = '#filter-branch';

export default () => {
    if($(id).length) {
        new SlimSelect({
            placeholder: 'oddział ',
            select: id,
            showSearch: false,
        });
        
        $(id).on('change', function() {
           location.href = this.value;
        });
    }
};