<?php

namespace Theme\Fields;

use Rafflex\AcfComposer\Field;
use StoutLogic\AcfBuilder\FieldsBuilder;

class EventDate extends Field
{
    /**
     * Default field type configuration.
     *
     * @return array
     */
    protected $defaults = [];

    /**
     * Fields to be registered with the application.
     *
     * @return array
     * @throws \StoutLogic\AcfBuilder\FieldNameCollisionException
     */
    public function fields()
    {
        $event_date = new FieldsBuilder('event_date', ['position' => 'side']);

        $event_date
            ->addDatePicker('start_date', [
                'label' => 'Data rozpoczęcia',
                'wrapper' => [
                    'width' => 50
                ],
                'return_format' => 'Ymd',
            ])
            ->addDatePicker('end_date', [
                'label' => 'Data zakończenia',
                'wrapper' => [
                    'width' => 50
                ]
                ,'return_format' => 'Ymd',
            ]);


        $event_date
            ->setLocation('post_type', '==', 'imprezy-cykliczne' AND 'post_type', '==', 'nasze-akcje')
        ;

        return $event_date->build();
    }
}
