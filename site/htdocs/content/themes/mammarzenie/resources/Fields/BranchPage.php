<?php

namespace Theme\Fields;

use Rafflex\AcfComposer\Field;
use StoutLogic\AcfBuilder\FieldsBuilder;

class BranchPage extends Field
{
    /**
     * Default field type configuration.
     *
     * @return array
     */
    protected $defaults = [];

    /**
     * Fields to be registered with the application.
     *
     * @return array
     * @throws \StoutLogic\AcfBuilder\FieldNameCollisionException
     */
    public function fields()
    {
        $branch_page = new FieldsBuilder('branch-page', [
            'style' => 'seamless'
        ]);

        $branch_page
            ->addWysiwyg('content', [
                'label' => 'Content',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => [],
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'default_value' => '',
                'tabs' => 'visual',
                'toolbar' => 'basic',
                'media_upload' => 0,
                'delay' => 0,
            ])
            ->addRepeater('volunteers', [
                'label' => 'Volunteers',
                'required' => 0,
                'min' => 1,
                'layout' => 'table',
                'button_label' => __('Dodaj wolontariusza'),
            ])
            ->addText('name', [
                'label' => __('Name'),
                'required' => 1
            ])
            ->addText('position', [
                'label' => __('Position'),
                'required' => 0
            ])
            ->addText('phone', [
                'label' => __('Phone'),
                'required' => 0
            ])
            ->addText('email', [
                'label' => __('E-mail'),
                'required' => 0
            ])
            ->endRepeater()
            ->addText('more_volunteers_button', [
                'label' => __('More volunteers - button name'),
                'required' => 1,
                'wrapper' => [
                    'width' => 50
                ]
            ])
            ->addText('more_news_button', [
                'label' => __('More news - button name'),
                'required' => 1,
                'wrapper' => [
                    'width' => 50
                ]
            ])
            ->addText('volunteers_title', [
                'label' => __('Our volunteers - title'),
                'required' => 1
            ]);

         $branch_page
             ->setLocation('post_type', '==', 'strony-oddzialu');

        return $branch_page->build();
    }
}
