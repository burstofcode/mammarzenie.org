<?php

namespace Theme\Fields;

use Rafflex\AcfComposer\Field;
use StoutLogic\AcfBuilder\FieldsBuilder;

class MainSponsor extends Field
{
    /**
     * Default field type configuration.
     *
     * @return array
     */
    protected $defaults = [];

    /**
     * Fields to be registered with the application.
     *
     * @return array
     * @throws \StoutLogic\AcfBuilder\FieldNameCollisionException
     */
    public function fields()
    {
        $main_sponsor = new FieldsBuilder(__('sponsor_link'));

        $main_sponsor
            ->addText('sponsor_link', [
                'label' => __('Link'),
                'required' => 0,
            ])
            ->setInstructions(__('Optional'));

         $main_sponsor
             ->setLocation('post_type', '==', 'sponsor');

        return $main_sponsor->build();
    }
}
