<?php

namespace Theme\Blocks\Counter;

use App\Models\Branch;
use App\Models\Volunteer;
use App\Models\Wish;
use Rafflex\AcfComposer\Block;
use StoutLogic\AcfBuilder\FieldsBuilder;
use Theme\Blocks\Counter\assets\Icon;
use function Theme\Blocks\reusable\align;
use function Theme\Blocks\reusable\className;

class Counter extends Block
{
    /**
     * Default field type configuration.
     *
     * @return array
     */
    protected $defaults = [];

    /**
     * Data to be passed to the block before registering.
     * @see https://www.advancedcustomfields.com/resources/acf_register_block_type/
     *
     * @return array
     */
    public function register()
    {
        return [
            'name' => 'counter',
            'title' => __('Counter'),
            'description' => __('Organisation in numbers.'),
            'category' => 'sections',
            'icon' => Icon::svg(),
            'post_types' => ['page'],
            'supports' => [
                'align' => ['full', 'wide']
            ]
        ];
    }

    /**
     * Fields to be attached to the block.
     *
     * @return array
     * @throws \StoutLogic\AcfBuilder\FieldNameCollisionException
     */
    public function fields()
    {
        $counter = new FieldsBuilder('counter');

        $counter
            ->setLocation('block', '==', 'acf/counter');

        $counter
            ->addText('title', [
                'label' => __('Title'),
            ])
            ->addText('button_name', [
                'label' => __('Button name'),
                'wrapper' => [
                    'width' => 50
                ]
            ])
            ->addPageLink('button_url', [
                'label' => __('Button URL'),
                'wrapper' => [
                    'width' => 50
                ],
                'post_type' => ['page', 'post'],
                'allow_null' => 1,
                'allow_archives' => 1,
                'multiple' => 0,
                'ui' => 1
            ]);

        return $counter->build();
    }

    /**
     * Data to be passed to the rendered block.
     *
     * @return array
     */
    public function with()
    {
        return [
            'align' => align($this->block),
            'class_name' => className($this->block),

            'title' => $this->title(),
            'button_name' => $this->buttonName(),
            'button_url' => $this->buttonUrl(),
            'background_url' => $this->backgroundUrl(),

            'volunteers' => $this->voluteers(),
            'branches' => $this-> branches(),
            'fulfilled_dreams' => $this->fulfilledDreams(),
        ];
    }

    /**
     * @return mixed
     */
    public function title()
    {
        return get_field('title');
    }

    /**
     * @return mixed
     */
    public function buttonName()
    {
        return get_field('button_name');
    }

    /**
     * @return mixed
     */
    public function buttonUrl()
    {
        return get_field('button_url');
    }

    /**
     * Returns image placeholder.
     * @return mixed
     */
    public function backgroundUrl()
    {
        return app('wp.theme')->getUrl('dist/images/numbers.jpg');
    }

    /**
     * @return int|void
     */
    public function voluteers()
    {
        return count(Volunteer::all());
    }

    /**
     * @return int|void
     */
    public function branches()
    {
        return count(Branch::list());
    }

    public function fulfilledDreams()
    {
        return count(Wish::where('wish_state_id', 4)->get());
    }
}
