<section class="contact-details container {{ $align }} {{ $class_name }}">

    <div class="contact-details__box">
        @if ($name)
        <h5 class="item item__name">{{ $name }}</h5>
        @endif
        @if ($address)
        <address class="item item__address">{!! $address !!}</address>
        @endif
        @if ($phone)
        <p class="item item__phone">{{ $phone }}</p>
        @endif
        @if ($email)
        <p class="item item__email">{{ $email }}</p>
        @endif
        @if ($opening_hours)
        <p class="item item__opening-hours">{!! $opening_hours !!}</p>
        @endif
        @if ($legal)
        <p class="item item__legal">{!! $legal !!}</p>
        @endif
        @if ($all_branches['title'])
        <a href="{{ $all_branches['link'] }}" class="item item__all-branches">
            {{ $all_branches['title'] }}
        </a>
        @endif
    </div>
    <div class="map" style="background-image: url( {{ $map_image }} )"></div>
</section>