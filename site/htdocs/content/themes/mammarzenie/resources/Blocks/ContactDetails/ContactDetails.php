<?php

namespace Theme\Blocks\ContactDetails;

use App\Services\PathService;
use Theme\Blocks\BaseBlock;
use StoutLogic\AcfBuilder\FieldsBuilder;
use Theme\Blocks\ContactDetails\assets\Icon;
use function Theme\Blocks\reusable\align;
use function Theme\Blocks\reusable\className;

/**
 * Class ContactDetails
 * @package Theme\Blocks\ContactDetails
 */
class ContactDetails extends BaseBlock
{
    /**
     * Default field type configuration.
     *
     * @return array
     */
    protected $defaults = [];

    /**
     * Data to be passed to the block before registering.
     * @see https://www.advancedcustomfields.com/resources/acf_register_block_type/
     *
     * @return array
     */
    public function register()
    {
        return [
            'name' => 'contact-details',
            'title' => __('Contact Details'),
            'description' => __('List of ways to contact with'),
            'category' => 'advanced',
            'icon' => Icon::svg(),
            'post_types' => ['page'],
            'supports' => [
                'align' => ['full', 'wide']
            ]
        ];
    }

    /**
     * Fields to be attached to the block.
     *
     * @return array
     * @throws \StoutLogic\AcfBuilder\FieldNameCollisionException
     */
    public function fields()
    {
        $contact_details = new FieldsBuilder( 'contact_details' );

        $contact_details
            ->setLocation( 'block', '==', 'acf/contact-details' );

        $contact_details
            ->addText( 'name', [
                'label' => __( 'Nazwa' )
            ] )
            ->addTextarea( 'address', [
                'label' => __( 'Address' ),
                'rows' => 2,
                'new_lines' => 'br'
            ] )
            ->addText( 'phone', [
                'label' => __( 'Phone' ),
                'wrapper' => [
                    'width' => 50
                ]
            ] )
            ->addText( 'email', [
                'label' => __( 'Email' ),
                'wrapper' => [
                    'width' => 50
                ]
            ] )
            ->addTextarea( 'opening_hours', [
                'label' => __( 'Opening hours' ),
                'rows' => 2,
                'new_lines' => 'br'
            ] )
            ->addTextarea( 'legal', [
                'label' => __( 'Legal details' ),
                'rows' => 3,
                'new_lines' => 'br'
            ] )
            ->addText( 'all_branches_title', [
                'label' => __( 'All branches - title' )
            ] )
            ->addPageLink( 'all_branches_link', [
                'label' => __( 'All branches - link' ),
                'type' => 'page_link',
                'post_type' => ['page'],
                'allow_null' => 1,
                'allow_archives' => 1,
                'multiple' => 0,
            ] )
                ->conditional( 'all_branches_title', '!=', '');

        return $contact_details->build();
    }

    /**
     * Overwrite the view function.
     *
     * @return array|string
     * @throws \Throwable
     */
    public function view()
    {
        if (view($view = "ContactDetails/views/{$this->slug}")) {
            return view(
                $view,
                array_merge($this->with(), ['block' => $this->block])
            )->render();
        }

        return view(
            __DIR__ . '/../resources/views/view-404.blade.php',
            ['view' => $view]
        )->render();
    }

    /**
     * Data to be passed to the rendered block.
     *
     * @return array
     */
    public function with()
    {
        return [
            'align' => align($this->block),
            'class_name' => className($this->block),

            'name' => $this->name(),
            'address' => $this->address(),
            'phone' => $this->phone(),
            'email' => $this->email(),
            'opening_hours' => $this->openingHours(),
            'legal' => $this->legal(),
            'all_branches' => $this->allBranches(),
            'all_branches_title' => $this->allBranchesTitle(),
            'all_branches_link' => $this->allBranchesLink(),
            'map_image' => $this->mapImage(),
        ];
    }

    /**
     * Returns the name field.
     *
     * @return string
     */
    public function name()
    {
        return get_field( 'name' );
    }

    /**
     * Returns the address field.
     *
     * @return string
     */
    public function address()
    {
        return get_field( 'address' );
    }

    /**
     * Returns the phone field.
     *
     * @return string
     */
    public function phone()
    {
        return get_field( 'phone' );
    }

    /**
     * Returns the email field.
     *
     * @return string
     */
    public function email()
    {
        return get_field( 'email' );
    }

    /**
     * Returns the opening_hours field.
     *
     * @return string
     */
    public function openingHours()
    {
        return get_field( 'opening_hours' );
    }

    /**
     * Returns the legal field.
     *
     * @return string
     */
    public function legal()
    {
        return get_field( 'legal' );
    }

    /**
     * Returns the all_branches_title and all_branches_link fields.
     *
     * @return array
     */
    public function allBranches()
    {
        return [
            'title' => get_field( 'all_branches_title' ),
            'link' => get_field( 'all_branches_link' )
        ];
    }

    /**
     * Returns the all_branches_title field.
     *
     * @return string
     */
    public function allBranchesTitle()
    {
        return get_field( 'all_branches_title' );
    }

    /**
     * Returns the all_branches_title field.
     *
     * @return string
     */
    public function allBranchesLink()
    {
        return get_field( 'all_branches_link' );
    }

    /**
     * Returns the path to the image with map.
     *
     * @return string
     */
    public function mapImage()
    {
        return PathService::image('map.svg');
    }
}
