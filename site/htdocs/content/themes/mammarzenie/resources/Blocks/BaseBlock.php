<?php

namespace Theme\Blocks;

use App\Services\PlaceholderService;
use Rafflex\AcfComposer\Block;

/**
 * Class BaseBlock
 * @package Blocks
 */
class BaseBlock extends Block
{

    /**
     * @return mixed
     */
    protected function avatarPlaceholder()
    {
        return PlaceholderService::placeholderAvatar();
    }

}