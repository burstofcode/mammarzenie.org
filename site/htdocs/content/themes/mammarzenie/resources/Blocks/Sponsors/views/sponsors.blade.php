<section class=" sponsors container {{ $align }} {{ $class_name }}">
    <header class="container">
        <div class="grid flex--justify-center">
            <div class="grid__column--12 grid__column--lg-8 block__header">
                    <h2 class="block__title">
                        @if($title)
                            {{ $title }}
                        @else
                            Sponsors
                        @endif
                    </h2>
            </div>
        </div>
    </header>

    <div id="js-sponsors" class="swiper-container">
        <div class="swiper-wrapper">

            @query([
                    'post_type' => 'sponsor',
                    'post_status' => 'publish',
                    'posts_per_page' => -1
                ])

                @php $link = get_field('sponsor_link', get_the_ID()) @endphp

                <div class="swiper-slide display--flex justify-content--center">
                    @if($link)<a href="{{ $link }}" class="display--flex justify-content--center" target="_blank">@endif
                        <div class="sponsors__image" style="background-image: url({{ get_the_post_thumbnail_url(get_the_ID()) }})" title="{{ the_title() }}"></div>
                    @if($link)</a>@endif
                </div>
            @endquery

        </div>
    </div>

</section>
