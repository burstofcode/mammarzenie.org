<?php

namespace Theme\Blocks\Sponsors;

use Rafflex\AcfComposer\Block;
use StoutLogic\AcfBuilder\FieldsBuilder;
use Theme\Blocks\Sponsors\assets\Icon;
use function Theme\Blocks\reusable\align;
use function Theme\Blocks\reusable\className;

class Sponsors extends Block
{
    /**
     * Default field type configuration.
     *
     * @return array
     */
    protected $defaults = [];

    /**
     * Data to be passed to the block before registering.
     * @see https://www.advancedcustomfields.com/resources/acf_register_block_type/
     *
     * @return array
     */
    public function register()
    {
        return [
            'name' => 'sponsors',
            'title' => __('Sponsors'),
            'description' => __('Slider with logos of sponsors from Main Sponsors'),
            'category' => 'sections',
            'icon' => Icon::svg(),
            'post_types' => ['page'],
            'supports' => [
                'align' => ['full', 'wide']
            ]
        ];
    }

    /**
     * Fields to be attached to the block.
     *
     * @return array
     * @throws \StoutLogic\AcfBuilder\FieldNameCollisionException
     */
    public function fields()
    {
        $sponsors = new FieldsBuilder('sponsors');

        $sponsors
            ->setLocation('block', '==', 'acf/sponsors');

        $sponsors
            ->addText('title', [
                'label' => __('Title'),
                'wrapper' => [
                    'width' => 100
                ]
            ]);

        return $sponsors->build();
    }

    /**
     * Data to be passed to the rendered block.
     *
     * @return array
     */
    public function with()
    {
        return [
            'align' => align($this->block),
            'class_name' => className($this->block),

            'title' => $this->title(),
        ];
    }

    /**
     * Returns the slides field.
     *
     * @return array
     */
    public function title()
    {
        return get_field('title');
    }
}
