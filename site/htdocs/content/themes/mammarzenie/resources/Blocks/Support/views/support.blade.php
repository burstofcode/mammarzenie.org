<section class="support container--wide {{ $align }} {{ $class_name }}">
    {{--Section header --}}
    <header class="container">
        <div class="grid flex--justify-center">
            <div class="grid__column--12 grid__column--lg-8 block__header">
                    <h2 class="block__title text-primary">
                        @if($title)
                            {{ $title }}
                        @else
                            Tytuł. Zobacz jak możesz pomóc
                        @endif
                    </h2>
            </div>
        </div>
    </header>

    <div class="container">
        <div class="support__tabs">
            <ul class="support__list list--unstyled display--flex flex--justify-center">
                <li class="support__item">
                    <button id="js-support-button-private" class="button__type--private button button--filled button--primary">
                        @if($private_tab)
                            {{ $private_tab }}
                        @else
                            Tab. Prywatne
                        @endif
                    </button>
                </li>
                <li class="support__item">
                    <button id="js-support-button-company" class="button__type--company button button--outlined button--primary">
                        @if($company_tab)
                            {{ $company_tab }}
                        @else
                            Tab. Firmowe
                        @endif
                    </button>
                </li>

            </ul>
        </div>
    </div>
    <div class="container support__panes">

        <div id="js-support-content-private" class="support__content support__type support__type--private support__type--show">
            <div class="grid">

                @if($private_support)
                    @foreach($private_support as $item)
                        <div class="support__single grid__column text--center">
                            <a href="{{ $item['link'] }}">
                                <div class="support__icon">
                                    @if($item["icon_type"] == 1 && $item["font_icon"])
                                        <i class="{{ $item["font_icon"]["value"] }}"></i>
                                    @elseif($item["icon_type"] == 0 && $item["image_icon"])
                                        <img src="{{ $item["image_icon"]["url"] }}" alt="{{ $item["image_icon"]["alt"] }}">
                                    @endif
                                </div>
                                <h5 class="support__title">{!! $item['title'] !!}</h5>
                            </a>
                        </div>
                    @endforeach
                @endif

            </div>
            <div class="grid display--flex flex--justify-center">
                <div class="grid__column--12 grid__column--md-10 grid__column--lg-8 text--center">
                    <p class="support__info">{!! $private_info !!}</p>
                </div>
            </div>
            <div class="grid">
                <div class="grid__column--12 text--center">
                    <a href="
                        @if($private_btn_url)
                            {{ $private_btn_url }}
                        @else
                            {{ home_url('/') }}
                        @endif
                    " class="support__more button button--filled button--primary">
                        @if($private_btn_name)
                            {{ $private_btn_name }}
                        @else
                            Więcej...
                        @endif
                    </a>
                </div>
            </div>
        </div>

        <div id="js-support-content-company" class="support__content support__type support__type--company support__type--hide">
            <div class="grid">

                @if($company_support)
                    @foreach($company_support as $item)
                        <div class="support__single grid__column text--center">
                            <a href="{{ $item['link'] }}">
                                <div class="support__icon">
                                     @if($item["icon_type"] == 1 && $item["font_icon"])
                                        <i class="{{ $item["font_icon"]["value"] }}"></i>
                                    @elseif($item["icon_type"] == 0 && $item["image_icon"])
                                        <img src="{{ $item["image_icon"]["url"] }}" alt="{{ $item["image_icon"]["alt"] }}">
                                    @endif
                                </div>
                                <h5 class="support__title">{!! $item['title'] !!}</h5>
                            </a>
                        </div>
                    @endforeach
                @endif

            </div>
            <div class="grid display--flex flex--justify-center">
                <div class="grid__column--12 grid__column--md-10 grid__column--lg-8 text--center">
                    <p class="support__info">{!! $company_info !!}</p>
                </div>
            </div>
            <div class="grid">
                <div class="grid__column--12 text--center">
                    <a href="
                        @if($company_btn_url)
                            {{ $company_btn_url }}
                        @else
                            {{ home_url('/') }}
                        @endif
                            " class="support__more button button--filled button--primary">
                        @if($company_btn_name)
                            {{ $company_btn_name }}
                        @else
                            Więcej...
                        @endif
                    </a>
                </div>
            </div>
        </div>

    </div>

</section>


{{--<script--}}
{{--        src="https://code.jquery.com/jquery-3.4.1.min.js"--}}
{{--        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="--}}
{{--        crossorigin="anonymous"></script>--}}
{{--<script>--}}
{{--    let btnPrivate = $('#js-support-button-private');--}}
{{--    let btnCompany = $('#js-support-button-company');--}}

{{--    let contentPrivate = $('#js-support-content-private');--}}
{{--    let contentCompany = $('#js-support-content-company');--}}

{{--    let btnActive = 'button--filled';--}}
{{--    let btnInactive = 'button--outlined';--}}

{{--    let contentShow = 'support__type--show';--}}
{{--    let contentHide = 'support__type--hide';--}}

{{--    btnPrivate.on("click", function () {--}}
{{--        $(this).removeClass(btnInactive).addClass(btnActive);--}}
{{--        $(btnCompany).removeClass(btnActive).addClass(btnInactive);--}}
{{--        $(contentPrivate).removeClass(contentHide).addClass(contentShow);--}}
{{--        $(contentCompany).removeClass(contentShow).addClass(contentHide);--}}
{{--    });--}}

{{--    btnCompany.on("click", function () {--}}
{{--        $(this).removeClass(btnInactive).addClass(btnActive);--}}
{{--        $(btnPrivate).removeClass(btnActive).addClass(btnInactive);--}}
{{--        $(contentCompany).removeClass(contentHide).addClass(contentShow);--}}
{{--        $(contentPrivate).removeClass(contentShow).addClass(contentHide);--}}
{{--    });--}}

{{--    $(".donation__box--private").on("click", function() {--}}
{{--        $(".donation__box--private").removeClass("active");--}}
{{--        $(this).addClass("active");--}}
{{--    });--}}

{{--    $(".donation__box--company").on("click", function() {--}}
{{--        $(".donation__box--company").removeClass("active");--}}
{{--        $(this).addClass("active");--}}
{{--    });--}}
{{--</script>--}}