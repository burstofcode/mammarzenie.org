<?php

namespace Theme\Blocks\Support;

use Rafflex\AcfComposer\Block;
use StoutLogic\AcfBuilder\FieldsBuilder;
use Theme\Blocks\Support\assets\Icon;
use function Theme\Blocks\reusable\align;
use function Theme\Blocks\reusable\className;

/**
 * Class Support
 * @package Theme\Blocks\Achievements
 */
class Support extends Block
{
    /**
     * Default field type configuration.
     *
     * @return array
     */
    protected $defaults = [];

    /**
     * Data to be passed to the block before registering.
     * @see https://www.advancedcustomfields.com/resources/acf_register_block_type/
     *
     * @return array
     */
    public function register()
    {
        return [
            'name' => 'support',
            'title' => __( 'Support' ),
            'description' => __( 'List of fulfilled dreams' ),
            'category' => 'sections',
            'icon' => Icon::svg(),
            'post_types' => ['page'],
            'supports' => [
                'align' => ['full', 'wide']
            ]
        ];
    }

    /**
     * Fields to be attached to the block.
     *
     * @return array
     * @throws \StoutLogic\AcfBuilder\FieldNameCollisionException
     */
    public function fields()
    {
        $support = new FieldsBuilder( 'support' );

        $support
            ->setLocation( 'block', '==', 'acf/support' );

        $support
            ->addText( 'title', [
                'label' => __( 'Title' )
            ] );

        $support
            ->addTab('Private')
                ->addText( 'private_tab_name', [
                    'label' => __( 'Tab name' )
                ] )
                    ->addRepeater('private_support', [
                        'label' => __('Support types'),
                        'required' => 1,
                        'min' => 1,
                        'layout' => 'block',
                        'button_label' => __( 'Add New' )
                    ])
                        ->addSelect( 'font_icon', [
                            'label' => __( 'Font Icon' ),
                            'required' => 1,
                            'wrapper' => [
                                'width' => 65,
                                'id' => 'font_icon__select'
                            ],
                            'choices' => [
                                'fmm__make-a-donation' =>       __( '<i class="fmm__make-a-donation"></i><span>Make a donation</span>' ),
                                'fmm__become-a-volunteer' =>    __( '<i class="fmm__become-a-volunteer"></i><span>Become a volunteer</span>' ),
                                'fmm__donate-one-percent' =>    __( '<i class="fmm__donate-one-percent"></i><span>Donate 1%</span>' ),
                                'fmm__adopt-the-dream' =>       __( '<i class="fmm__adopt-the-dream"></i><span>Adopt a dream</span>' ),
                                'fmm__flowers-for-dreams' =>    __( '<i class="fmm__flowers-for-dreams"></i><span>Flowers for a dream</span>' ),
                                'fmm__download-the-survey' =>   __( '<i class="fmm__download-the-survey"></i><span>Download the survey</span>' ),
                                'fmm__volunteer-meeting' =>     __( '<i class="fmm__volunteer-meeting"></i><span>Volunteer meeting</span>' ),
                                'fmm__trial' =>                 __( '<i class="fmm__trial"></i><span>Trial</span>' ),
                                'fmm__make-decision' =>         __( '<i class="fmm__make-decision"></i><span>Make decision</span>' ),
                                'fmm__pass-test' =>             __( '<i class="fmm__pass-test"></i><span>Pass test</span>' ),
                                'fmm__financial-support' =>     __( '<i class="fmm__financial-support"></i><span>Financial support</span>' ),
                                'fmm__employees' =>             __( '<i class="fmm__employees"></i><span>Employees</span>' ),
                                'fmm__join-us' =>               __( '<i class="fmm__join-us"></i><span>Join us</span>' ),
                                'fmm__charity-purchase' =>      __( '<i class="fmm__charity-purchase"></i><span>Charity purchase</span>' )
                            ],
                            'default_value' => [],
                            'allow_null' => 0,
                            'multiple' => 0,
                            'ui' => 1,
                            'ajax' => 1,
                            'return_format' => 'array',
                        ] )
                        ->conditional( 'icon_type', '==', 1 )
                        ->addImage( 'image_icon', [
                            'label' => __( 'Image Icon' ),
                            'required' => 1,
                            'return_format' => 'array',
                            'wrapper' => [
                                'width' => 65
                            ]
                        ] )
                        ->conditional( 'icon_type', '==', 0 )
            // Switches to choose font icon or image icon
                        ->addTrueFalse( 'icon_type', [
                            'label' => __( 'Icon type' ),
                            'default_value' => 1,
                            'ui' => 1,
                            'ui_on_text' => __( 'FONT' ),
                            'ui_off_text' => __( 'IMAGE' ),
                            'wrapper' => [
                                'width' => 35
                            ]
                        ] )
                        ->addTextarea('title', [
                            'label' => __('Title'),
                            'rows' => '2',
                            'new_lines' => 'br',
                        ])
                        ->addPageLink('link', [
                            'label' => __('Link'),
                            'post_type' => [],
                            'taxonomy' => [],
                            'allow_null' => 0,
                            'allow_archives' => 1,
                            'multiple' => 0,
                        ]);
        $support
            ->addTextarea('private_info', [
                'label' => __('Description'),
                'maxlength' => 500,
                'rows' => '5',
                'new_lines' => 'br',
            ])
            ->addText( 'private_btn_name', [
                'label' => __( 'Button Name' ),
                'wrapper' => [
                    'width' => 50,
                ],
            ] )
            ->addPageLink('private_btn_url', [
                'label' => __('Button Link'),
                'post_type' => [],
                'taxonomy' => [],
                'allow_null' => 0,
                'allow_archives' => 1,
                'multiple' => 0,
                'wrapper' => [
                    'width' => 50,
                ],
            ]);

        $support
            ->addTab('Company')
            ->addText( 'company_tab_name', [
                'label' => __( 'Tab name' )
            ] )
            ->addRepeater('company_support', [
                'label' => __('Support types'),
                'required' => 1,
                'min' => 1,
                'layout' => 'block',
                'button_label' => __( 'Add New' ),
            ])
            ->addSelect( 'font_icon', [
                'label' => __( 'Font Icon' ),
                'required' => 1,
                'wrapper' => [
                    'width' => 65,
                    'id' => 'font_icon__select'
                ],
                'choices' => [
                    'fmm__make-a-donation' =>       __( '<i class="fmm__make-a-donation"></i><span>Make a donation</span>' ),
                    'fmm__become-a-volunteer' =>    __( '<i class="fmm__become-a-volunteer"></i><span>Become a volunteer</span>' ),
                    'fmm__donate-one-percent' =>    __( '<i class="fmm__donate-one-percent"></i><span>Donate 1%</span>' ),
                    'fmm__adopt-the-dream' =>       __( '<i class="fmm__adopt-the-dream"></i><span>Adopt a dream</span>' ),
                    'fmm__flowers-for-dreams' =>    __( '<i class="fmm__flowers-for-dreams"></i><span>Flowers for a dream</span>' ),
                    'fmm__download-the-survey' =>   __( '<i class="fmm__download-the-survey"></i><span>Download the survey</span>' ),
                    'fmm__volunteer-meeting' =>     __( '<i class="fmm__volunteer-meeting"></i><span>Volunteer meeting</span>' ),
                    'fmm__trial' =>                 __( '<i class="fmm__trial"></i><span>Trial</span>' ),
                    'fmm__make-decision' =>         __( '<i class="fmm__make-decision"></i><span>Make decision</span>' ),
                    'fmm__pass-test' =>             __( '<i class="fmm__pass-test"></i><span>Pass test</span>' ),
                    'fmm__financial-support' =>     __( '<i class="fmm__financial-support"></i><span>Financial support</span>' ),
                    'fmm__employees' =>             __( '<i class="fmm__employees"></i><span>Employees</span>' ),
                    'fmm__join-us' =>               __( '<i class="fmm__join-us"></i><span>Join us</span>' ),
                    'fmm__charity-purchase' =>      __( '<i class="fmm__charity-purchase"></i><span>Charity purchase</span>' )
                ],
                'default_value' => [],
                'allow_null' => 0,
                'multiple' => 0,
                'ui' => 1,
                'ajax' => 1,
                'return_format' => 'array',
            ] )
            ->conditional( 'icon_type', '==', 1 )
            ->addImage( 'image_icon', [
                'label' => __( 'Image Icon' ),
                'required' => 1,
                'return_format' => 'array',
                'wrapper' => [
                    'width' => 65
                ]
            ] )
            ->conditional( 'icon_type', '==', 0 )
            // Switches to choose font icon or image icon
            ->addTrueFalse( 'icon_type', [
                'label' => __( 'Icon type' ),
                'default_value' => 1,
                'ui' => 1,
                'ui_on_text' => __( 'FONT' ),
                'ui_off_text' => __( 'IMAGE' ),
                'wrapper' => [
                    'width' => 35
                ]
            ] )
            ->addTextarea('title', [
                'label' => __('Title'),
                'rows' => '2',
                'new_lines' => 'br',
            ])
            ->addPageLink('link', [
                'label' => __('Link'),
                'post_type' => [],
                'taxonomy' => [],
                'allow_null' => 0,
                'allow_archives' => 1,
                'multiple' => 0,
            ]);
        $support
            ->addTextarea('company_info', [
                'label' => __('Description'),
                'maxlength' => 500,
                'rows' => '5',
                'new_lines' => 'br',
            ])
            ->addText( 'company_btn_name', [
                'label' => __( 'Button Name' ),
                'wrapper' => [
                    'width' => 50,
                ],
            ] )
            ->addPageLink('company_btn_url', [
                'label' => __('Button Link'),
                'post_type' => [],
                'taxonomy' => [],
                'allow_null' => 0,
                'allow_archives' => 1,
                'multiple' => 0,
                'wrapper' => [
                    'width' => 50,
                ],
            ]);

        return $support->build();
    }

    /**
     * Data to be passed to the rendered block.
     *
     * @return array
     */
    public function with()
    {
        return [
            'align' => align($this->block),
            'class_name' => className($this->block),

            'title' => $this->title(),

            'private_tab' => $this->privateTab(),
            'private_support' => $this->privateSupport(),
            'private_info' => $this->privateInfo(),
            'private_btn_name' => $this->privateBtnName(),
            'private_btn_url' => $this->privateBtnUrl(),

            'company_tab' => $this->companyTab(),
            'company_support' => $this->companySupport(),
            'company_info' => $this->companyInfo(),
            'company_btn_name' => $this->companyBtnName(),
            'company_btn_url' => $this->companyBtnUrl(),
        ];
    }

    /**
     * Returns the title field.
     *
     * @return array
     */
    public function title()
    {
        return get_field( 'title' );
    }

    /**
     * @return mixed
     */
    public function privateTab()
    {
        return get_field('private_tab_name');
    }

    /**
     * @return array|mixed
     */
    public function privateSupport()
    {
        return get_field('private_support') ?? [];
    }

    /**
     * @return mixed
     */
    public function privateInfo()
    {
        return get_field('private_info');
    }

    /**
     * @return mixed
     */
    public function privateBtnName()
    {
        return get_field('private_btn_name');
    }

    /**
     * @return mixed
     */
    public function privateBtnUrl()
    {
        return get_field('private_btn_url');
    }

    /**
     * @return mixed
     */
    public function companyTab()
    {
        return get_field('company_tab_name');
    }

    /**
     * @return array|mixed
     */
    public function companySupport()
    {
        return get_field('company_support') ?? [];
    }

    /**
     * @return mixed
     */
    public function companyInfo()
    {
        return get_field('company_info');
    }

    /**
     * @return mixed
     */
    public function companyBtnName()
    {
        return get_field('company_btn_name');
    }

    /**
     * @return mixed
     */
    public function companyBtnUrl()
    {
        return get_field('company_btn_url');
    }

}
