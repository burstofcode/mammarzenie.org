<?php

namespace Theme\Blocks\Example;

use Rafflex\AcfComposer\Block;
use StoutLogic\AcfBuilder\FieldsBuilder;

class Example extends Block
{
    /**
     * Default field type configuration.
     *
     * @return array
     */
    protected $defaults = [];

    /**
     * Data to be passed to the block before registering.
     * @see https://www.advancedcustomfields.com/resources/acf_register_block_type/
     *
     * @return array
     */
    public function register()
    {
        return [
            'name' => 'Example',
            'description' => 'Lorem ipsum',
            'category' => 'sections',
            'icon' => 'welcome-comments'
        ];
    }

    /**
     * Fields to be attached to the block.
     *
     * @return array
     */
    public function fields()
    {
        $example = new FieldsBuilder('example');

        $example
            ->setLocation('block', '==', 'acf/example');

        $example
            ->addText('label')
            ->addTextarea('description')
            ->addRepeater('items')
            ->addText('item')
            ->endRepeater();

        return $example->build();
    }

    /**
     * Data to be passed to the rendered block.
     *
     * @return array
     */
    public function with()
    {
        return [
            'label' => $this->label(),
            'description' => $this->description(),
            'items' => $this->items(),
        ];
    }

    /**
     * Returns the label field.
     *
     * @return string
     */
    public function label()
    {
        return get_field('label');
    }

    /**
     * Returns the description field.
     *
     * @return string
     */
    public function description()
    {
        return get_field('description');
    }

    /**
     * Returns the items field.
     *
     * @return array
     */
    public function items()
    {
        return get_field('items') ?? [];
    }
}
