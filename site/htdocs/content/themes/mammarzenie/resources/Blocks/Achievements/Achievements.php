<?php

namespace Theme\Blocks\Achievements;

use App\Models\Dreamer;
use Rafflex\AcfComposer\Block;
use StoutLogic\AcfBuilder\FieldsBuilder;
use Theme\Blocks\Achievements\assets\Icon;
use function Theme\Blocks\reusable\align;
use function Theme\Blocks\reusable\className;

/**
 * Class Achievements
 * @package Theme\Blocks\Achievements
 */
class Achievements extends Block
{
    /**
     * Default field type configuration.
     *
     * @return array
     */
    protected $defaults = [];

    /**
     * Data to be passed to the block before registering.
     * @see https://www.advancedcustomfields.com/resources/acf_register_block_type/
     *
     * @return array
     */
    public function register()
    {
        return [
            'name' => 'achievements',
            'title' => __( 'Achievements' ),
            'description' => __( 'List of fulfilled dreams' ),
            'category' => 'sections',
            'icon' => Icon::svg(),
            'post_types' => ['page'],
            'supports' => [
                'align' => ['full', 'wide']
            ]
        ];
    }

    /**
     * Fields to be attached to the block.
     *
     * @return array
     * @throws \StoutLogic\AcfBuilder\FieldNameCollisionException
     */
    public function fields()
    {
        $achievements = new FieldsBuilder( 'achievements' );

        $achievements
            ->setLocation( 'block', '==', 'acf/achievements' );


        $achievements
            ->addText( 'title', [
                'label' => __( 'Title' ),
            ] )
            ->addText( 'button', [
                'label' => __( 'Button name' ),
            ] );

        return $achievements->build();
    }

    /**
     * Data to be passed to the rendered block.
     *
     * @return array
     */
    public function with()
    {
        return [
            'align' => align($this->block),
            'class_name' => className($this->block),

            'title' => $this->title(),
            'button' => $this->button(),
            'placeholder' => $this->placeholder(),
        ];
    }

    /**
     * Returns the title field.
     *
     * @return array
     */
    public function title()
    {
        return get_field( 'title' );
    }

    /**
     * Returns the button name field.
     *
     * @return array
     */
    public function button()
    {
        return get_field( 'button' );
    }

    public function placeholder()
    {
        return app('wp.theme')->getUrl('dist/images/aktualnosc.jpg');
    }
}
