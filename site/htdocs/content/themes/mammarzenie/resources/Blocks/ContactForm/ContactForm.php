<?php

namespace Theme\Blocks\ContactForm;

use App\Services\PathService;
use Theme\Blocks\BaseBlock;
use StoutLogic\AcfBuilder\FieldsBuilder;
use Theme\Blocks\ContactForm\assets\Icon;
use function Theme\Blocks\reusable\align;
use function Theme\Blocks\reusable\className;

/**
 * Class ContactDetails
 * @package Theme\Blocks\ContactDetails
 */
class ContactForm extends BaseBlock
{
    /**
     * Default field type configuration.
     *
     * @return array
     */
    protected $defaults = [];

    /**
     * Data to be passed to the block before registering.
     * @see https://www.advancedcustomfields.com/resources/acf_register_block_type/
     *
     * @return array
     */
    public function register()
    {
        return [
            'name' => 'contact-form',
            'title' => __('Contact Form'),
            'description' => __('List of ways to contact with'),
            'category' => 'advanced',
            'icon' => Icon::svg(),
            'post_types' => ['page'],
            'supports' => [
                'align' => ['full', 'wide']
            ]
        ];
    }

    /**
     * Fields to be attached to the block.
     *
     * @return array
     * @throws \StoutLogic\AcfBuilder\FieldNameCollisionException
     */
    public function fields()
    {
        $contact_form = new FieldsBuilder( 'contact_form' );

        $contact_form
            ->setLocation( 'block', '==', 'acf/contact-form' );

        $contact_form
            ->addText( 'title', [
                'label' => __( 'Title' )
            ] )
            ->addText( 'shortcode', [
                'label' => __( 'Shortcode' )
            ] );

        return $contact_form->build();
    }

    /**
     * Overwrite the view function.
     *
     * @return array|string
     * @throws \Throwable
     */
    public function view()
    {
        if (view($view = "ContactForm/views/{$this->slug}")) {
            return view(
                $view,
                array_merge($this->with(), ['block' => $this->block])
            )->render();
        }

        return view(
            __DIR__ . '/../resources/views/view-404.blade.php',
            ['view' => $view]
        )->render();
    }

    /**
     * Data to be passed to the rendered block.
     *
     * @return array
     */
    public function with()
    {
        return [
            'align' => align($this->block),
            'class_name' => className($this->block),

            'title' => $this->title(),
            'shortcode' => $this->shortcode(),
        ];
    }

    /**
     * Returns the title field.
     *
     * @return string
     */
    public function title()
    {
        return get_field( 'title' );
    }

    /**
     * Returns the shortcode field.
     *
     * @return string
     */
    public function shortcode()
    {
        return get_field( 'shortcode' );
    }
}
