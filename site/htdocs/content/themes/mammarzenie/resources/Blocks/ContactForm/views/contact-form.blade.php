<div class="contact-form container {{ $align }} {{ $class_name }}">
    <div class="container">
        <div class="grid flex--justify-center">
            <div class="grid__column--12 grid__column--lg-6">
                @if( $shortcode )
                    <div class="contact-form__title">
                        <h2 class="title">{{ $title }}</h2>
                    </div>
                    <div class="contact-form__form">
                        {{ $shortcode }}
                    </div>
                @else
                    <p style="text-align: center">Add your form's shortcode to view this block.</p>
                @endif

            </div>
        </div>
    </div>
</div>