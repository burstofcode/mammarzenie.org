<?php

namespace Theme\Blocks\Newsletter;

use App\Models\Branch;
use App\Models\Volunteer;
use App\Models\Wish;
use Rafflex\AcfComposer\Block;
use StoutLogic\AcfBuilder\FieldsBuilder;
use Theme\Blocks\Newsletter\assets\Icon;
use function Theme\Blocks\reusable\align;
use function Theme\Blocks\reusable\className;

class Newsletter extends Block
{
    /**
     * Default field type configuration.
     *
     * @return array
     */
    protected $defaults = [];

    /**
     * Data to be passed to the block before registering.
     * @see https://www.advancedcustomfields.com/resources/acf_register_block_type/
     *
     * @return array
     */
    public function register()
    {
        return [
            'name' => 'newsletter',
            'title' => __('Newsletter'),
            'description' => __('Newsletter sign-up block.'),
            'category' => 'sections',
            'icon' => Icon::svg(),
            'post_types' => ['page'],
            'supports' => [
                'align' => ['full', 'wide']
            ]
        ];
    }

    /**
     * Fields to be attached to the block.
     *
     * @return array
     * @throws \StoutLogic\AcfBuilder\FieldNameCollisionException
     */
    public function fields()
    {
        $newsletter = new FieldsBuilder('newsletter');

        $newsletter
            ->setLocation('block', '==', 'acf/newsletter');

        $newsletter
            ->addText('title', [
                'label' => __('Title'),
            ])
            ->addText('input_placeholder', [
                'label' => __('Input placeholder'),
            ])
            ->addText('button', [
                'label' => __('Button'),
            ]);
        return $newsletter->build();
    }

    /**
     * Data to be passed to the rendered block.
     *
     * @return array
     */
    public function with()
    {
        return [
            'align' => align($this->block),
            'class_name' => className($this->block),

            'title' => $this->title(),
            'input_placeholder' => $this->inputPlaceholder(),
            'button' => $this->button(),
        ];
    }

    /**
     * @return mixed
     */
    public function title()
    {
        return get_field('title');
    }

    /**
     * @return mixed
     */
    public function inputPlaceholder()
    {
        return get_field('input_placeholder');
    }

    /**
     * @return mixed
     */
    public function button()
    {
        return get_field('button');
    }
}
