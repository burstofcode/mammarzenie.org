<?php

namespace Theme\Blocks\Hero;

use Rafflex\AcfComposer\Block;
use StoutLogic\AcfBuilder\FieldsBuilder;
use Theme\Blocks\Hero\assets\Icon;
use function Theme\Blocks\reusable\align;
use function Theme\Blocks\reusable\className;

class Hero extends Block
{
    /**
     * Default field type configuration.
     *
     * @return array
     */
    protected $defaults = [];

    /**
     * Data to be passed to the block before registering.
     * @see https://www.advancedcustomfields.com/resources/acf_register_block_type/
     *
     * @return array
     */
    public function register()
    {
        return [
            'name' => 'hero',
            'title' => __('Hero'),
            'description' => __('Main hero banner on front page'),
            'category' => 'sections',
            'icon' => Icon::svg(),
            'post_types' => ['page'],
            'supports' => [
                'align' => ['full', 'wide']
            ]
        ];
    }

    /**
     * Fields to be attached to the block.
     *
     * @return array
     * @throws \StoutLogic\AcfBuilder\FieldNameCollisionException
     */
    public function fields()
    {
        $hero = new FieldsBuilder('hero');

        $hero
            ->setLocation('block', '==', 'acf/hero');

        $hero
            ->addRepeater('slides', [
                'label' => __('Slides'),
                'required' => 0,
                'min' => 1,
                'max' => 4,
                'layout' => 'block',
                'button_label' => __('Add slide'),
                'wrapper' => [
                    'width' => 100
                ],
            ])
                ->addText('title', [
                    'label' => __('Title'),
                    'wrapper' => [
                        'width' => 100
                    ]
                ])
                ->addText('tagline', [
                    'label' => __('Tagline'),
                    'wrapper' => [
                        'width' => 100
                    ]
                ])
                ->addText('btn_name', [
                    'label' => __('Button name'),
                    'wrapper' => [
                        'width' => 50
                    ]
                ])
                ->addPageLink('btn_link', [
                    'label' => __('Button link'),
                    'wrapper' => [
                        'width' => 50
                    ],
                    'post_type' => ['page', 'post'],
                    'allow_null' => 0,
                    'allow_archives' => 0,
                    'multiple' => 0,
                    'ui' => 1
                ])
                ->addImage('photo', [
                    'label' => __('Background photo'),
                    'return_format' => 'array',
                    'wrapper' => [
                        'width' => 50
                    ]
                ])
                ->addRange('opacity_level', [
                    'label' => __('Overlay level'),
                    'default_value' => '15',
                    'min' => '0',
                    'max' => '100',
                    'append' => '%',
                    'wrapper' => [
                        'width' => 50
                    ]
                ])
            ->endRepeater();

        return $hero->build();
    }

    /**
     * Data to be passed to the rendered block.
     *
     * @return array
     */
    public function with()
    {
        return [
            'align' => align($this->block),
            'class_name' => className($this->block),

            'slides' => $this->slides(),
            'placeholder' => $this->placeholder(),
        ];
    }

    /**
     * Returns the slides field.
     *
     * @return array
     */
    public function slides()
    {
        return get_field('slides') ?? [];
    }

    /**
     * Returns image placeholder.
     * @return mixed
     */
    public function placeholder()
    {
        return app('wp.theme')->getUrl('dist/images/placeholder_1440x585.jpg');
    }

    public function icon()
    {
        svgIcon();
    }
}
