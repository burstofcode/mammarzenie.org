<section class="hero container {{ $align }} {{ $class_name }}">
    <div id="js-hero" class="swiper-container hero__container">
        <div class="swiper-wrapper hero__wrapper">
            @php($counter = 0)

            @if($slides)
            @foreach($slides as $slide)

            @php($counter = $counter + 1)
            <div id="slide-{{ $counter }}" class="swiper-slide hero__slide" style="background-image: url(
              @if($slide['photo'])
               {{ $slide['photo']['url'] }}
              @else
                {{ $placeholder }}
              @endif
               )">
                <div class="container h--100">
                    <div class="grid h--100">
                        <div class="js-hero__captions captions grid__column--12 grid__column--lg-6">

                            @if($slide['title'])
                            <h1 class="hero__title">
                                {{ $slide['title'] }}
                            </h1>
                            @endif

                            @if($slide['tagline'])
                            <h2 class="hero__tagline h3">
                                {{ $slide['tagline'] }}
                            </h2>
                            @endif

                            @if($slide['btn_name'])
                            <a href="{{ $slide['btn_link'] }}" class="hero__button button button--light button--outlined">
                                {{ $slide['btn_name'] }}
                            </a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <style>
                #slide-{{$counter}}::before {
                    opacity: calc({{ $slide['opacity_level'] }} / 100);
                }
            </style>

            @endforeach
            @endif

        </div>

        @if($counter > 1)
        <div class="swiper-pagination"></div>
        @endif

    </div>

</section>