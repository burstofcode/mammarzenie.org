<?php

namespace Theme\Blocks\Team;

use App\Models\Branch;
use Theme\Blocks\BaseBlock;
use StoutLogic\AcfBuilder\FieldsBuilder;
use Theme\Blocks\Team\assets\Icon;
use function Theme\Blocks\reusable\align;
use function Theme\Blocks\reusable\className;

class Team extends BaseBlock
{
    /**
     * Default field type configuration.
     *
     * @return array
     */
    protected $defaults = [];

    /**
     * Data to be passed to the block before registering.
     * @see https://www.advancedcustomfields.com/resources/acf_register_block_type/
     *
     * @return array
     */
    public function register()
    {
        return [
            'name' => 'team',
            'title' => __('Team'),
            'description' => __('Team block'),
            'category' => 'advanced',
            'icon' => Icon::svg(),
            'post_types' => ['page'],
            'supports' => [
                'align' => ['full', 'wide']
            ]
        ];
    }

    /**
     * Fields to be attached to the block.
     *
     * @return array
     * @throws \StoutLogic\AcfBuilder\FieldNameCollisionException
     */
    public function fields()
    {
        $team = new FieldsBuilder( 'team' );

        $team
            ->setLocation( 'block', '==', 'acf/team' );

        $team
            // Single team member
            ->addRepeater( 'members', [
                'label' => __( 'Team members' ),
                'min' => 1,
                'layout' => 'block',
                'button_label' => __( 'Add member' )
            ])
                // Icon as font icon or as a image
                ->addImage( 'photo', [
                    'label' => __( 'Photo' ),
                    'required' => 1,
                    'return_format' => 'array'
                ] )
                // Position
                ->addText( 'name', [
                    'label' => __( 'Name' ),
                    'required' => 1
                ] )
                // Position
                ->addText( 'position', [
                    'label' => __( 'Position' ),
                    'required' => 1
                ] )
                // Content
                ->addTextarea( 'content', [
                    'label' => __( 'Content' ),
                    'required' => 1,
                    'maxlength' => '1200',
                    'rows' => '6'
                ] )
            ->endRepeater();

        return $team->build();
    }

    /**
     * Data to be passed to the rendered block.
     *
     * @return array
     */
    public function with()
    {
        return [
            'align' => align($this->block),
            'class_name' => className($this->block),

            'members' => $this->members(),
            'placeholder' => $this->placeholder(),
        ];
    }

    /**
     * Returns the members field.
     *
     * @return array
     */
    public function members()
    {
        return get_field( 'members' ) ?? [];
    }

    public function placeholder()
    {
        return $this->avatarPlaceholder();
    }
}
