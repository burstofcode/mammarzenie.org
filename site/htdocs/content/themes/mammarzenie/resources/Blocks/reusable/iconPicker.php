<?php

namespace Theme\Blocks\iconPicker;

use StoutLogic\AcfBuilder\FieldsBuilder;

function fields()
{
$icon_picker = new FieldsBuilder('icon_picker');

$icon_picker
// Icon as font icon or as a image
->addSelect( 'font_icon', [
'label' => __( 'Font Icon' ),
'required' => 1,
'wrapper' => [
'width' => 65,
'id' => 'font_icon__select'
],
'choices' => [
'fmm__make-a-donation' =>       __( '<i class="fmm__make-a-donation"></i><span>Make a donation</span>' ),
'fmm__become-a-volunteer' =>    __( '<i class="fmm__become-a-volunteer"></i><span>Become a volunteer</span>' ),
'fmm__donate-one-percent' =>    __( '<i class="fmm__donate-one-percent"></i><span>Donate 1%</span>' ),
'fmm__adopt-the-dream' =>       __( '<i class="fmm__adopt-the-dream"></i><span>Adopt a dream</span>' ),
'fmm__flowers-for-dreams' =>    __( '<i class="fmm__flowers-for-dreams"></i><span>Flowers for a dream</span>' ),
'fmm__download-the-survey' =>   __( '<i class="fmm__download-the-survey"></i><span>Download the survey</span>' ),
'fmm__volunteer-meeting' =>     __( '<i class="fmm__volunteer-meeting"></i><span>Volunteer meeting</span>' ),
'fmm__trial' =>                 __( '<i class="fmm__trial"></i><span>Trial</span>' ),
'fmm__make-decision' =>         __( '<i class="fmm__make-decision"></i><span>Make decision</span>' ),
'fmm__pass-test' =>             __( '<i class="fmm__pass-test"></i><span>Pass test</span>' ),
'fmm__financial-support' =>     __( '<i class="fmm__financial-support"></i><span>Financial support</span>' ),
'fmm__employees' =>             __( '<i class="fmm__employees"></i><span>Employees</span>' ),
'fmm__join-us' =>               __( '<i class="fmm__join-us"></i><span>Join us</span>' ),
'fmm__charity-purchase' =>      __( '<i class="fmm__charity-purchase"></i><span>Charity purchase</span>' )
],
'default_value' => [],
'allow_null' => 0,
'multiple' => 0,
'ui' => 1,
'ajax' => 1,
'return_format' => 'array',
] )
->conditional( 'icon_type', '==', 1 )
->addImage( 'image_icon', [
'label' => __( 'Image Icon' ),
'required' => 1,
'return_format' => 'array',
'wrapper' => [
'width' => 65
]
] )
->conditional( 'icon_type', '==', 0 )
// Switches to choose font icon or image icon
->addTrueFalse( 'icon_type', [
'label' => __( 'Icon type' ),
'default_value' => 0,
'ui' => 1,
'ui_on_text' => __( 'FONT' ),
'ui_off_text' => __( 'IMAGE' ),
'wrapper' => [
'width' => 35
]
] );

return $icon_picker->build();
}