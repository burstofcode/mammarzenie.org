<?php

namespace Theme\Blocks\Donation;

use StoutLogic\AcfBuilder\FieldsBuilder;
use Theme\Blocks\BaseBlock;
use Theme\Blocks\Donation\assets\Icon;
use function Theme\Blocks\reusable\align;
use function Theme\Blocks\reusable\className;

/**
 * Class Donation
 * @package Theme\Blocks\Donation
 */
class Donation extends BaseBlock
{
    /**
     * Default field type configuration.
     *
     * @return array
     */
    protected $defaults = [];

    /**
     * Data to be passed to the block before registering.
     * @see https://www.advancedcustomfields.com/resources/acf_register_block_type/
     *
     * @return array
     */
    public function register()
    {
        return [
            'name' => 'donation',
            'title' => __('Donation'),
            'description' => __('Donation form'),
            'category' => 'sections',
            'icon' => Icon::svg(),
            'post_types' => ['page'],
            'supports' => [
                'align' => ['full', 'wide']
            ]
        ];
    }

    /**
     * Fields to be attached to the block.
     *
     * @return array
     * @throws \StoutLogic\AcfBuilder\FieldNameCollisionException
     */
    public function fields()
    {
        $donation = new FieldsBuilder( 'donation' );

        $donation
            ->setLocation( 'block', '==', 'acf/donation' );

        $donation
            ->addText( 'title', [
                'label' => __( 'Title' )
            ] )
            ->addText( 'tab_one_time', [
                'label' => __( 'One Time Donation Tab' )
            ] )
            ->addText( 'tab_recurring', [
                'label' => __( 'Recurring Donation Tab' )
            ] )
            ->addText( 'static_label', [
                'label' => __( 'Static Option Label' )
            ] )
            ->addText( 'custom_label', [
                'label' => __( 'Custom Option Label' )
            ] )
            ->addText( 'currency', [
                'label' => __( 'Currency' )
            ] )
            ->addText( 'invoice', [
                'label' => __( 'Add Invoice Label' )
            ] )
            ->addText( 'submit', [
                'label' => __( 'Submit Button Name' )
            ] );

        return $donation->build();
    }

    /**
     * Data to be passed to the rendered block.
     *
     * @return array
     */
    public function with()
    {
        return [
            'align' => align($this->block),
            'class_name' => className($this->block),

            'title' => $this->title(),
            'tab_one_time' => $this->tabOneTime(),
            'tab_recurring' => $this->tabRecurring(),
            'static_label' => $this->staticLabel(),
            'custom_label' => $this->customLabel(),
            'currency' => $this->currency(),
            'invoice' => $this->invoice(),
            'submit' => $this->submit()
        ];
    }

    /**
     * @return array
     */
    public function title()
    {
        return get_field( 'title' );
    }

    /**
     * @return array
     */
    public function tabOneTime()
    {
        return get_field( 'tab_one_time' );
    }

    /**
     * @return array
     */
    public function tabRecurring()
    {
        return get_field( 'tab_recurring' );
    }

    /**
     * @return array
     */
    public function staticLabel()
    {
        return get_field( 'static_label' );
    }

    /**
     * @return array
     */
    public function customLabel()
    {
        return get_field( 'custom_label' );
    }

    /**
     * @return array
     */
    public function currency()
    {
        return get_field( 'currency' );
    }

    /**
     * @return array
     */
    public function invoice()
    {
        return get_field( 'invoice' );
    }

    /**
     * @return array
     */
    public function submit()
    {
        return get_field( 'submit' );
    }

}
