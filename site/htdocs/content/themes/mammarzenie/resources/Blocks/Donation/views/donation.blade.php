<section class="donation container {{ $align }} {{ $class_name }}">
    {{--Section header --}}
    <header class="container">
        <div class="grid flex--justify-center">
            <div class="grid__column--12 grid__column--lg-8 block__header">

                @if( $title )
                    <h2 class="block__title text-primary">{{ $title }}</h2>
                @endif

            </div>
        </div>
    </header>

    <div class="container">
        <div class="donation__tabs">
            <ul class="donation__list list--unstyled">
                @if( $tab_one_time )
                    <li class="donation__item">
                        <button id="js-support-button-private" class="button__type--private button button--filled button--primary">{{ $tab_one_time }}</button>
                    </li>
                @endif
                @if( $tab_recurring )
                    <li class="donation__item">
                        <button id="js-support-button-company" class="button__type--company button button--outlined button--primary">{{ $tab_recurring }}</button>
                    </li>
                @endif
            </ul>
        </div>
    </div>
    <div class="container donation__panes">

        <div id="js-support-content-private" class="donation__content donation__type donation__type--private support__type--show">
            <div class="donation__boxes">
                <div class="donation__single text--center">
                    <div class="donation__box donation__box--private">
                        <p class="donation__description">{{ $static_label }}</p>
                        <h3 class="donation__amount">10</h3>
                        <p class="donation__currency">{{ $currency }}</p>
                    </div>
                </div>
                <div class="donation__single text--center">
                    <div class="donation__box donation__box--private">
                        <p class="donation__description">{{ $static_label }}</p>
                        <h3 class="donation__amount">20</h3>
                        <p class="donation__currency">{{ $currency }}</p>
                    </div>
                </div>
                <div class="donation__single text--center">
                    <div class="donation__box donation__box--private active">
                        <p class="donation__description">{{ $static_label }}</p>
                        <h3 class="donation__amount">50</h3>
                        <p class="donation__currency">{{ $currency }}</p>
                    </div>
                </div>
                <div class="donation__single text--center">
                    <div class="donation__box donation__box--private">
                        <p class="donation__description">{{ $static_label }}</p>
                        <h3 class="donation__amount">100</h3>
                        <p class="donation__currency">{{ $currency }}</p>
                    </div>
                </div>
                <div class="donation__single donation__single--input text--center">
                    <div class="donation__box donation__box--private">
                        <p class="donation__description">{{ $custom_label }}</p>
                        <input type="number" class="donation__input">
                        <p class="donation__currency">{{ $currency }}</p>
                    </div>
                </div>
            </div>
            <div class="grid donation__submit-area">
                <div class="donation__invoice-tick grid__column--12 grid__column--lg-6 flex--justify-end">
                    <div class="form-group">
                        <input name="want-invoice--private" type="checkbox" id="want-invoice--private">
                        <label for="want-invoice--private">{!! $invoice !!}</label>
                    </div>
                </div>
                <div class="donation__submit-button grid__column--12 grid__column--lg-6 flex--justify-end">
                    <a href="" class="donation__more button button--filled button--primary">{{ $submit }}</a>
                </div>
            </div>
        </div>

        <div id="js-support-content-company" class="donation__content donation__type donation__type--company support__type--hide">
            <div class="donation__boxes">
                <div class="donation__single text--center">
                    <div class="donation__box donation__box--company">
                        <p class="donation__description">{{ $static_label }}</p>
                        <h3 class="donation__amount">10</h3>
                        <p class="donation__currency">{{ $currency }}</p>
                    </div>
                </div>
                <div class="donation__single text--center">
                    <div class="donation__box donation__box--company">
                        <p class="donation__description">{{ $static_label }}</p>
                        <h3 class="donation__amount">20</h3>
                        <p class="donation__currency">{{ $currency }}</p>
                    </div>
                </div>
                <div class="donation__single text--center">
                    <div class="donation__box donation__box--company active">
                        <p class="donation__description">{{ $static_label }}</p>
                        <h3 class="donation__amount">50</h3>
                        <p class="donation__currency">{{ $currency }}</p>
                    </div>
                </div>
                <div class="donation__single text--center">
                    <div class="donation__box donation__box--company">
                        <p class="donation__description">{{ $static_label }}</p>
                        <h3 class="donation__amount">100</h3>
                        <p class="donation__currency">{{ $currency }}</p>
                    </div>
                </div>
                <div class="donation__single donation__single--input text--center">
                    <div class="donation__box donation__box--company">
                        <p class="donation__description">{{ $custom_label }}</p>
                        <input type="number" class="donation__input">
                        <p class="donation__currency">{{ $currency }}</p>
                    </div>
                </div>
            </div>
            <div class="grid donation__submit-area">
                <div class="donation__invoice-tick grid__column--12 grid__column--lg-6 flex--justify-end">
                    <div class="form-group">
                        <input name="want-invoice--company" type="checkbox" id="want-invoice--company">
                        <label for="want-invoice--company">{!! $invoice !!}</label>
                    </div>
                </div>
                <div class="donation__submit-button grid__column--12 grid__column--lg-6 flex--justify-end">
                    <a href="" class="donation__more button button--filled button--primary">{{ $submit }}</a>
                </div>
            </div>
        </div>

    </div>

</section>
