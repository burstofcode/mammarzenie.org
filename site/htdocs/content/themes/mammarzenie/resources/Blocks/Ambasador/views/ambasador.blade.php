<section class=" ambasador container {{ $align }} {{ $class_name }}">
    <div class="container">
        <div class="grid">
            <div class="ambasador__left grid__column--12 grid__column--md-5 grid__column--lg-6">
                <img src="{{ $photo['url'] }}" alt="{{ $photo['alt'] }}" class="ambasador__photo">
            </div>
            <div class="ambasador__right grid__column--12 grid__column--md-7 grid__column--lg-6">
                <i class="ambasador__quote fmm__quote"></i>
                <h5 class="ambasador__info">
                    {{ $content }}
                </h5>

                <div class="ambasador__more">
                    <a href="{{ $button_url }}" class="ambasador__link">
                        {{ $button_name }} >
                    </a>
                </div>

                <p class="ambasador__name">{{ $signature_name }}</p>
                <p class="ambasador__position">{{ $signature_title }}</p>
            </div>
        </div>
    </div>
</section>