<?php

namespace Theme\Blocks\Ambasador;

use Rafflex\AcfComposer\Block;
use StoutLogic\AcfBuilder\FieldsBuilder;
use Theme\Blocks\Ambasador\assets\Icon;
use function Theme\Blocks\reusable\align;
use function Theme\Blocks\reusable\className;

class Ambasador extends Block
{
    /**
     * Default field type configuration.
     *
     * @return array
     */
    protected $defaults = [];

    /**
     * Data to be passed to the block before registering.
     * @see https://www.advancedcustomfields.com/resources/acf_register_block_type/
     *
     * @return array
     */
    public function register()
    {
        return [
            'name' => 'ambasador',
            'title' => __('Ambasador'),
            'description' => __('Short info about ambasador.'),
            'category' => 'sections',
            'icon' => Icon::svg(),
            'post_types' => ['page'],
            'supports' => [
                'align' => ['full', 'wide']
            ]
        ];
    }

    /**
     * Fields to be attached to the block.
     *
     * @return array
     * @throws \StoutLogic\AcfBuilder\FieldNameCollisionException
     */
    public function fields()
    {
        $ambasador = new FieldsBuilder('ambasador');

        $ambasador
            ->setLocation('block', '==', 'acf/ambasador');

        $ambasador
            ->addImage('photo', [
                'label' => __('Photo'),
                'return_format' => 'array',
            ])
            ->addTextarea('content', [
                'label' => __('Content'),
                'rows' => '5',
                'new_lines' => 'br',
            ])
            ->addText('button_name', [
                'label' => __('Button name'),
                'wrapper' => [
                    'width' => 50
                ]
            ])
            ->addPageLink('button_url', [
                'label' => __('Button URL'),
                'wrapper' => [
                    'width' => 50
                ],
                'post_type' => ['page', 'post'],
                'allow_null' => 1,
                'allow_archives' => 1,
                'multiple' => 0,
                'ui' => 1
            ])
            ->addText('signature_name', [
                'label' => __('Name'),
                'wrapper' => [
                    'width' => 100
                ]
            ])
            ->addText('signature_title', [
                'label' => __('Title'),
                'wrapper' => [
                    'width' => 100
                ]
            ]);

        return $ambasador->build();
    }

    /**
     * Data to be passed to the rendered block.
     *
     * @return array
     */
    public function with()
    {
        return [
            'align' => align($this->block),
            'class_name' => className($this->block),

            'photo' => $this->photo(),
            'content' => $this->content(),
            'button_name' => $this->buttonName(),
            'button_url' => $this->buttonUrl(),
            'signature_name' => $this->signatureName(),
            'signature_title' => $this->signatureTitle(),
        ];
    }


    /**
     * @return mixed
     */
    public function photo()
    {
        return get_field('photo');
    }

    /**
     * @return mixed
     */
    public function content()
    {
        return get_field('content');
    }

    /**
     * @return mixed
     */
    public function buttonName()
    {
        return get_field('button_name');
    }

    /**
     * @return mixed
     */
    public function buttonUrl()
    {
        return get_field('button_url');
    }

    /**
     * @return mixed
     */
    public function signatureName()
    {
        return get_field('signature_name');
    }

    /**
     * @return mixed
     */
    public function signatureTitle()
    {
        return get_field('signature_title');
    }
}
