<section  class="pending-wish dreamers-loop dreamers-loop--grid-view container {{ $align }} {{ $class_name }}">
    <header class="container">
        <div class="grid flex--justify-center">
            <div class="grid__column--12 grid__column--lg-7 block__header">
                    <h2 class="block__title text-primary">
                        @if($title)
                            {{ $title }}
                        @else
                            Tytuł. Oczekujące marzenia
                        @endif
                    </h2>
                    <h6 class="block__tagline p">
                        @if($tagline)
                            {{ $tagline }}
                        @else
                            Podtytuł. Oto nasi podopieczni [...]
                        @endif
                    </h6>
                <i class="fmm__princess"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span><span class="path20"></span><span class="path21"></span><span class="path22"></span><span class="path23"></span><span class="path24"></span><span class="path25"></span><span class="path26"></span><span class="path27"></span><span class="path28"></span><span class="path29"></span><span class="path30"></span></i>
                <i class="fmm__stars-ver2"></i>
            </div>
        </div>
    </header>
    {{-- Section content - slider --}}
    <div id="js-pending-wish" class="pending-wish--grid-view container">
        <div class="grid pending-wish__wrap">

            @foreach($dreamers as $dreamer)

                <div class="dreamers-loop__single grid__column--12 grid__column--sm-6 grid__column--md-4 grid__column--lg-3">
                    <a href="#link" class="dreamers-loop__link">
                        <figure class="dreamers-loop__figure">
                            @inject('image', 'App\Services\ImageService')
                            <div class="dreamers-loop__photo" style="background-image: url(
                        @if(isset($dreamer->image->fname))
                            {{ home_url() }}{!! '/dreamers/images' . $image::imagePathPart($dreamer->image->dreamer_id) . $dreamer->image->dreamer_id . '/' . $dreamer->image->fname !!}
                            @else
                            {{ get_stylesheet_directory_uri() . '/dist/images/default_avatar.svg' }}
                            @endif
                        )"></div>
                            <figcaption class="dreamers-loop__caption">
                                <p class="dreamers-loop__info">
                                <span class="dreamers-loop__name">
                                    {{ $dreamer->firstname }}
                                </span>,
                                    <span class="dreamers-loop__age">
                                    @inject('age', 'App\Services\AgeService')
                                        {{ $age::calculateAge( $dreamer->dateOfBirth
                                            ->birth_date ) }}
                                        @inject('word', 'App\Services\WordService')
                                        {{ $word::modifyWordYear($age::calculateAge( $dreamer
                                            ->dateOfBirth->birth_date )) }}
                                </span>
                                </p>
                                <p class="dreamers-loop__excerpt">
                                    Moje marzenie: {{ $dreamer->wish->contents }}
                                </p>
                            </figcaption>
                        </figure>
                    </a>
                </div>

            @endforeach

        </div>
        <div class="container text--center">
            <a href="{{ home_url('/marzyciele/oczekujace/cala-polska/wszystkie/desc/20') }}" class="pending-wish__button button button--filled button--primary">
                @if($button)
                    {{ $button }}
                @else
                    Więcej marzycieli...
                @endif
            </a>
        </div>
    </div>
</section>
