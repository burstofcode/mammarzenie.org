<?php

namespace Theme\Blocks\PendingWish;

use App\Models\Dreamer;
use App\Services\RemoteHostService;
use Rafflex\AcfComposer\Block;
use StoutLogic\AcfBuilder\FieldsBuilder;
use Theme\Blocks\PendingWish\assets\Icon;
use function Theme\Blocks\reusable\align;
use function Theme\Blocks\reusable\className;

class PendingWish extends Block
{
    /**
     * Default field type configuration.
     *
     * @return array
     */
    protected $defaults = [];

    /**
     * Data to be passed to the block before registering.
     * @see https://www.advancedcustomfields.com/resources/acf_register_block_type/
     *
     * @return array
     */
    public function register()
    {
        return [
            'name' => 'pending-wish',
            'title' => __('Pending Wish'),
            'description' => __('List of pending wishes.'),
            'category' => 'sections',
            'icon' => Icon::svg(),
            'post_types' => ['page'],
            'supports' => [
                'align' => ['full', 'wide']
            ]
        ];
    }

    /**
     * Fields to be attached to the block.
     *
     * @return array
     * @throws \StoutLogic\AcfBuilder\FieldNameCollisionException
     */
    public function fields()
    {
        $pending_wish = new FieldsBuilder('pending_wish');

        $pending_wish
            ->setLocation('block', '==', 'acf/pending-wish');

        $pending_wish
            ->addText('title', [
                'label' => __('Title'),
                'required' => 1
            ])
            ->addText('tagline', [
                'label' => __('Tagline'),
                'required' => 1
            ])
            ->addText('button', [
                'label' => __('Button'),
                'required' => 1,
                'wrapper' => [
                    'width' => 50
                ],
            ])
            ->addSelect('to_show', [
                'label' => __('To Show'),
                'required' => 1,
                'allow_null' => 0,
                'multiple' => 0,
                'default_value' => 'random',
                'choices' => [
                    'newest' => __('Newest'),
                    'random' => __('Random')
                ],
                'wrapper' => [
                    'width' => 50
                ],
            ])
            ;

        return $pending_wish->build();
    }

    /**
     * Data to be passed to the rendered block.
     *
     * @return array
     */
    public function with()
    {
        return [
            'align' => align($this->block),
            'class_name' => className($this->block),

            'title' => $this->title(),
            'tagline' => $this->tagline(),
            'button' => $this->button(),
            'dreamers' => $this->dreamers(),
            'remote_image_path' => $this->remoteImagePath(),
        ];
    }

    /**
     * Overwrite the view function.
     *
     * @return array|string
     * @throws \Throwable
     */
    public function view()
    {
        if (view($view = "blocks/{$this->slug}")) {
            return view(
                $view,
                array_merge($this->with(), ['block' => $this->block])
            )->render();
        }

        return view(
            __DIR__ . '/../resources/views/view-404.blade.php',
            ['view' => $view]
        )->render();
    }

    /**
     * Returns the title field.
     *
     * @return array
     */
    public function title()
    {
        return get_field( 'title' );
    }

    /**
     * Returns the tagline field.
     *
     * @return array
     */
    public function tagline()
    {
        return get_field( 'tagline' );
    }

    /**
     * Returns the button name field.
     *
     * @return array
     */
    public function button()
    {
        return get_field( 'button' );
    }

    /**
     * @return mixed
     */
    public function dreamers()
    {
        return Dreamer::randomPendingWishes();
    }

    public function remoteImagePath()
    {
        return RemoteHostService::dreamerImagePath();
    }
}
