<?php

namespace Theme\Blocks\IconBlock;

use App\Models\Branch;
use Rafflex\AcfComposer\Block;
use StoutLogic\AcfBuilder\FieldsBuilder;
use Theme\Blocks\IconBlock\assets\Icon;
use function Theme\Blocks\reusable\align;
use function Theme\Blocks\reusable\className;

class IconBlock extends Block
{
    /**
     * Default field type configuration.
     *
     * @return array
     */
    protected $defaults = [];

    /**
     * Data to be passed to the block before registering.
     * @see https://www.advancedcustomfields.com/resources/acf_register_block_type/
     *
     * @return array
     */
    public function register()
    {
        return [
            'name' => 'icon-block',
            'title' => __('Icon Block'),
            'description' => __('Icon with text'),
            'category' => 'advanced',
            'icon' => Icon::svg(),
            'post_types' => ['page'],
            'supports' => [
                'align' => ['full', 'wide']
            ]
        ];
    }

    /**
     * Fields to be attached to the block.
     *
     * @return array
     * @throws \StoutLogic\AcfBuilder\FieldNameCollisionException
     */
    public function fields()
    {
        $icon_block = new FieldsBuilder( 'icon-block' );

        $icon_block
            ->setLocation( 'block', '==', 'acf/icon-block' );

        $icon_block
            // Block title
            ->addText( 'title', [
                'label' => __( 'Title' )
            ] )
            // Single icon block
            ->addRepeater( 'icon_blocks', [
                'label' => __( 'Icon Blocks' ),
                'required' => 1,
                'min' => 1,
                'layout' => 'block',
                'button_label' => __( 'Add New' ),
            ] )
            // Icon as font icon or as a image
            ->addSelect( 'font_icon', [
                'label' => __( 'Font Icon' ),
                'required' => 1,
                'wrapper' => [
                    'width' => 65,
                    'id' => 'font_icon__select'
                ],
                'choices' => [
                    'fmm__make-a-donation' =>       __( '<i class="fmm__make-a-donation"></i><span>Make a donation</span>' ),
                    'fmm__become-a-volunteer' =>    __( '<i class="fmm__become-a-volunteer"></i><span>Become a volunteer</span>' ),
                    'fmm__donate-one-percent' =>    __( '<i class="fmm__donate-one-percent"></i><span>Donate 1%</span>' ),
                    'fmm__adopt-the-dream' =>       __( '<i class="fmm__adopt-the-dream"></i><span>Adopt a dream</span>' ),
                    'fmm__flowers-for-dreams' =>    __( '<i class="fmm__flowers-for-dreams"></i><span>Flowers for a dream</span>' ),
                    'fmm__download-the-survey' =>   __( '<i class="fmm__download-the-survey"></i><span>Download the survey</span>' ),
                    'fmm__volunteer-meeting' =>     __( '<i class="fmm__volunteer-meeting"></i><span>Volunteer meeting</span>' ),
                    'fmm__trial' =>                 __( '<i class="fmm__trial"></i><span>Trial</span>' ),
                    'fmm__make-decision' =>         __( '<i class="fmm__make-decision"></i><span>Make decision</span>' ),
                    'fmm__pass-test' =>             __( '<i class="fmm__pass-test"></i><span>Pass test</span>' ),
                    'fmm__financial-support' =>     __( '<i class="fmm__financial-support"></i><span>Financial support</span>' ),
                    'fmm__employees' =>             __( '<i class="fmm__employees"></i><span>Employees</span>' ),
                    'fmm__join-us' =>               __( '<i class="fmm__join-us"></i><span>Join us</span>' ),
                    'fmm__charity-purchase' =>      __( '<i class="fmm__charity-purchase"></i><span>Charity purchase</span>' )
                ],
                'default_value' => [],
                'allow_null' => 0,
                'multiple' => 0,
                'ui' => 1,
                'ajax' => 1,
                'return_format' => 'array',
            ] )
            ->conditional( 'icon_type', '==', 1 )
            ->addImage( 'image_icon', [
                'label' => __( 'Image Icon' ),
                'required' => 1,
                'return_format' => 'array',
                'wrapper' => [
                    'width' => 65
                ]
            ] )
            ->conditional( 'icon_type', '==', 0 )
            // Switches to choose font icon or image icon
            ->addTrueFalse( 'icon_type', [
                'label' => __( 'Icon type' ),
                'default_value' => 1,
                'ui' => 1,
                'ui_on_text' => __( 'FONT' ),
                'ui_off_text' => __( 'IMAGE' ),
                'wrapper' => [
                    'width' => 35
                ]
            ] )
            // Title
            ->addText( 'title', [
                'label' => __('Title'),
                'required' => 1
            ] )
            // Content
            ->addTextarea( 'content', [
                'label' => __( 'Content' ),
                'required' => 1,
                'maxlength' => '400',
                'rows' => '6'
            ] )
            // Switches to enable links
            ->addTrueFalse( 'enable_link_1', [
                'label' => __( 'Link 1' ),
                'default_value' => 0,
                'ui' => 1,
                'ui_on_text' => __( 'ON' ),
                'ui_off_text' => __( 'OFF' ),
                'wrapper' => [
                    'width' => 50
                ]
            ] )
            ->addTrueFalse( 'enable_link_2', [
                'label' => __( 'Link 2' ),
                'default_value' => 0,
                'ui' => 1,
                'ui_on_text' => __( 'ON' ),
                'ui_off_text' => __( 'OFF' ),
                'wrapper' => [
                    'width' => 50
                ]
            ] )
            ->conditional( 'enable_link_1', '==', 1 )
            // 1st link
            ->addText( 'external_1', [
                'label' => __( 'Link 1 (External)' ),
                'wrapper' => [
                    'width' => 70
                ]
            ])
            ->conditional( 'enable_link_1', '==', 1 )
            ->andCondition( 'link_type_1', '==', 1 )
            ->addPageLink( 'internal_1', [
                'label' => __( 'Link 1 (Internal)' ),
                'allow_null' => 0,
                'multiple' => 0,
                'return_format' => 'object',
                'ui' => 1,
                'wrapper' => [
                    'width' => 70
                ]
            ] )
            ->conditional( 'enable_link_1', '==', 1 )
            ->andCondition( 'link_type_1', '==', 0 )
            ->addTrueFalse( 'link_type_1', [
                'label' => __( 'Link 1 (Type)' ),
                'default_value' => 0,
                'ui' => 1,
                'ui_on_text' => __( 'EXT' ),
                'ui_off_text' => __( 'INT' ),
                'wrapper' => [
                    'width' => 30
                ]
            ])
            ->conditional( 'enable_link_1', '==', 1 )
            ->addText('link_title_1', [
                'label' => __( 'Link 1 (Title)' ),
                'wrapper' => [
                    'width' => 100
                ]
            ] )
            ->conditional( 'enable_link_1', '==', 1 )
            // 2nd link
            ->addText('external_2', [
                'label' => __( 'Link 2 (External)' ),
                'wrapper' => [
                    'width' => 70
                ]
            ])
            ->conditional( 'enable_link_2', '==', 1 )
            ->andCondition( 'enable_link_1', '==', 1 )
            ->andCondition( 'link_type_2', '==', 1 )
            ->addPageLink( 'internal_2', [
                'label' => __( 'Link 2 (Internal)' ),
                'allow_null' => 0,
                'multiple' => 0,
                'return_format' => 'object',
                'ui' => 1,
                'wrapper' => [
                    'width' => 70
                ]
            ])
            ->conditional( 'enable_link_2', '==', 1 )
            ->andCondition( 'enable_link_1', '==', 1 )
            ->andCondition( 'link_type_2', '==', 0 )
            ->addTrueFalse( 'link_type_2', [
                'label' => __( 'Link 2 (Type)' ),
                'default_value' => 0,
                'ui' => 1,
                'ui_on_text' => __( 'EXT' ),
                'ui_off_text' => __( 'INT' ),
                'wrapper' => [
                    'width' => 30
                ]
            ])
            ->conditional( 'enable_link_2', '==', 1 )
            ->andCondition( 'enable_link_1', '==', 1 )
            ->addText( 'link_title_2', [
                'label' => __( 'Link 2 (Title)' ),
                'wrapper' => [
                    'width' => 100
                ]
            ] )
            ->conditional( 'enable_link_2', '==', 1 )
            ->andCondition( 'enable_link_1', '==', 1 )
            ->endRepeater();

        return $icon_block->build();
    }

    /**
     * Data to be passed to the rendered block.
     *
     * @return array
     */
    public function with()
    {
        return [
            'align' => align($this->block),
            'class_name' => className($this->block),

            'title' => $this->title(),
            'icon_blocks' => $this->iconBlocks(),
        ];
    }

    /**
     * Overwrite the view function.
     *
     * @return array|string
     * @throws \Throwable
     */
    public function view()
    {
        if (view($view = "IconBlock/views/{$this->slug}")) {
            return view(
                $view,
                array_merge($this->with(), ['block' => $this->block])
            )->render();
        }

        return view(
            __DIR__ . '/../resources/views/view-404.blade.php',
            ['view' => $view]
        )->render();
    }

    /**
     * @return mixed
     */
    public function title()
    {
        return get_field( 'title' );
    }

    /**
     * @return array|mixed
     */
    public function iconBlocks()
    {
        return get_field( 'icon_blocks' ) ?? [];
    }
}
