<section class="icon-block pt--5 pb--6 pt--sm-6 pb--sm-7 pb--lg-8 container {{ $align }} {{ $class_name }}">

    <header class="container">
        <div class="flex--justify-center">
            <div class="block__header">
                <h2 class="block__title text-primary">
                @if($title)
                    {{ $title }}
                @else
                    IconBlock
                @endif
                </h2>

            </div>
        </div>
    </header>

    <div class="container">
        <div class="grid">
            @if($icon_blocks)
                @foreach($icon_blocks as $icon_block)
                    <div class="icon-block__single grid__column--12 grid__column--md-6 grid__column--lg-4">

                        <!-- Font icon -->
                        @if($icon_block['icon_type'] == 1 && $icon_block["font_icon"])
                            <div class="icon-block__icon">
                                <i class="icon-block__font {{ $icon_block["font_icon"]["value"] }}"></i>
                            </div>

                            <!-- Image icon -->
                        @elseif($icon_block['icon_type'] == 0 && $icon_block["font_icon"])
                            <div class="icon-block__icon">
                                <img class="icon-block__image" src="{{ $icon_block['image_icon']['url'] }}" alt="{{ $icon_block['image_icon']['alt'] }}">
                            </div>

                            <!-- Fallback icon -->
                        @else
                            <div class="icon-block__icon"></div>
                        @endif

                    <!-- Title -->
                        <h5 class="icon-block__title">{{ $icon_block['title'] }}</h5>

                        <!-- Content -->
                        <p>{{ $icon_block['content'] }}</p>

                        <!-- Link 1 -->
                        @if($icon_block['enable_link_1'] == 1)
                            @php
                                if($icon_block['link_type_1'] == 1 and $icon_block['external_1']) {
                                $url = $icon_block['external_1'];
                                $target = '_blank';
                                } elseif($icon_block['link_type_1'] == 0 and $icon_block['internal_1']) {
                                $url = $icon_block['internal_1'];
                                $target = '_self';
                                } else {
                                $url = 'https://www.mammarzenie.org';
                                $target = '_self';
                                }
                            @endphp
                            @if($icon_block['link_title_1'])
                                <a class="icon-block__link icon-block__link--1" href="{{ $url }}" target="{{ $target }}">{{ $icon_block['link_title_1'] }} ></a>
                            @endif
                        @endif

                    <!-- Link 2 -->
                        @if($icon_block['enable_link_1'] == 1 and $icon_block['enable_link_2'] == 1)
                            @php
                                if($icon_block['link_type_2'] == 1 and $icon_block['external_2']) {
                                $url = $icon_block['external_2'];
                                $target = '_blank';
                                } elseif($icon_block['link_type_2'] == 0 and $icon_block['internal_2']) {
                                $url = $icon_block['internal_2'];
                                $target = '_self';
                                } else {
                                $url = 'https://www.mammarzenie.org';
                                $target = '_self';
                                }
                            @endphp
                            @if($icon_block['link_title_2'])
                                <a class="icon-block__link icon-block__link--2" href="{{ $url }}" target="{{ $target }}">{{ $icon_block['link_title_2'] }} ></a>
                            @endif
                        @endif

                    </div>
                @endforeach
            @endif
        </div>
    </div>
</section>

