<section class="branches pt--5 pb--6 pt--sm-6 pb--sm-7 pb--lg-8 container {{ $align }} {{ $class_name }}">
    {{--Section header --}}
    <header class="container">
        <div class="flex--justify-center">
            <div class="block__header">
                <h2 class="block__title text-primary">
                @if($title)
                    {{ $title }}
                @else
                    Branches
                @endif
                </h2>
            </div>
        </div>
    </header>
    {{-- Section content --}}
    <div class="container">
        <ul class="branches__list">
            @foreach ( $branches as $branch )
                <li class="branches__item">
                    <a href="{{ home_url('/') }}{{ $branch->slug }}" class="branches__link">{{ $branch->city }}</a>
                </li>
            @endforeach
        </ul>
    </div>
</section>
