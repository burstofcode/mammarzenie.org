<?php

namespace Theme\Blocks\Branches;

use App\Models\Branch;
use Rafflex\AcfComposer\Block;
use StoutLogic\AcfBuilder\FieldsBuilder;
use Theme\Blocks\Branches\assets\Icon;
use function Theme\Blocks\reusable\align;
use function Theme\Blocks\reusable\className;

class Branches extends Block
{
    /**
     * Default field type configuration.
     *
     * @return array
     */
    protected $defaults = [];

    /**
     * Data to be passed to the block before registering.
     * @see https://www.advancedcustomfields.com/resources/acf_register_block_type/
     *
     * @return array
     */
    public function register()
    {
        return [
            'name' => 'branches',
            'title' => __('Branches'),
            'description' => __('List of all branches'),
            'category' => 'sections',
            'icon' => Icon::svg(),
            'post_types' => ['page'],
            'supports' => [
                'align' => ['full', 'wide']
            ]
        ];
    }

    /**
     * Fields to be attached to the block.
     *
     * @return array
     * @throws \StoutLogic\AcfBuilder\FieldNameCollisionException
     */
    public function fields()
    {
        $branches = new FieldsBuilder( 'branches' );

        $branches
            ->setLocation( 'block', '==', 'acf/branches' );

        $branches
            ->addText( 'title', [
                'label' => __( 'Title' ),
            ] );

        return $branches->build();
    }

    /**
     * Data to be passed to the rendered block.
     *
     * @return array
     */
    public function with()
    {
        return [
            'align' => align($this->block),
            'class_name' => className($this->block),

            'title' => $this->title(),
            'branches' => $this->branches(),
        ];
    }

    /**
     * @return mixed
     */
    public function title()
    {
        return get_field( 'title' );
    }

    /**
     * @return Branch[]|\Illuminate\Database\Eloquent\Collection
     */
    public function branches()
    {
        return Branch::list();
    }
}
