<?php

namespace Theme\Blocks\Slogan;

use StoutLogic\AcfBuilder\FieldsBuilder;
use Theme\Blocks\BaseBlock;
use Theme\Blocks\Slogan\assets\Icon;
use function Theme\Blocks\reusable\align;
use function Theme\Blocks\reusable\className;

/**
 * Class Slogan
 * @package Theme\Blocks\Slogan
 */
class Slogan extends BaseBlock
{
    /**
     * Default field type configuration.
     *
     * @return array
     */
    protected $defaults = [];

    /**
     * Data to be passed to the block before registering.
     * @see https://www.advancedcustomfields.com/resources/acf_register_block_type/
     *
     * @return array
     */
    public function register()
    {
        return [
            'name' => 'slogan',
            'title' => __('Slogan'),
            'description' => __('Big text on background'),
            'category' => 'advanced',
            'icon' => Icon::svg(),
            'post_types' => ['page'],
            'supports' => [
                'align' => ['full', 'wide']
            ]
        ];
    }

    /**
     * Fields to be attached to the block.
     *
     * @return array
     * @throws \StoutLogic\AcfBuilder\FieldNameCollisionException
     */
    public function fields()
    {
        $slogan = new FieldsBuilder( 'slogan' );

        $slogan
            ->setLocation( 'block', '==', 'acf/slogan' );

        $slogan
            ->addTextarea( 'title', [
                'label' => __( 'Title' ),
                'rows' => 3,
                'required' => 1,
                'maxlength' => 200,
                'wrapper' => [
                    'width' => 100
                ]
            ])
            ->setInstructions( __( 'You can use up to 200 characters' ) );

        return $slogan->build();
    }

    /**
     * Data to be passed to the rendered block.
     *
     * @return array
     */
    public function with()
    {
        return [
            'align' => align($this->block),
            'class_name' => className($this->block),

            'title' => $this->title(),
        ];
    }

    /**
     * @return mixed
     */
    public function title()
    {
        return get_field( 'title' );
    }
}
