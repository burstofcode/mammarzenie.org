<section class="slogan pt--5 pb--6 pt--sm-6 pb--sm-7 pb--lg-8 container {{ $align }} {{ $class_name }}">
    <header class="container">
        <div class="grid flex--justify-center">
            <div class="grid__column--12 grid__column--lg-10 block__header">
                @if($title)
                    <h2 class="block__title text-primary">{{ $title }}</h2>
                @endif
            </div>
        </div>
    </header>
</section>