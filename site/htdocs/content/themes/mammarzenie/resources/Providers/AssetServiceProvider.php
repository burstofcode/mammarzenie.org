<?php

namespace Theme\Providers;

use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;
use Themosis\Core\ThemeManager;
use Themosis\Support\Facades\Action;
use Themosis\Support\Facades\Asset;

/**
 * Class AssetServiceProvider
 * @package Theme\Providers
 */
class AssetServiceProvider extends ServiceProvider
{
    /**
     * Theme Assets
     *
     * Here we define the loaded assets from our previously defined
     * "dist" directory. Assets sources are located under the root "assets"
     * directory and are then compiled, thanks to Laravel Mix, to the "dist"
     * folder.
     *
     * @see https://laravel-mix.com/
     */
    public function register()
    {
        /** @var ThemeManager $theme */
        $theme = $this->app->make('wp.theme');
        $font_family = 'Poppins:400,700';

        Asset::add('theme_styles', 'styles/theme.css', [], $theme->getHeader('version'))->to('front');
        Asset::add('theme_woo', 'styles/woocommerce.css', ['theme_styles'], $theme->getHeader('version'))->to('front');
        Asset::add('theme_js', 'scripts/theme.js', [], $theme->getHeader('version'))->to('front');
        Asset::add('theme_font', "//fonts.googleapis.com/css?family=" . $font_family . '&display=swap&subset=latin-ext')->setType('style')->to('front');


//            Asset::add('theme_google_api', '//maps.googleapis.com/maps/api/js?key=' . env('GOOGLE_MAPS_API_KEY'), false, null, true);


        // Add custom style to Gutenberg Editor
        Action::add('enqueue_block_editor_assets', function() {
            add_theme_support( 'editor-styles' );
            wp_enqueue_style('editor/font', '//fonts.googleapis.com/css?family=Poppins:400,700&display=swap&subset=latin-ext', false ,null);
            wp_enqueue_style('editor/style', app('wp.theme')->getUrl('dist/styles/editor.css'), false ,null);
//            wp_enqueue_script('editor/script', app('wp.theme')->getUrl('dist/scripts/editor.js'), [ 'wp-block-editor', 'wp-i18n', 'wp-element', 'wp-components', 'wp-editor' ] ,null);
        });

        Asset::add('admin_styles', 'styles/admin.css', [], $theme->getHeader('version'))->to('admin');
    }
}
