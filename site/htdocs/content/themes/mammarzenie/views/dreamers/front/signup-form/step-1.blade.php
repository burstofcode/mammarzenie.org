{{-- STEP 1 --}}
<h3 class="section-title">Start</h3>
<section>
    <h4 class="start-heading">Ankieta zgłoszeniowa dziecka</h4>
    <p class="start-info">Wypełnianie ankiety możesz rozpocząć naciskając przycisk "Rozpocznij".</p>
</section>