{{-- STEP 5 --}}
<h3 class="section-title">Inny opiekun prawny</h3>
<section>

    <div class="section-heading grid">
        <div class="grid__column--12">
            <h6 class="section-name display--block">Inny opiekun prawny (jeżeli jest)</h6>
        </div>
    </div>

    <div class="grid">
        <div class="grid__column--12">
            <label for="full-name">Imię i nazwisko</label>
            <input id="full-name" name="full-name" type="text" class="">
        </div>
        <div class="grid__column--12">
            <label for="address-1">Adres zamieszkania</label>
            <textarea id="address-1" name="address-1" rows="3"></textarea>
        </div>
    </div>

    <div class="grid">
        <div class="grid__column--12">
            <label class="section-message">Numery telefonu innego opiekuna prawnego <span>(jeżeli inny niż dziecka)</span></label>
        </div>
    </div>

    <div class="grid">
        <div class="grid__column--12 grid__column--md-6">
            <label for="full-name">Komórka</label>
            <input id="full-name" name="full-name" type="number" class="">
        </div>
        <div class="grid__column--12 grid__column--md-6">
            <label for="full-name">Dom</label>
            <input id="full-name" name="full-name" type="number" class="">
        </div>
    </div>

    <div class="grid">
        <div class="grid__column--12 grid__column--md-6">
            <label for="full-name">Praca</label>
            <input id="full-name" name="full-name" type="number" class="">
        </div>
    </div>

</section>