{{-- STEP 7 --}}
<h3 class="section-title">Osoby zgłaszające dziecko</h3>
<section>

    <div class="section-heading grid">
        <div class="grid__column--12">
            <h6 class="section-name display--block">Osoby zgłaszające dziecko</h6>
        </div>
    </div>

    <div class="grid">
        <div class="grid__column--12">
            <label for="full-name">Imię i nazwisko</label>
            <input id="full-name" name="full-name" type="text">
        </div>
        <div class="grid__column--12">
            <label for="full-name">Adres email</label>
            <input id="full-name" name="full-name" type="email" placeholder="Na ten adres wysłane zostanie potwierdzenie odbioru ankiety">
        </div>
    </div>

    <div class="grid">
        <div class="grid__column--12 grid__column--md-6">
            <label>Pokrewieństwo z dzieckiem</label>
            <div class="form-group">
                <input type="radio" id="q10-yes" value="chłopiec" name="q10" class="radio">
                <label for="q10-yes">rodzic</label>
                <input type="radio" id="q10-no" value="dziewczynka" name="q10" class="radio">
                <label for="q10-no">inna osoba</label>
            </div>
        </div>
    </div>

    <div class="grid">
        <div class="grid__column--12">
            <label class="section-message">Numery telefonu</label>
        </div>
    </div>

    <div class="grid">
        <div class="grid__column--12 grid__column--md-6">
            <label for="full-name">Komórka</label>
            <input id="full-name" name="full-name" type="number" class="">
        </div>
        <div class="grid__column--12 grid__column--md-6">
            <label for="full-name">Dom</label>
            <input id="full-name" name="full-name" type="number" class="">
        </div>
    </div>

    <div class="grid">
        <div class="grid__column--12">
            <label class="section-message">Inne</label>
        </div>
    </div>

    <div class="grid">
        <div class="grid__column--12">
            <label for="full-name">Skąd osoba zgłaszająca dowiedziała się o naszej Fundacji?</label>
            <input id="full-name" name="full-name" type="text">
        </div>
    </div>

</section>