{{-- STEP 4 --}}
<h3 class="section-title">Rodzice / opiekunowie prawni</h3>
<section>

    <div class="section-heading grid">
        <div class="grid__column--12">
            <h6 class="section-name display--block">Rodzice / opiekunowie prawni</h6>
        </div>
    </div>

    <div class="grid">
        <div class="grid__column--12 grid__column--md-6">
            <label for="full-name">Imię i nazwisko ojca</label>
            <input id="full-name" name="full-name" type="text" class="">
        </div>
        <div class="grid__column--12 grid__column--md-6">
            <label for="full-name">Imię i nazwisko matki</label>
            <input id="full-name" name="full-name" type="text" class="">
        </div>
    </div>

    <div class="grid">
        <div class="grid__column--12">
            <label for="address-1">Adres zamieszkania</label>
            <textarea id="address-1" name="address-1" rows="3"></textarea>
        </div>
        <div class="grid__column--12">
            <label>Czy oboje rodziców posiadają prawa rodzicielskie?</label>
            <div class="form-group">
                <input type="radio" id="q4-yes" value="chłopiec" name="q4" class="radio">
                <label for="q4-yes">Tak</label>
                <input type="radio" id="q4-no" value="dziewczynka" name="q4" class="radio">
                <label for="q4-no">Nie</label>
            </div>
        </div>
    </div>

    <div class="grid">
        <div class="grid__column--12">
            <label class="section-message">Numery telefonów rodziców <span>(jeżeli inny niż dziecka)</span></label>
        </div>
    </div>

    <div class="grid">
        <div class="grid__column--12 grid__column--md-6">
            <label for="full-name">Komórka ojca</label>
            <input id="full-name" name="full-name" type="number" class="">
        </div>
        <div class="grid__column--12 grid__column--md-6">
            <label for="full-name">Komórka matki</label>
            <input id="full-name" name="full-name" type="number" class="">
        </div>
    </div>

    <div class="grid">
        <div class="grid__column--12 grid__column--md-6">
            <label for="full-name">Dom</label>
            <input id="full-name" name="full-name" type="number" class="">
        </div>
    </div>

</section>