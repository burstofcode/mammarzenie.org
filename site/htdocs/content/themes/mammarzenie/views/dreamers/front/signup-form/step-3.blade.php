{{-- STEP 3 --}}
<h3 class="section-title">Informacje o marzycielu</h3>
<section>

    <div class="section-heading grid">
        <div class="grid__column--12">
            <h6 class="section-name display--block">Informacje o marzycielu</h6>
        </div>
    </div>

    <div class="grid">
        <div class="grid__column--12">
            <label>Czy dziecko miało już spełniane marzenie przez Fundację Mam Marzenie?</label>
            <div class="form-group">
                <input type="radio" id="q1-yes" value="chłopiec" name="q1" class="radio">
                <label for="q1-yes">Tak</label>
                <input type="radio" id="q1-no" value="dziewczynka" name="q1" class="radio">
                <label for="q1-no">Nie</label>
            </div>
        </div>
        <div class="grid__column--12">
            <label>Czy dziecko jest opóźnione rozwojowo w stosunku do swojego wieku?</label>
            <div class="form-group">
                <input type="radio" id="q3-yes" value="chłopiec" name="q2" class="radio">
                <label for="q3-yes">Tak</label>
                <input type="radio" id="q3-no" value="dziewczynka" name="q2" class="radio">
                <label for="q3-no">Nie</label>
            </div>
        </div>
        <div class="grid__column--12">
            <label>Czy dziecko jest w stanie wyartykuować swoje marzenie?</label>
            <div class="form-group">
                <input type="radio" id="q2-yes" value="chłopiec" name="q3" class="radio">
                <label for="q2-yes">Tak</label>
                <input type="radio" id="q2-no" value="dziewczynka" name="q3" class="radio">
                <label for="q2-no">Nie</label>
            </div>
        </div>
        <div class="grid__column--12">
            <label for="diagnosis">Jeśli <strong>NIE</strong>, jaki jest sposób komunikowania z dzieckiem?</label>
            <input id="diagnosis" name="diagnosis" type="text">
        </div>
    </div>
</section>