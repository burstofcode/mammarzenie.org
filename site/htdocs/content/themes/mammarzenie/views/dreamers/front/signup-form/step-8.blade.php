{{-- STEP 8 --}}
<h3 class="section-title">RODO</h3>
<section>

    <div class="section-heading grid">
        <div class="grid__column--12">
            <h6 class="section-name display--block">RODO</h6>
        </div>
    </div>

    <div class="grid">
        <div class="grid__column--12">
            <div class="form-group">
                <input name="rodo" type="checkbox" id="rodo">
                <label for="rodo"><strong>Wyrażam zgodę</strong> na przetwarzanie danych osobowych zawartych w ankiecie zgłoszeniowej w celu przeprowadzenia czynności wymaganych w kwalifikacji dziecka do programu Fundacji Mam Marzenie. Wyrażenie zgody na przetwarzanie danych osobowych powiązane jest również ze zgodą na ujawnienie tajemnicy lekarskiej, tj. udzielenie informacji w zakresie stanu zdrowia oraz sposobu leczenia przez lekarza prowadzącego.<br><br>
                    <strong>Bez wyrażenia zgody na przetwarzanie danych osobowyc, wysłanie ankiety jest niemożliwe.</strong></label>
            </div>
        </div>
    </div>

</section>