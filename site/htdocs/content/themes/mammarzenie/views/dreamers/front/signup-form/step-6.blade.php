{{-- STEP 6 --}}
<h3 class="section-title">Informacje medyczne</h3>
<section>

    <div class="section-heading grid">
        <div class="grid__column--12">
            <h6 class="section-name display--block">Informacje medyczne</h6>
        </div>
    </div>

    <div class="grid">
        <div class="grid__column--12">
            <label for="full-name">Imię i nazwisko lekarza prowadzącego</label>
            <input id="full-name" name="full-name" type="text" class="">
        </div>
        <div class="grid__column--12">
            <label for="address-1">Nazwa placówki medycznej, gdzie dziecko jest leczone</label>
            <textarea id="address-1" name="address-1" rows="2"></textarea>
        </div>
        <div class="grid__column--12">
            <label for="address-1">Adres placówki</label>
            <textarea id="address-1" name="address-1" rows="3"></textarea>
        </div>
    </div>

    <div class="grid">
        <div class="grid__column--12">
            <label class="section-message">Numery telefonu lekarza</label>
        </div>
    </div>

    <div class="grid">
        <div class="grid__column--12 grid__column--md-6">
            <label for="full-name">Komórka</label>
            <input id="full-name" name="full-name" type="number" class="">
        </div>
        <div class="grid__column--12 grid__column--md-6">
            <label for="full-name">Dom</label>
            <input id="full-name" name="full-name" type="number" class="">
        </div>
    </div>

    <div class="grid">
        <div class="grid__column--12 grid__column--md-6">
            <label for="full-name">Praca</label>
            <input id="full-name" name="full-name" type="number" class="">
        </div>
    </div>

</section>