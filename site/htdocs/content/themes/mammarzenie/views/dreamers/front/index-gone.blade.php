@extends('layouts.main')

<script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous">
</script>
<script>
    let originalUrl = location.href;
    let currentUrl;
    if(originalUrl.indexOf('?') > 0) {
        currentUrl = originalUrl.substring(0, originalUrl.indexOf('?'));
    } else {
        currentUrl = originalUrl;
    }
</script>

@section('content')

    @include('parts.dreamer.baner')
    @include('parts.dreamer.gone-info')
    @include('parts.dreamer.content')
    @include('parts.dreamer.filters-bottom')

@endsection
