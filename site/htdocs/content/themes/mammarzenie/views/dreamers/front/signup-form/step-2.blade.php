{{-- STEP 2 --}}
<h3 class="section-title">Dane osobowe dziecka</h3>
<section>
    <div class="section-heading grid">
        <div class="grid__column--12">
            <h6 class="section-name display--block">Dane osobowe dziecka</h6>
        </div>
    </div>

    <div class="grid">
        <div class="grid__column--12">
            <label for="branch-filter">Do jakiego oddziału zgłaszasz dziecko?</label>
            <select class="dropdown__select" id="branch-filter">
                <option data-placeholder="true"></option>
                @foreach( $branches as $branch )
                    <option value="{{ $branch->slug }}">{{ $branch->city }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="grid">
        <div class="grid__column--12 grid__column--md-6">
            <label for="full-name">Imię i nazwisko dziecka</label>
            <input id="full-name" name="full-name" type="text" class="required">
        </div>
        <div class="grid__column--12 grid__column--md-6">
            <label for="pesel">Numer PESEL</label>
            <input id="pesel" name="pesel" type="number" class="">
        </div>
    </div>

    <div class="grid">
        <div class="grid__column--12 grid__column--md-6">
            <label for="dob">Data urodzenia</label>
            <input id="dob" name="dob" type="text" class="" placeholder="wg. wzoru rrrr-mm-dd">
        </div>
        <div class="grid__column--12 grid__column--md-6">
            <label>Płeć</label>
            <div class="form-group">
                <input type="radio" id="boy" value="chłopiec" name="radio-group" class="radio">
                <label for="boy">chłopiec</label>
                <input type="radio" id="girl" value="dziewczynka" name="radio-group" class="radio">
                <label for="girl">dziewczynka</label>
            </div>
        </div>
    </div>

    <div class="grid">
        <div class="grid__column--12 grid__column--md-6">
            <label for="mobile">Numer telefonu dziecka lub opiekuna</label>
            <input id="mobile" name="mobile" type="number">
        </div>
        <div class="grid__column--12 grid__column--md-6">
            <label for="diagnosis">Diagnoza choroby</label>
            <textarea id="diagnosis" name="address-1" rows="1"></textarea>
        </div>
    </div>

    <div class="grid">
        <div class="grid__column--12 grid__column--md-6">
            <label for="phone">Dom</label>
            <input id="phone" name="phone" type="number">
        </div>
        <div class="grid__column--12 grid__column--md-6">
            <label for="address-1">Adres zamieszkania</label>
            <textarea id="address-1" name="address-1" rows="2"></textarea>
        </div>
    </div>

    <div class="grid">
        <div class="grid__column--12 grid__column--md-6">
            <label for="siblings">Rodzeństwo</label>
            <textarea id="siblings" name="siblings" rows="2" placeholder="proszę podać imiona, wiek oraz czy mieszkają z dzieckiem"></textarea>
        </div>
        <div class="grid__column--12 grid__column--md-6">
            <label for="address-2">Adres zameldowania</label>
            <textarea id="address-2" name="address-2" rows="2" placeholder="jeżeli inny niż zamieszkania"></textarea>
        </div>
    </div>

</section>