<!-- Thanks to Pieter B. for helping out with the logistics -->
@extends('layouts.main')

@section('content')

    @include('parts.dreamer.baner')

    <div class="container">
        <form id="dreamer-application" action="#" class="dreamer-application">
            <div>

                @include('dreamers.front.signup-form.step-1')
                @include('dreamers.front.signup-form.step-2')
                @include('dreamers.front.signup-form.step-3')
                @include('dreamers.front.signup-form.step-4')
                @include('dreamers.front.signup-form.step-5')
                @include('dreamers.front.signup-form.step-6')
                @include('dreamers.front.signup-form.step-7')
                @include('dreamers.front.signup-form.step-8')

            </div>
        </form>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.19.1/jquery.validate.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-steps/1.1.0/jquery.steps.js"></script>
    <script>

        if (window.screen.width < 1070) {
            var stepList = "horizontal";
        } else {
            var stepList = "vertical";
        }

        const form = $("#dreamer-application");
        form.validate({
            errorPlacement: function errorPlacement(error, element) { },
            rules: {
                confirm: {
                    equalTo: "#password"
                }
            }
        });
        form.children("div").steps({
            headerTag: "h3",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            stepsOrientation: stepList,
            labels: {
                finish: "Wyślij",
                next: "Rozpocznij",
                previous: "Cofnij",
                loading: "Ładowanie ...",
            },
            onStepChanging: function (event, currentIndex, newIndex)
            {
                $('a[href$="next"]').text('Dalej');

                if ( currentIndex < newIndex ) {
                    form.validate().settings.ignore = ":disabled,:hidden";
                    return form.valid();
                } else {
                    return form;
                }
            },
            onFinishing: function (event, currentIndex)
            {
                form.validate().settings.ignore = ":disabled";
                return form.valid();
            },
            onFinished: function (event, currentIndex)
            {
                alert("Submitted!");
            }
        });

        $("input").on("keyup", function() {
            form.validate().element($(this));
        });
    </script>
@endsection