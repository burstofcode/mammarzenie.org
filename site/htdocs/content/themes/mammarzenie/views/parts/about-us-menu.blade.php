@if ($navigation)
    <ul class="nav__menu">
        @foreach ($navigation as $item)
            <li class="nav__item {{ $item->active ? 'active' : '' }}{{ $item->classes }}">
                <a href="{{ $item->url }}" class="nav__link">
                    {{ $item->label }}
                </a>

                @if ($item->children)
                    <ul class="nav__menu--child">
                        @foreach ($item->children as $child)
                            <li class="nav__item--child {{ $child->active ? 'active' : '' }}">
                                <a href="{{ $child->url }}" class="nav__link--child">
                                    {{ $child->label }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                @endif
            </li>
        @endforeach
    </ul>
@endif
