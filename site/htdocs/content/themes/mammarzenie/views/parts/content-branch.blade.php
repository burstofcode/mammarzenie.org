<section class="content">
    {!! $content !!}
</section>

<section class="feat-volunteers container">
    @if( have_rows('volunteers', $id) )
        <div class="grid">
            @while ( have_rows('volunteers', $id) ) @php( the_row() )
                <div class="volunteer grid__column--6">
                    <h5 class="volunteer__name">{{ the_sub_field('name') }}</h5>
                    <p class="volunteer__position">{{ the_sub_field('position') }}</p>
                    <p class="volunteer__phone">tel.: {{ the_sub_field('phone') }}</p>
                    <p class="volunteer__email">{{ the_sub_field('email') }}</p>
                </div>
            @endwhile
        </div>
    @endif

    <div class="container text--center">
        <a href="/wolontariat/" class="button button--filled button--primary">
            @if(isset($vol_btn))
                {{ $vol_btn }}
            @else
                Nadaj mi nazwę
            @endif
        </a>
    </div>

</section>