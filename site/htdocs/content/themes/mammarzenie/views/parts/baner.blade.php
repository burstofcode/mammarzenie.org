<div class="baner container--wide">
    <div class="container">
        <div class="baner__breadcrumbs">
            {{ bcn_display($return = false, $linked = true, $reverse = false, $force = false) }}
        </div>

        @if(is_single())
            @include('parts.post-nav')
        @endif

        @inject('menu', 'App\Services\AboutUsMenuService')
        @if( $menu->isActive(request()) )
            <div class="baner__menu container">
                @include('parts.about-us-menu')
            </div>
        @endif

        <div class="baner__heading">
            <h1 class="baner__title">
                {!! the_title() !!}
            </h1>
            @if( isset($post) )
                @if( $post->post_type == 'nasze-akcje' )
                    <p class="baner__category">Nasze akcje</p>
                @elseif( $post->post_type == 'imprezy-cykliczne' )
                    <p class="baner__category">Imprezy cykliczne</p>
                @endif
            @endif


            @if ( get_post_type() === 'co-robimy' and is_single() )
                <div class="category">
                    {!! $event_type_category !!}
                </div>
            @endif
        </div>

    </div>
</div>