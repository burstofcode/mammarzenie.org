<div class="baner container--wide">
    <div class="container">
        <div class="baner__breadcrumbs">
            @inject('view', 'App\Services\BreadcrumbService')
            {!! $view->breadcrumbs(request()) !!}
        </div>

        <div class="baner__heading">
            <h1 class="baner__title">
                Oddział {{ $title }}
            </h1>
        </div>

    </div>
</div>
