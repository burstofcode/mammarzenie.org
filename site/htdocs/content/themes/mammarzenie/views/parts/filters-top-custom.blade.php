

<div id="filters" class="filters--top container">
    <div class="grid">

        @if(!empty($event_type_filter))
            @include('components.filters.event-type')
        @endif

        @if(!empty($branch_filter))
            @include('components.filters.branch')
        @endif

        @if(!empty($event_time_frame_filter))
            @include('components.filters.event-time-frame')
        @endif

    </div>
</div>