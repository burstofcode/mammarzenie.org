@if ( $post->post_type != 'nasze-akcje' AND $post->post_type != 'imprezy-cykliczne' AND $post->post_type != 'post' )
    <div class="entry__meta">
    <span class="date">
      {{ get_the_date('d F Y', $post->ID) }}
    </span>
    @php($branches = App\Models\Branch::list())

    @foreach( $branches as $branch )
        @if( $post->post_type == $branch->slug )
            <span class="period">&nbsp;|&nbsp;</span>
            <span class="branch">
                Oddział {{ $branch->city }}
            </span>
        @endif
    @endforeach

    </div>
@endif
