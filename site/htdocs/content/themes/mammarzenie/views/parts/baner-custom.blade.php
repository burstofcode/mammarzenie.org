<div class="baner container--wide">
    <div class="container">
        <div class="baner__breadcrumbs">
            @include('components.breadcrumb')
        </div>

        <h1 class="baner__title text--center">
            {{ $title }}
        </h1>
    </div>
</div>