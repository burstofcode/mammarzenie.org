<div class="post-nav display--flex around">

    <div class="post-nav__prev display--inline-block">
        @php previous_post_link('%link', '<i class="post-nav__prev-icon fmm__chevron-left"></i><span class="post-nav__prev-text">Zobacz poprzednie</span>', false) @endphp
    </div>

    <div class="post-nav__next display--inline-block">
        @php next_post_link('%link', '<span class="post-nav__next-text">Zobacz następne</span><i class="post-nav__next-icon fmm__chevron-right"></i>', false) @endphp
    </div>

</div>