@if($posts)
    <div class="container">
        @foreach($posts as $post)
            <div class="post grid display--flex flex--justify-center">
                <div class="post__box grid__column--12 grid__column--lg-10">
                    <div class="grid">
                        <div class="post--left grid__column--12 grid__column--md-5 grid__column--lg-4">
                            <figure class="post__figure">
                                <a href="{{ get_the_permalink($post->ID) }}">
                                    <div class="post__image" style="background-image: url(
                                    @if ($post->thumbnail)
                                        {{ $post->thumbnail }}
                                    @else
                                        {{ $placeholder }}
                                    @endif
                                    )"></div>
                                </a>
                            </figure>
                        </div>
                        <div class="post--right grid__column--12 grid__column--md-7 grid__column--lg-8">
                            @inject('event_status', 'App\Services\EventStatusService')
                            {!! $event_status->register($post->ID, false) !!}
                            @include('parts.entry-meta')
                            <h5 class="post__title"><a href="{{ get_the_permalink($post->ID) }}">{{ $post->post_title }}</a></h5>
                            <p class="post__excerpt">
                                {!! excerpt($post->ID, 30) !!}
                            </p>
                            <a href="{{ get_the_permalink($post->ID) }}">Czytaj więcej ></a>

                            <div class="social-sharing">
                                @include('components.sharing-buttons')
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        @endforeach
    </div>
@endif