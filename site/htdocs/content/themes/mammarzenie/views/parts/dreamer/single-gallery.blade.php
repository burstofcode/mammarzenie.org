<div class="container single-dreamer single-dreamer__gallery display--flex flex--justify-center">
    <div class="grid__column--12 grid__column--lg-10">
        <ul id="dreamer__gallery" class="dreamer__gallery container">

            @php( $images = $event->event_image )
            @inject('image_service', 'App\Services\ImageService')

            @foreach( $images as $image )
                @php( $url = $event_image_path . $image_service::imagePathPart($event->id) . $event->id . '/' . $image->fname )
                @if(isset($image->fname))
                    <li class="dreamer__gallery-box">
                        <a href="{{ $url }}" class="swipebox">
                            <img src="{{ $url }}" alt="image">
                        </a>
                    </li>
                @endif
            @endforeach

        </ul>
    </div>
</div>