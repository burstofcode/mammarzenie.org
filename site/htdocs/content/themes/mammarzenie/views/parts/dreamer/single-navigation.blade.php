<div class="container single-dreamer single-dreamer__navigation display--flex flex--justify-center">
    <div class="grid">
        <div class="grid__column--2 grid__column--sm-6 grid__column--md-4 display--flex flex--justify-start">
            <a href="" class="dreamer__navigation-prev display--inline-block">
                <i class="fmm__chevron-left"></i><span>Zobacz poprzednie</span>
            </a>
        </div>
        <div class="grid__column--8 grid__column--sm-12 grid__column--md-4 display--flex flex--justify-center">
            <a href="{{ home_url('/chce-pomoc/') }}" class="button button--primary button--filled">Chcę pomóc</a>
        </div>
        <div class="grid__column--2 grid__column--sm-6 grid__column--md-4 display--flex flex--justify-end">
            <a href="" class="dreamer__navigation-next display--inline-block">
                <span>Zobacz następne</span><i class="fmm__chevron-right"></i>
            </a>
        </div>
    </div>
</div>
