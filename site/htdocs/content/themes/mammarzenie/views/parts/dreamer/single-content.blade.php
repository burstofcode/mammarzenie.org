<div class="container single-dreamer single-dreamer__content display--flex flex--justify-center">
    <div class="grid__column--12 grid__column--lg-10">

            <p><strong><span style="text-transform: uppercase;">{{ $event->event_category->name }}</span><br><span><small>{{ $event->place }}, {{ $event->date }}</small></span></strong></p>
            <p>{!! $event->report !!}</p>


    </div>
</div>
