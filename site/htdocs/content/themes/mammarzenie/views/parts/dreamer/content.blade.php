<div id="dreamers-loop" class="dreamers-loop dreamers-loop--grid-view container">
    <div class="dreamers-loop__wrap grid">

        @foreach($dreamers as $dreamer)

            <div class="dreamers-loop__single grid__column--12 grid__column--sm-6 grid__column--md-4 grid__column--lg-3">
                <a href="{{ home_url('/marzyciele/' . $dreamer->id . '-' . $dreamer->firstname) }}" class="dreamers-loop__link">
                    <figure class="dreamers-loop__figure">
                        @inject('image', 'App\Services\ImageService')
                        <div class
                             ="dreamers-loop__photo" style="background-image: url(
                        @if(isset($dreamer->image->fname))
                        {{ $dreamer_image_path }}{!! $image::imagePathPart($dreamer->image->dreamer_id) . $dreamer->image->dreamer_id . '/' . $dreamer->image->fname !!}
                        @else
                        {{ get_stylesheet_directory_uri() . '/dist/images/default_avatar.svg' }}
                        @endif
                    )"></div>
                        <figcaption class="dreamers-loop__caption">
                            <p class="dreamers-loop__info">
                                <span class="dreamers-loop__name">
                                    {{ $dreamer->firstname }}
                                </span>,
                                <span class="dreamers-loop__age">
                                    @inject('age', 'App\Services\AgeService')
                                    {{ $age::calculateAge( $dreamer->dateOfBirth
                                        ->birth_date ) }}
                                    @inject('word', 'App\Services\WordService')
                                    {{ $word::modifyWordYear($age::calculateAge( $dreamer
                                        ->dateOfBirth->birth_date )) }}
                                </span>
                            </p>
                            <p class="dreamers-loop__excerpt">
                                Moje marzenie: {{ $dreamer->wish->contents }}
                            </p>
                        </figcaption>
                        <div class="dreamers-loop__type grid">
                            <p class="grid__column--md-7">{{ $dreamer->wish->wish_category->name }}</p>
                            @inject('fmm_font_service', 'App\Services\FmmFontService')
                            <div class="grid__column--md-5 text--center">
                                {!! $fmm_font_service->wishTypeIcon($dreamer->wish->wish_category->ident) !!}
                            </div>
                        </div>
                    </figure>
                </a>
            </div>

        @endforeach

    </div>
</div>