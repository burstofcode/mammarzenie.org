<section  class="pending-wish dreamers-loop dreamers-loop--grid-view container">
    <header class="container">
        <div class="grid flex--justify-center">
            <div class="grid__column--12 grid__column--lg-7 block__header">
                <h2 class="block__title text-primary">
                    @php
                        $related_title = $dreamer->wish->wish_state->name;
                        $related_uri = $dreamer->wish->wish_state->ident;
                    @endphp

                    @if($related_title)
                        Inne {{ $related_title }} marzenia
                    @endif
                </h2>
            </div>
        </div>
    </header>
    {{-- Section content - slider --}}
    <div id="js-pending-wish" class="pending-wish--grid-view container">
        <div class="grid pending-wish__wrap">

            @php($type = $dreamer->wish->wish_state->ident)
            @if($type == 'oczekujace')
                @php($dreamers = $dreamers_pending)
            @elseif($type == 'zaadoptowane')
                @php($dreamers = $dreamers_adopted)
            @elseif($type == 'spelnione')
                @php($dreamers = $dreamers_fulfilled)
            @elseif($type == 'niespelnione')
                @php($dreamers = $dreamers_gone)
            @endif
            @foreach($dreamers as $dreamer)

                <div class="dreamers-loop__single grid__column--12 grid__column--sm-6 grid__column--md-4 grid__column--lg-3">
                    <a href="{{ home_url('/marzyciele/' . $dreamer->id . '-' . $dreamer->firstname) }}" class="dreamers-loop__link">
                        <figure class="dreamers-loop__figure">
                            @inject('image', 'App\Services\ImageService')
                            <div class="dreamers-loop__photo" style="background-image: url(
                        @if(isset($dreamer->image->fname))
                            {!! $dreamer_image_path . $image::imagePathPart($dreamer->image->dreamer_id) . $dreamer->image->dreamer_id . '/' . $dreamer->image->fname !!}
                            @else
                            {{ get_stylesheet_directory_uri() . '/dist/images/default_avatar.svg' }}
                            @endif
                        )"></div>
                            <figcaption class="dreamers-loop__caption">
                                <p class="dreamers-loop__info">
                                <span class="dreamers-loop__name">
                                    {{ $dreamer->firstname }}
                                </span>,
                                    <span class="dreamers-loop__age">
                                    @inject('age', 'App\Services\AgeService')
                                        {{ $age::calculateAge( $dreamer->dateOfBirth
                                            ->birth_date ) }}
                                        @inject('word', 'App\Services\WordService')
                                        {{ $word::modifyWordYear($age::calculateAge( $dreamer
                                            ->dateOfBirth->birth_date )) }}
                                </span>
                                </p>
                                <p class="dreamers-loop__excerpt">
                                    Moje marzenie: {{ $dreamer->wish->contents }}
                                </p>
                            </figcaption>
                        </figure>
                    </a>
                </div>

            @endforeach

        </div>
        <div class="container text--center">
            <a href="{{ home_url('/marzyciele/' . $related_uri . '/cala-polska/wszystkie/desc/20') }}" class="pending-wish__button button button--filled button--primary">

                    Więcej marzycieli

            </a>
        </div>
    </div>
</section>
