<div class="container single-dreamer single-dreamer__cover display--flex flex--justify-center">
    <div class="grid__column--12 grid__column--lg-10">
        <div class="grid">
            <div class="dreamer__left grid__column--12 grid__column--sm-6 grid__column--md-5 grid__column--xl-4">
                <figure class="dreamer__figure">

                    @inject('image', 'App\Services\ImageService')

                    <div class
                         ="dreamer__photo" style="background-image: url(
                        @if(isset($dreamer->image->fname))
                    {!! $dreamer_image_path . $image::imagePathPart($dreamer->id) . $dreamer->id . '/' . $dreamer->image->fname !!}
                    @else
                    {{ get_stylesheet_directory_uri() . '/dist/images/default_avatar.svg' }}
                    @endif
           )"></div>
                </figure>
            </div>
            <div class="dreamer__middle grid__column--12 grid__column--sm-6 grid__column--md-5 grid__column--xl-6">
                <div class="dreamer__middle--top">
                    <h3 class="dreamer__name">

                        {{ $dreamer->firstname }},

                        <span class="dreamers-loop__age">
                            @inject('age', 'App\Services\AgeService')
                            {{ $age::calculateAge( $dreamer->dateOfBirth
                                ->birth_date ) }}
                            @inject('word', 'App\Services\WordService')
                            {{ $word::modifyWordYear($age::calculateAge( $dreamer
                                ->dateOfBirth->birth_date )) }}
                                </span></h3>
                    <p class="dreamer__category"><span class="label">Kategoria: </span>
                        <span class="name">
                            {{ $dreamer->wish->wish_category->name }}
                        </span></p>
                </div>
                <div class="dreamer__middle--bottom">
                    <p class="dreamer__type">
                        <span class="label">Stan marzenia:</span>
                        <span class="name">{{ $dreamer->wish->wish_state->name }}</span>
                    </p>
                    @php($type = $dreamer->wish->wish_state->ident)
                    <div class="dreamer__type--icons">
                        <i class="dreamer__type--icon @if($type == 'oczekujace') active @endif fmm__face-awaiting" title="Marzenie oczekujące"></i>
                        <i class="dreamer__type--icon @if($type == 'zaadoptowane') active @endif fmm__face-adopted" title="Marzenie zaadaptowane"></i>
                        <i class="dreamer__type--icon @if($type == 'spelnione') active @endif fmm__face-fulfilled" title="Marzenie spełnione"></i>
                    </div>
                </div>
            </div>
            <div class="dreamer__right grid__column--12 grid__column--md-2">
                <div class="chevrons">
                    <i class="fmm__chevron-left"></i>
                    <i class="fmm__chevron-right"></i>
                </div>
            </div>
        </div>
    </div>
</div>
