@if(isset($dreamer))
@inject('is_active', 'App\Services\DreamersMenuService')
@endif
<ul class="nav__menu">
    <li class="nav__item @if(isset($dreamer)) {{ $is_active->pendingMenuItem($dreamer) }} @endif @if(strpos(Request::url(), 'oczekujace') !== false) active @endif">
        <a href="{{ url('/marzyciele/oczekujace/cala-polska/wszystkie/desc/20') }}" class="nav__link">
            Marzenia oczekujące
        </a>
    </li>
    <li class="nav__item @if(isset($dreamer)) {{ $is_active->adoptedMenuItem($dreamer) }} @endif @if(strpos(Request::url(), 'zaadoptowane') !== false) active @endif">
        <a href="{{ url('/marzyciele/zaadoptowane/cala-polska/wszystkie/desc/20') }}" class="nav__link">
            Marzenia zaadoptowane
        </a>
    </li>
    <li class="nav__item @if(isset($dreamer)) {{ $is_active->fulfilledMenuItem($dreamer) }} @endif @if(strpos(Request::url(), 'spelnione') !== false) active @endif">
        <a href="{{ url('/marzyciele/spelnione/cala-polska/wszystkie/desc/20') }}" class="nav__link">
            Marzenia spełnione
        </a>
    </li>
    <li class="nav__item @if(isset($dreamer)) {{ $is_active->goneMenuItem($dreamer) }} @endif @if(strpos(Request::url(), 'byli-z-nami') !== false) active @endif">
        <a href="{{ url('/marzyciele/byli-z-nami/20') }}" class="nav__link">
            Byli z nami
        </a>
    </li>
    <li class="nav__item @if(strpos(Request::url(), 'formularz-zgloszeniowy') !== false) active @endif">
        <a href="{{ url('/marzyciele/formularz-zgloszeniowy') }}" class="nav__link">
            Zgłoś marzyciela
        </a>
    </li>

</ul>