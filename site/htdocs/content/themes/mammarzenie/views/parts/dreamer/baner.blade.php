<div class="baner container--wide">
    <div class="container">
        <div class="baner__breadcrumbs">
            @inject('view', 'App\Services\BreadcrumbService')
            {!! $view->breadcrumbs(request()) !!}
        </div>

        <div class="baner__menu container">
            @include('parts.dreamer.menu')
        </div>

        <h1 class="baner__title">
            {{ $title }}
        </h1>
    </div>
</div>