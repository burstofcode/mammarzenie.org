

<div id="filters" class="filters--top container">
    <div class="grid">

        @if(!empty($date_filter))
            @include('components.filters.date')
        @endif

        @if(!empty($dream_category_filter))
            @include('components.filters.wish-category')
        @endif

        @if(!empty($event_type_filter))
            @include('components.filters.event-type')
        @endif

        @if(!empty($branch_filter))
            @include('components.filters.branch')
        @endif

        @if(!empty($event_time_frame_filter))
            @include('components.filters.event-time-frame')
        @endif

        @if(!empty($view_type_filter))
            @include('components.filters.view-type')
        @endif

    </div>
</div>