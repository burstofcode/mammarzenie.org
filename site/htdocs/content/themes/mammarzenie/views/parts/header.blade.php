<header class="header" id="js-header">
  <div class="container" id="js-header-nav">

    @include('parts.header-brand')

    <button id="js-header-nav__hamburger" class="nav__hamburger" type="button">
      <span></span>
      <span></span>
      <span></span>
      <span></span>
    </button>

    @include('parts.header-menu')

  </div>
</header>
