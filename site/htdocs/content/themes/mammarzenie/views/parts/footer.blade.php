<footer class="footer container--fluid">
  <div class="container footer--top">
    <div class="grid">

      <div class="footer__column-1 grid__column--12 grid__column--md-6 grid__column--lg-4">
        <p class="footer__line-1">Fundacja Mam Marzenie</p>
        <p class="footer__line-2">ul. Św. Krzyża 7, 31-028 Kraków</p>
        <p class="footer__line-3">+48 12 426 31 11</p>
        <p class="footer__line-4">fundacja@mammarzenie.org</p>
      </div>

      <div class="footer__column-2 grid__column--12 grid__column--md-6 grid__column--lg-4">
        <p class="footer__line-1">Nr konta:</p>
        <p class="footer__line-2">26 1050 1445 1000 0022 7647 0461</p>
      </div>

      <div class="footer__column-3 grid__column--12 grid__column--lg-4">
        <ul class="footer__social">
          <li class="footer__social-item">
            <a class="footer__social-link" href="#facebook">
              <i class="footer__social-icon fmm__facebook"></i>
            </a>
          </li>
          <li class="footer__social-item">
            <a class="footer__social-link" href="#twitter">
              <i class="footer__social-icon fmm__twitter"></i>
            </a>
          </li>
          <li class="footer__social-item">
            <a class="footer__social-link" href="#youtube">
              <i class="footer__social-icon fmm__youtube-play"></i>
            </a>
          </li>
          <li class="footer__social-item">
            <a class="footer__social-link" href="#instagram">
              <i class="footer__social-icon fmm__instagram"></i>
            </a>
          </li>
        </ul>
      </div>

    </div>
  </div>

  <div class="container footer--bottom">
    <div class="grid">
      <div class="grid__column--12 grid__column--lg-7">
        @include('parts.footer-menu')
      </div>
      <div class="grid__column--12 grid__column--lg-5">
        <p class="footer__credits">{{ sprintf( '© %s Copyright', date( 'Y' ) ) }} Fundacja Mam Marzenie</p>
      </div>
    </div>
  </div>
</footer>
