<script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous">
</script>
<script>
    let current;

        current = window.location.href;

        console.log(current);
        if (current.indexOf('marzyciele') > -1) {
            $('.item_marzyciele').addClass('active');
        } else if (current.indexOf('wolontariat') > -1) {
            $('.item_wolontariat').addClass('active');
        } else if (current.indexOf('co-robimy') > -1) {
            $('.item_co_robimy').addClass('active');
        } else if (current.indexOf('aktualnosci') > -1) {
            $('.item_aktualnosci').addClass('active');
        } else if (current.indexOf('o-fundacji') > -1) {
            $('.item_o_fundacji').addClass('active');
        } else if (current.indexOf('blog') > -1) {
            $('.item_blog').addClass('active');
        }


</script>

@php($branches = App\Models\Branch::list())

@foreach($branches as $branch)
    <script>
        if (current.indexOf('mammarzenie.org/<?php echo $branch->slug; ?>') > -1) {
            $('.item_wolontariat').addClass('active');
        }
    </script>
@endforeach