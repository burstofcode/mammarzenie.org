
<article id="post-{{ $post->ID }}" {!! post_class() !!}>

    <div class="container display--flex flex--justify-center">
        <div class="grid__column--12 grid__column--lg-10">


    {!! post_thumbnail($post->ID) !!}

            @include('parts.entry-meta')

    <div class="entry-content">
        {!! $post->post_content !!}
    </div><!-- .entry-content -->

        </div>
    </div>
</article><!-- #post-{{ $post->ID }} -->
