<!doctype html>
<html {!! get_language_attributes() !!}>
<head>
    <meta charset="{{ get_bloginfo('charset') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    @head
</head>
<body @php(body_class(strtolower(get_the_title())))>
<div id="page" class="wrap">
    <a class="skip-link screen-reader-text" href="#content">{{ esc_html__('Skip to content', THEME_TD) }}</a>

    @include('parts.header')

    @include('parts.active-menu')

    <div class="content">
        <main class="main">
            @yield('content')
        </main>
    </div>

    @include('parts.footer')
</div><!-- #page -->

@footer

</body>
</html>
