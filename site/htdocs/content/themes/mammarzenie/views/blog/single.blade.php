@extends('layouts.main')

@section('content')
    @loop

        @include('parts.baner')
        @include('parts.content-post')
        <div class="container">
            @if(is_single())
                @include('parts.post-nav')
            @endif
        </div>

    @endloop
@endsection