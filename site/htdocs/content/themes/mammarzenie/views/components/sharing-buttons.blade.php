<div class="share display--flex flex--justify-center">
    <a href="" class="facebook">
        <i class="fmm__facebook"></i>
    </a>
    <a href="" class="twitter">
        <i class="fmm__twitter"></i>
    </a>
    <a href="" class="instagram">
        <i class="fmm__instagram"></i>
    </a>
</div>