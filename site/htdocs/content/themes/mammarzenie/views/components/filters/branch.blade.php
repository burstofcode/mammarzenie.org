<script>
    let branchFromController = '{{ $the_branch }}';
</script>

<div class="filter--branch filter grid__column--12 grid__column--sm-6 grid__column--md-3">
    <select class="dropdown__select" id="filter-branch">
        <option data-placeholder="true"></option>

        @foreach( $branches as $branch )
            <script>
                document.write(`<option value="` + currentUrl.replace(branchFromController, "{{ $branch->slug }}") + `"`);
            </script>
            @if(strpos(Request::url(), $branch->slug) !== false)
                selected
            @endif
        >
                {{ $branch->city }}
            </option>
        @endforeach
    </select>
</div>