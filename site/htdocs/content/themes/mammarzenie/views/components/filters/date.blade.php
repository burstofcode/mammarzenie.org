<script>
    let dateFromController = '{{ $the_order }}';
</script>

<div class="filter--date filter grid__column--12 grid__column--sm-6 grid__column--md-3">
    <select class="dropdown__select" id="filter-date">
        <option data-placeholder="true"></option>

        <script>
            document.write(`<option value="` + currentUrl.replace(dateFromController, "desc") + `"`);
        </script>
        @if( strpos(Request::url(), 'desc' ) !== false)
            selected
        @endif
        >
        od najnowszych
        </option>

        <script>
            document.write(`<option value="` + currentUrl.replace(dateFromController, "asc") + `"`);
        </script>
        @if( strpos(Request::url(), 'asc' ) !== false)
            selected
        @endif
        >
        od najstarszych
        </option>

    </select>
</div>