<script>
    let paginationFromController = '{{ $the_pagi }}';
</script>
<div class="filter--items-per-page filter grid__column--12 grid__column--lg-3">
    <select class="dropdown__select" id="filter-items-per-page">
        @php
        $pagination_list = ["12", "20", "36", "48"];
        @endphp

        @foreach( $pagination_list as $pagination )
            <script>
                document.write(`<option value="` + currentUrl.replace(paginationFromController, "{{ $pagination }}") + `"`);
            </script>
            @if(strpos(Request::url(), $pagination) !== false)
                selected
            @endif
            >
            {{ $pagination }} / stronę
            </option>
        @endforeach
    </select>
</div>
