<script>
    let eventStatusFromController = '{{ $the_event_status }}';
</script>

<div class="filter--event-timeframe filter grid__column--12 grid__column--sm-6 grid__column--md-3">
    <select class="dropdown__select" id="filter-event-timeframe">
        <script>
            document.write(`<option value="` + currentUrl.replace(eventStatusFromController, "trwajace") + `"`);
        </script>
        @if( strpos(Request::url(), 'trwajace' ) !== false)
            selected
        @endif
        >
        trwające
        </option>

        <script>
            document.write(`<option value="` + currentUrl.replace(eventStatusFromController, "zakonczone") + `"`);
        </script>
        @if( strpos(Request::url(), 'zakonczone' ) !== false)
            selected
        @endif
        >
        zakończone
        </option>
    </select>
</div>
