<script>
    let catFromController = '{{ $the_cat }}';
</script>
<div class="filter--dreamers-category filter grid__column--12 grid__column--sm-6 grid__column--md-3">
    <select class="dropdown__select" id="filter-dreamers-category">
        <option data-placeholder="true"></option>

        @foreach( $wish_cats as $cat )
            <script>
                document.write(`<option value="` + currentUrl.replace(catFromController, "{{ $cat->ident }}") + `"`);
            </script>
            @if(strpos(Request::url(), $cat->ident) !== false)
                selected
            @endif
            >
            {{ $cat->name }}
            </option>
        @endforeach
    </select>
</div>