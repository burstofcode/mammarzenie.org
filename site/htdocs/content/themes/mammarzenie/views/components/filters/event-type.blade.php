<script>
    let eventTypeFromController = '{{ $the_event }}';
</script>

<div class="filter--event-type filter grid__column--12 grid__column--sm-6 grid__column--md-3">
    <select class="dropdown__select" id="filter-event-type">
        <script>
            document.write(`<option value="` + currentUrl.replace(eventTypeFromController, "wszystkie") + `"`);
        </script>
        @if( strpos(Request::url(), 'wszystkie' ) !== false)
            selected
        @endif
        >
        wszystkie
        </option>

        <script>
            document.write(`<option value="` + currentUrl.replace(eventTypeFromController, "imprezy-cykliczne") + `"`);
        </script>
        @if( strpos(Request::url(), 'imprezy-cykliczne' ) !== false)
            selected
        @endif
        >
        imprezy cykliczne
        </option>

        <script>
            document.write(`<option value="` + currentUrl.replace(eventTypeFromController, "nasze-akcje") + `"`);
        </script>
        @if( strpos(Request::url(), 'nasze-akcje' ) !== false)
            selected
        @endif
        >
        nasze akcje
        </option>
    </select>
</div>
