<div id="view-type--buttons" class="filter--view-type filter grid__column--12 grid__column--sm-6 grid__column--md-3">
    <div id="view-type--grid" class="view-type--grid active">
        <i class="fmm__view-grid"></i>
    </div>
    <div id="view-type--list" class="view-type--list">
        <i class="fmm__view-list"></i>
    </div>
</div>
