@php $delimiter = ' > ' @endphp
<span><a href="{{ url('/') }}">Strona głowna</a></span>
{{ $delimiter }}

@if( strpos(Request::url(), 'co-robimy') )
<span>Co robimy</span>
@elseif( strpos(Request::url(), 'aktualnosci') )
    <span>Aktualności</span>
@endif