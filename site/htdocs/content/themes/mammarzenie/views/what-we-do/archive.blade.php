@extends('layouts.main')

    <script
            src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous">
    </script>
    <script>
        let originalUrl = location.href;
        let currentUrl;
        if(originalUrl.indexOf('?') > 0) {
            currentUrl = originalUrl.substring(0, originalUrl.indexOf('?'));
        } else {
            currentUrl = originalUrl;
        }
    </script>


    @section('content')
        <div class="archive">
            @include('parts.baner-custom')
            @include('parts.dreamer.filters-top')
            @include('parts.content-news')
            @include('parts.filters-bottom-custom')
        </div>
    @endsection
