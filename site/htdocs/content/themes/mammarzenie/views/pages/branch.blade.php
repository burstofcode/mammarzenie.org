@extends('layouts.main')

@section('content')

        @include('parts.baner-branch')

    <div class="container">

        @include('parts.content-branch')

        <section class="archive">

            <h2 class="block__title text--center">Aktualności</h2>

            @include('parts.content-news')

            <div class="container text--center">

                <a href="{{ home_url('/aktualnosci/' . $posts[0]->post_type) . '/desc/20'}}" class="button button--filled button--primary">
                    {{ $news_btn }}
                </a>
            </div>

        </section>

        <div class="container">
            <h2 class="block__title text--center">{{ $vol_title }}</h2>
        </div>

        <div class="container">
            <ul class="branches__list">
                @foreach ( $all_volunteers as $volunteer )
                    <li class="branches__item">
                        <p class="branches__link">{{ $volunteer->first_name }} {{ $volunteer->last_name }}</p>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>


@endsection