@extends('layouts.main')

@section('content')
    @loop

        @if(!is_front_page())
            @include('parts.baner')
        @endif

        @template('parts.content', 'page')

    @endloop
@endsection