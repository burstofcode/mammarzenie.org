@extends('layouts.main')

@section('content')
    @loop

        @if(!is_front_page())
            @include('parts.baner')
        @endif

    <section class=" ambasador-extended container--wide">
        <div class="container">
            <div class="grid flex--justify-center">
                <div class="ambasador-extended__left grid__column--12 grid__column--lg-5">
                    <img src="{{ get_the_post_thumbnail_url() }}" alt="" class="ambasador-extended__photo">
                </div>
                <div class="ambasador-extended__right grid__column--12 grid__column--lg-5">
                    @template('parts.content', 'page')
                </div>
            </div>
        </div>
    </section>

    @endloop
@endsection