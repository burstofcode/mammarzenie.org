@extends('layouts.main')

@section('content')
    <div class="container narrow">
        @loop

        @if(!is_front_page())
            @include('parts.baner')
        @endif

        @template('parts.content', 'page')

        @endloop
    </div>
@endsection