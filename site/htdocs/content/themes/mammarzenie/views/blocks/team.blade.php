<div class="team container {{ $align }} {{ $class_name }}">
    <div class="grid flex--justify-center">
        <ul class="grid__column--lg-10">

            @foreach($members as $member)
                <li class="team__member grid">
                    <div class="team__member--left grid__column--md-3 grid__column--lg-2">
                        {{-- Photo --}}
                        <figure class="team__figure">
                            @if ($member['photo'])
                                <div class="team__photo" style="background-image: url({{ $member['photo']['url'] }})"></div>
                            @else
                                        <div class="team__photo" style="background-image: url({{ $placeholder }})"></div>
                            @endif
                        </figure>
                    </div>
                    <div class="team__member--right grid__column--md-9 grid__column--lg-10">
                        {{-- Name --}}
                        <h5 class="team__name">
                            @if ($member['name'])
                                {{ $member['name'] }}
                            @else
                                John Doe
                            @endif
                        </h5>
                        {{-- Position --}}
                        <p class="team__position">

                            @if ($member['position'])
                                {{ $member['position'] }}
                            @else
                                CEO
                            @endif
                        </p>
                        {{-- Descrition --}}
                        <p class="team__description">

                            @if ($member['content'])
                                {{ $member['content'] }}
                            @else
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            @endif
                        </p>
                    </div>
                </li>
            @endforeach

        </ul>
    </div>
</div>
