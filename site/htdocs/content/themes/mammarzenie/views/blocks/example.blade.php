<div class="bg-blue-600 text-white p-4 mb-4">
    <h1 id="checkitout">This is tester of my composer package. Example.</h1>
    <h2 class="text-3xl mb-2">{{ $label }}</h2>
    <p>{{ $description }}</p>
    @if ($items)
    <ul>
        @foreach ($items as $item)
        <li>{{ $item['item'] }}</li>
        @endforeach
    </ul>
    @endif
</div>