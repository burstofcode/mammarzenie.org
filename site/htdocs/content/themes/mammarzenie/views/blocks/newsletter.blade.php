<section class="newsletter container {{ $align }} {{ $class_name }}">

    <div class="newsletter__box">
        <h2 class="newsletter__title">
            @if($title)
                {{ $title }}
            @else
                Newsletter
            @endif
        </h2>
        <input class="newsletter__email" type="email" placeholder="
            @if($input_placeholder)
                {{ $input_placeholder }}
            @else
                Your email
            @endif
        ">
        <button class="newsletter__submit button button--filled button--primary">
            @if($button)
                {{ $button }}
            @else
                SignUp
            @endif
        </button>
    </div>

</section>