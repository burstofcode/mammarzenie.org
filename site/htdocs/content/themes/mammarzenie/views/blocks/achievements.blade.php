<section class=" achievements container--wide {{ $align }} {{ $class_name }}">
    <header class="container">
        <div class="grid flex--justify-center">
            <div class="grid__column--12 grid__column--lg-8 block__header">
                <h2 class="block__title text-primary">
                    @if($title)
                        {{ $title }}
                    @else
                        Achievements
                    @endif
                </h2>
            </div>
        </div>
    </header>

    <div id="js-achievements" class="container">
        <div class="grid achievements__wrap">

            @query([
            'post_type' => ['imprezy-cykliczne', 'nasze-akcje'],
            'posts_per_page' => 4,
            'order' => 'DESC'
            ])

                <div class="achievements__single grid__column--6 grid__column--lg-3">
                    <a href="{{ get_the_permalink(get_the_ID()) }}" class="achievements__link">
                        <div class="achievements__box">
                            <figure class="achievements__figure">
                                <div style="background-image: url({{ get_the_post_thumbnail_url(get_the_ID()) }})" class="achievements__photo"></div>
                                <div class="achievements__overlay"></div>
                            </figure>
                            <div class="achievements__captions">
                                <p class="achievements__date">
                                    {{ get_the_date('d F Y', get_the_ID()) }}
                                </p>
                                <h3 class="h5 achievements__title">
                                    {!! Loop::title() !!}
                                </h3>
                            </div>
                        </div>
                    </a>
                </div>

            @endquery

        </div>
    </div>

    <div class="container text--center">
        <a href="" class="achievements__button button button--filled button--primary">
            @if($button)
                {{ $button }}
            @else
                Więcej...
            @endif
        </a>
    </div>

</section>
