<section class=" news container {{ $align }} {{ $class_name }}">
    <header class="container">
        <div class="grid flex--justify-center">
            <div class="grid__column--12 grid__column--lg-8 block__header">
                <h2 class="block__title text-primary">
                    @if($title)
                        {{ $title }}
                    @else
                        Tytuł. Aktualności
                    @endif
                </h2>
            </div>
        </div>
    </header>

    <div id="js-news" class="container">
        <div class="grid news__wrap">

            @query([
            'post_type' => ['bialystok', 'bydgoszcz', 'gorzow-wlkp', 'katowice', 'kielce', 'krakow', 'lublin', 'lodz', 'olsztyn', 'opole', 'poznan', 'rzeszow', 'szczecin', 'trojmiasto', 'warszawa', 'wroclaw'],
            'posts_per_page' => 4,
            'order' => 'DESC'
            ])

                <div class="news__single grid__column--6 grid__column--lg-3">
                    <a href="{{ get_the_permalink(get_the_ID()) }}" class="news__link">
                        <div class="news__box">
                            <figure class="news__figure">
                                <div style="background-image: url({{ get_the_post_thumbnail_url(get_the_ID()) }})" class="news__photo"></div>
                                <div class="news__overlay"></div>
                            </figure>
                            <div class="news__captions">
                                <p class="news__date">
                                    {{ get_the_date('d F Y', get_the_ID()) }}
                                </p>
                                <h3 class="h4 news__title">
                                    {!! Loop::title() !!}
                                </h3>
                            </div>
                        </div>
                    </a>
                </div>

            @endquery

        </div>
    </div>

    <div class="container text--center">
        <a href="" class="news__button button button--filled button--primary">
            @if($button)
                {{ $button }}
            @else
                Więcej...
            @endif
        </a>
    </div>

</section>