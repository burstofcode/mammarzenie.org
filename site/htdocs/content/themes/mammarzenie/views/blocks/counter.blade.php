<section class="counter container {{ $align }} {{ $class_name }}">
    <header class="container">
        <div class="grid flex--justify-center">
            <div class="block__header grid__column--12 grid__column--lg-8">
                <h2 class="block__title text-primary">
                    @if($title)
                        {{ $title }}
                    @else
                        Counter
                    @endif
                </h2>
            </div>
        </div>
    </header>

    <div class="container">
        <div class="grid">

            <div class="counter__single grid__column--6 grid__column--lg-3">
                <div class="counter__circle">
                    <h5 class="counter__number">@inject('age', 'App\Services\AgeService')
                        {{ $age::calculateAge( '14-06-2003' ) }}</h5>
                </div>
                <h5 class="counter__title">lat działalnosci</h5>
            </div>

            <div class="counter__single grid__column--6 grid__column--lg-3">
                <div class="counter__circle">
                    <h5 class="counter__number">{{ $fulfilled_dreams }}</h5>
                </div>
                <h5 class="counter__title">spełnionych marzeń</h5>
            </div>

            <div class="counter__single grid__column--6 grid__column--lg-3">
                <div class="counter__circle">
                    <h5 class="counter__number">{{ $volunteers }}</h5>
                </div>
                <h5 class="counter__title">wolontariuszy</h5>
            </div>

            <div class="counter__single grid__column--6 grid__column--lg-3">
                <div class="counter__circle">
                    <h5 class="counter__number">{{ $branches }}</h5>
                </div>
                <h5 class="counter__title">oddziałow</h5>
            </div>

        </div>
    </div>

    <div class="container text--center">
        <a href="{{ $button_url }}" class="counter__button button button--outlined button--light">
            @if($button_name)
                {{ $button_name }}
            @else
                More
            @endif
        </a>
    </div>

</section>

<style>
    .counter {
        background-image: url({{ $background_url }});
    }
</style>
