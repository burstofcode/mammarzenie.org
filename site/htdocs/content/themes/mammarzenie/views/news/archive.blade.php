@extends('layouts.main')

    <script
            src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous">
    </script>
    <script>
        let originalUrl = location.href;
        let currentUrl;
        if(originalUrl.indexOf('?') > 0) {
            currentUrl = originalUrl.substring(0, originalUrl.indexOf('?'));
        } else {
            currentUrl = originalUrl;
        }
    </script>


    @section('content')
        <div class="archive">
            <div class="baner container--wide">
                <div class="container">
                    <div class="baner__breadcrumbs">
                        @include('components.breadcrumb')
                    </div>

                    <h1 class="baner__title text--center">
                        {{ $title }}
                    </h1>
                </div>
            </div>

            <div id="filters" class="filters--top container">
                <div class="grid">

                    @if(!empty($date_filter))
                        @include('components.filters.date')
                    @endif

                    @if(!empty($event_type_filter))
                        @include('components.filters.event-type')
                    @endif

                    @if(!empty($branch_filter))
                        @include('components.filters.branch')
                    @endif

                    @if(!empty($event_time_frame_filter))
                        @include('components.filters.event-time-frame')
                    @endif

                </div>
            </div>

            @include('parts.content-news')

            @include('parts.filters-bottom-custom')
        </div>
    @endsection
