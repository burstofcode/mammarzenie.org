<?php

namespace WPEloquent\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use WPEloquent\Model\Post\Meta;
use WPEloquent\Model\Term\Relationships;
use WPEloquent\Model\Term\Taxonomy;
use \WPEloquent\Traits\HasMeta;

/**
 * Class Post
 * @package WPEloquent\Model
 */
class Post extends  Model {

    use HasMeta;

    protected $table      = 'posts';
    protected $primaryKey = 'ID';
    protected $post_type = null;
    public $timestamps    = false;

    const CREATED_AT = 'post_date';
	const UPDATED_AT = 'post_modified';
	
	public function newQuery() {
		$query = parent::newQuery();
		if($this->post_type) {
			return $this->scopeType($query, $this->post_type);
		}
		return $query;
	}

    public function author () {
        return $this->hasOne(\WPEloquent\Model\User::class, 'ID', 'post_author');
    }

    /**
     * @return HasMany
     */
    public function meta () {
        return $this->hasMany(Meta::class, 'post_id')
                    ->select(['post_id', 'meta_key', 'meta_value']);
    }

    /**
     * @return HasManyThrough
     */
    public function terms () {
        return $this->hasManyThrough(
                    Taxonomy::class,
                    Relationships::class,
                    'object_id',
                    'term_taxonomy_id'
                )->with('term');
    }

    /**
     * @return HasManyThrough
     */
    public function categories () {
        return $this->terms()->where('taxonomy', 'category');
    }

    /**
     * @return HasManyThrough
     */
    public function branchy () {
        return $this->terms()->where('taxonomy', 'bialystok');
    }

    /**
     * @return HasMany
     */
    public function attachments () {
        return $this->hasMany(\WPEloquent\Model\Attachment::class, 'post_parent', 'ID')->where('post_type', 'attachment');
    }

    /**
     * @return HasManyThrough
     */
    public function tags () {
        return $this->terms()->where('taxonomy', 'post_tag');
    }

    /**
     * @return HasMany
     */
    public function comments () {
        return $this->hasMany(\WPEloquent\Model\Comment::class, 'comment_post_ID');
    }

    /**
     * @param $query
     * @param string $status
     * @return mixed
     */
    public function scopeStatus ( $query, $status = 'publish') {
        return $query->where('post_status', $status);
    }

    /**
     * @param $query
     * @param string $type
     * @return mixed
     */
    public function scopeType ( $query, $type = 'post') {
        return $query->where('post_type', $type);
    }

    /**
     * @param $query
     * @param string $type
     * @return mixed
     */
    public function scopeOrType ( $query, $type = 'post') {
        return $query->orWhere('post_type', $type);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopePublished ($query) {
        return $query->status('publish');
    }

    /**
     * @param $query
     * @param string $taxonomy
     * @return mixed
     */
    public function scopeTaxonomy ( $query, $taxonomy = 'category') {
        return $query->terms()->where('taxonomy', $taxonomy);
    }

    /**
     * @return HasManyThrough
     */
    public function eventBranch () {
        return $this->terms()->where('taxonomy', 'event_branch');
    }

    /**
     * @inheritDoc
     */
    public function getQueueableRelations()
    {
        // TODO: Implement getQueueableRelations() method.
    }

    /**
     * @inheritDoc
     */
    public function getQueueableConnection()
    {
        // TODO: Implement getQueueableConnection() method.
    }

    /**
     * @inheritDoc
     */
    public function resolveRouteBinding( $value )
    {
        // TODO: Implement resolveRouteBinding() method.
    }
}
