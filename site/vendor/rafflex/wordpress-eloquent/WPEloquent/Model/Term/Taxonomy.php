<?php

namespace WPEloquent\Model\Term;

use Illuminate\Database\Eloquent\Model;
use WPEloquent\Model\Term;

/**
 * Class Taxonomy
 * @package WPEloquent\Model\Term
 */
class Taxonomy extends Model {
    protected $table = 'term_taxonomy';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function term() {
        return $this->belongsTo(Term::class, 'term_id', 'term_id');
    }
}
