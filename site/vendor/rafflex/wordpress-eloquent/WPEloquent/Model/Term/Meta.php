<?php

namespace WPEloquent\Model\Term;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Meta
 * @package WPEloquent\Model\Term
 */
class Meta extends Model {
    protected $table = 'term_meta';
    protected $fillable = ['meta_key', 'meta_value'];
    protected $primaryKey = 'meta_id';


}
