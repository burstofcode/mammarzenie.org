<?php

namespace WPEloquent\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Link
 * @package WPEloquent\Model
 */
class Link extends Model {
    protected $table      = 'links';
    protected $primaryKey = 'link_id';
    public $timestamps    = false;
}
