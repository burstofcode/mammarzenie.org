<?php

// Home
Breadcrumbs::for('home', function ($trail) {
   $trail->push('Strona główna', route('home'));
});

// Home > Marzyciele
Breadcrumbs::for('dreamers', function ($trail) {
    $trail->parent('home');
    $trail->push('Marzyciele', route('dreamers'));
});

// Home > Marzyciele
Breadcrumbs::for('wish_state', function ($trail) {
    $trail->parent('dreamers');
    $trail->push('Marzenia oczekujące', route('dreamers.wish-state'));
});