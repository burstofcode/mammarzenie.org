<?php

/**
 * Application routes.
 */

use Themosis\Support\Facades\Route;

Route::any('template', ['container-template', function() {
    return view('templates.container');
}]);

Route::any('template', ['narrow-template', function() {
    return view('templates.narrow');
}]);

Route::any('template', ['wide-template', function() {
    return view('templates.wide');
}]);

Route::any('template', ['full-template', function() {
    return view('templates.full');
}]);

Route::any('ambasadorka-fundacji', function() {
    return view('pages.ambasador');
} );

// Dreamers
Route::group([
    'prefix' => 'marzyciele',
], function() {
    Route::redirect('/', '/marzyciele/oczekujace/cala-polska/wszystkie/desc/20')
        ->name('dreamers');
    Route::any('/{state}/{branch}/{category}/{order}/{pagination}', 'DreamerController@index')
        ->name('dreamers.wish-state');
    Route::any('/byli-z-nami/{pagination}', 'DreamerController@indexGone');
    Route::any('/formularz-zgloszeniowy', 'Dreamer\SignUpFormController@index');
    Route::any('/{dreamer_id}', 'DreamerController@show');
});


// What we do
Route::any('single', 'BlogController@show');
Route::any('co-robimy/{event}/{branch}/{event_status}/{pagination}', 'WhatWeDoController@index');
//Route::any('co-robimy/nasze-akcje/{slug}/*', 'BlogController@show');
Route::redirect('/co-robimy', '/co-robimy/wszystkie/cala-polska/1/20');
Route::redirect('/nasze-akcje', '/co-robimy/nasze-akcje/cala-polska/1/20');
Route::redirect('/co-robimy/nasze-akcje', '/co-robimy/nasze-akcje/cala-polska/1/20');
Route::redirect('/imprezy-cykliczne', '/co-robimy/imprezy-cykliczne/cala-polska/1/20');
Route::redirect('/co-robimy/imprezy-cykliczne', '/co-robimy/imprezy-cykliczne/cala-polska/1/20');

//Route::any('/bydgoszcz/{slug}', 'BlogController@show');
// Blog
Route::redirect('/blog', '/blog/20');
Route::any('/blog/{pagination}', 'BlogController@index');
//Route::get('/blog/{slug}', 'BlogController@show');

// News
Route::any('/aktualnosci/{branch}/{order}/{pagination}', 'NewsController@index');
Route::redirect('/aktualnosci', '/aktualnosci/cala-polska/desc/20');

/**
 * Branch pages
 */
Route::any('/bialystok', 'BranchController@show');
Route::any('/bydgoszcz', 'BranchController@show');
Route::any('/gorzow-wlkp', 'BranchController@show');
Route::any('/katowice', 'BranchController@show');
Route::any('/kielce', 'BranchController@show');
Route::any('/krakow', 'BranchController@show');
Route::any('/lublin', 'BranchController@show');
Route::any('/lodz', 'BranchController@show');
Route::any('/olsztyn', 'BranchController@show');
Route::any('/opole', 'BranchController@show');
Route::any('/poznan', 'BranchController@show');
Route::any('/rzeszow', 'BranchController@show');
Route::any('/szczecin', 'BranchController@show');
Route::any('/trojmiasto', 'BranchController@show');
Route::any('/warszawa', 'BranchController@show');
Route::any('/wroclaw', 'BranchController@show');

// Branches
Route::any('branches', 'BranchController@index');

// Pages
Route::any('page', function() {
    return view('pages.default');
});

